<?php

namespace App\Common;

use App\Models\General;
use DateTime;
use Illuminate\Support\Facades\Auth;

if (!function_exists('timeOfDay')) {
    function timeOfDay()
    {
        $time = date("H");
        if (date("H") < 12) {
            return "Good Morning";
        } elseif (date("H") > 11 && date("H") < 18) {
            return "Good Afternoon";
        } elseif (date("H") > 17) {
            return "Good Evening";
        }
    }
}

if (!function_exists('settings')) {
    function settings()
    {
        return General::first();
    }
}
