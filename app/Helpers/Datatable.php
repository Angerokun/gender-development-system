<?php

namespace App\Datatable;

if (!function_exists('action_data')) {
    function action_data($show, $edit, $delete, $module)
    {
        return '<div class="dropdown">
                    <button class="btn btn-info dropdown-toggle" type="button" 
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="' . $show . '">
                            <i class="fa fa-eye"></i> View Record
                        </a>
                        <a class="dropdown-item" href="' . $edit . '">
                            <i class="fa fa-edit"></i> Edit Record
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item popup_form" data-toggle="modal" 
                            data-url="' . $delete . '" title="Delete ' . $module . ' Record">
                            <i class="fa fa-trash" aria-hidden="true"></i> Delete Record
                        </a>
                    </div>
                </div>';
    }
}
