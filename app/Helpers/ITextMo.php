<?php

namespace App;

class ITextMo
{
     public $API_KEY = 'TR-ANGEL032116_HKNUH';
     public $priority = 'NORMAL';
     public $senderId = 'Gender and Development LSPU'; //Set name of sender when using Sms
     public $passwd = '#$qr4(i)nl';
     public $url = 'https://www.itexmo.com/php_api/api.php';
     public $stopSending = false;   //prevent always send sms when it is not needed or for development purposes

    //sending OTP for Registration of freelancer
    public static function sendNotification($user, $contact)
    {
        $msg = "Your one-time SerbisPh web verification code is valid for 5 minutes";
        $sms = new ItextMo();
        return $sms->sendSms($contact, $msg);
    }

    //sending SMS backend
    public function sendSms($contact, $message)
    {
        $isError = 0;
        $errorMessage = true;
        $itexmo = array(
            '1' => $contact,
            '2' => $message,
            '3' => $this->API_KEY,
            'passwd' => $this->passwd,
            '5' => 'NORMAL',
            '6' => $this->senderId
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($itexmo));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        if (curl_errno($ch)) {
            $isError = true;
            $errorMessage = curl_error($ch);
        }
        curl_close($ch);
        if ($isError) {
            return array('error' => 1 , 'message' => $errorMessage);
        } else {
            return array('error' => 0 );
        }

        // if($this->stopSending == false)
        // {
        //     $ch = curl_init();
        //     curl_setopt_array(
        //         $ch,
        //         array(
        //             CURLOPT_URL => $this->url,
        //             CURLOPT_RETURNTRANSFER => true,
        //             CURLOPT_POST => true,
        //             CURLOPT_POSTFIELDS => $postData
        //         )
        //     );

        //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //     $output = curl_exec($ch);
        //     if (curl_errno($ch)) {
        //         $isError = true;
        //         $errorMessage = curl_error($ch);
        //     }
        //     curl_close($ch);
        //     if($isError){
        //         return array('error' => 1 , 'message' => $errorMessage);
        //     }else{
        //         return array('error' => 0 );
        //     }
        // }
        // else{
        //     return array('error' => 1, 'message' => 'Message not sent!');
        // }
    }
}
