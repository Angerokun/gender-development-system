<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AccountVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return view(
            'portal.account-verification.index',
            [
                'users' => $user
            ]
        );
    }

    /**
     * @param $id
     * @return void
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view(
            'portal.account-verification.show',
            [
                'user' => $user
            ]
        );
    }

    /**
     * get Verify
     * @return void
     */
    public function verifyModal($id, $status)
    {
        $user = User::findOrFail($id);

        return view('portal.account-verification.verify', [
            'user' => $user,
            'status' => $status
        ]);
    }

    /**
     * get Verify
     * @return void
     */
    public function registerVerification()
    {
        return view('register-success');
    }

    /**
     * verify
     *
     * @param Request $request
     * @param [type] $id
     * @param [type] $status
     *
     * @return void
     */
    public function verify(Request $request, $id, $status)
    {
        $user = User::findOrFail($id);


        $user->update([
            'status' => $status,
            'remarks' => $request->remarks
        ]);

        $to_name = $user->name;
        $to_email = $user->email;
        $data = array(
            'name' => $to_name,
            'status' => $status,
            'remarks' => $request->remarks,
        );
        Mail::send(
            'portal.emails.mail',
            $data,
            function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('GAD LSPU Account Verification');

                $message->from(env('MAIL_USERNAME'), 'GAD LSPU');
            }
        );

        return redirect(route('account-verification.index'));
    }
}
