<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumItem;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use File;
use Yajra\DataTables\DataTables;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.albums.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portal.albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        [
            'image-data0' => 'required',
            'image-data1' => 'required',
        ],
        [
            'image-data0.required' => 'Upload image is required',
            'image-data1.required' => 'Upload at least two images of the Album'
        ]
        );

        $album = Album::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        foreach ($request->input('items') as $key => $ctr) {
            $image  = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('image-data'.$ctr)));
            $save_img = Image::make($image);
            $img_url = 'album_img/'.time().'_'.$ctr.'_'.$album->id.'.png';
            $save_img->save(public_path($img_url));

            $add_image = AlbumItem::create([
                    'album_id' => $album->id,
                    'image_url'  => $img_url,
                    'sort_order' => $request->input('sort_order' . $ctr)
            ]);
        }

        return array(
            "status" => "success",
            'desc' => 'Album Successfully Uploaded!'
        );

        // return array(
        //     "status" => "success",
        //     'redirect' => true,
        //     'route' => route('albums.show', $album->id),
        //     'desc' => 'Album Successfully Uploaded!'
        // );
    }

    /**
     * @param Request $request
     * @param $id
     * @return void
     */
    public function addPhotoAlbum(Request $request, $id)
    {
        $this->validate($request,
        [
            'image-data0' => 'required',
            'image-data1' => 'required',
        ],
        [
            'image-data0.required' => 'Upload image is required',
            'image-data1.required' => 'Upload at least two images of the Album'
        ]
        );

        foreach ($request->input('items') as $key => $ctr) {
            $image  = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('image-data'.$ctr)));
            $save_img = Image::make($image);
            $img_url = 'album_img/'.time().'_'.$ctr.'_'.$id.'.png';
            $save_img->save(public_path($img_url));

            $add_image = AlbumItem::create([
                    'album_id' => $id,
                    'image_url'  => $img_url,
                    'sort_order' => $request->input('sort_order' . $ctr)
            ]);
        }

        return array(
            "status" => "success",
            'desc' => 'Album Successfully Uploaded!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $album = Album::findOrFail($id);

        return view('portal.albums.show', [
            'album' => $album
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $album = Album::findOrFail($id);

        return view('portal.albums.edit', [
            'album' => $album
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Album::where('id', $id)->update([
            'title' => $request->title,
            'description' => $request->description
        ]);

        if(!$request->has('exist_items') && !$request->has('items')){
            $this->validate($request ,[
                'image-data0' => 'required',
                'image-data1' => 'required',
            ],
            ['image-data0.required' => 'Upload image is required',
            'image-data1.required' => 'Upload at least two images of the product']
            );
        }

        if (!$request->has('exist_items')) {
            $get_img = AlbumItem::where('album_id', $id)->get();
            $this->deleteFileImg($get_img);      //delete File image
            $exist_img = AlbumItem::where('album_id', $id)->delete(); //delete record on database
        } else {
            foreach ($request->input('exist_items') as $key => $value) {
                $update_img = AlbumItem::where('id', $value)->update([
                    'sort_order'    => $request->input('sort_order_edit' . $value)
                ]);
            }

            $get_img = AlbumItem::whereNotIn('id', $request->input('exist_items'))
                                      ->where('album_id', $id)->get();

            $this->deleteFileImg($get_img); //delete File image
            $exist_img = AlbumItem::whereNotIn('id', $request->input('exist_items'))
                                   ->where('album_id', $id)->delete(); //delete record on database
        }

        if ($request->has('items')) {
            foreach ($request->input('items') as $key => $ctr) {
                $image  = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('image-data'.$ctr)));
                $save_img = Image::make($image);
                $img_url = 'album_img/'.time().'_'.$ctr.'_'.$id.'.png';
                $save_img->save(public_path($img_url));

                $add_image = AlbumItem::create([
                        'album_id' => $id,
                        'image_url'  => $img_url,
                        'sort_order' => $request->input('sort_order' . $ctr)
                ]);
            }
        }

        return array(
            "status" => "success",
            'desc' => 'Album Successfully Updated!'
        );
    }

    /**
     * @param $img
     * @return void
     */
    public function deleteFileImg($img)
    {
        foreach ($img as $key => $value) {
            $image_path = public_path($value->image_url);
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
        }
    }

    /**
     * delete
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $album = Album::findOrFail($id);

        return view(
            'portal.albums.delete',
            ['album' => $album]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);

        $album->delete();

        return redirect(route('albums.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function albumDatatable()
    {
        $album = Album::all();

        $dt = DataTables::of($album)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('title', function ($model) {
            return $model->title;
        })
        ->addColumn('description', function ($model) {
            return $model->description;
        })
        ->make(true);

        return $dt;
    }
}
