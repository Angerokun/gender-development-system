<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME_PORTAL;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * credentials
     *
     * @param Request $request
     *
     * @return void
     */
    protected function credentials(Request $request)
    {
        if ($request->email == 'admin@lspu.com') {
            $validation = ['status' => null];
        } else {
            $validation = ['status' => 'Approved'];
        }
        return array_merge($request->only($this->username(), 'password'), $validation);
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo()
    {
        $role = Auth::user()->profile_type;
        switch ($role) {
            case 'App\\Models\\StudentProfile':
                return '/portal/profiling' . '/' .  Auth::user()->profile_id;

                break;
            case 'App\\Models\\AdminProfile':
                return '/portal/dashboard';
                break;
            default:
                return '/home';
                break;
        }
    }
}
