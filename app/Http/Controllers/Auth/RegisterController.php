<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AdminProfile;
use App\Models\FacultyProfile;
use App\Models\NonTeachingProfile;
use App\Models\StudentProfile;
use App\Models\User;
use App\Notifications\RegisterNotification;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/portal/profiling';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        // $this->guard()->login($user);
        return $this->registered($request, $user)
                            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'contact_no' => ['required'],
            'category' => ['required'],
            'gender' => ['required'],
            'department' => ['nullable'],
            'year' => ['nullable'],
            'course' => ['nullable'],
            'section' => ['nullable'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            // 'is_lgbt' => ['nullable']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        //$last_name = (strpos($data['name'], ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $data['name']);
        //$first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $data['name'] ) );

        // switch ($data['category']) {
        //     case 'Admin':
        //         $profile = AdminProfile::firstOrCreate([
        //             'first_name' => $data['first_name'],
        //             'last_name' => $data['last_name'],
        //             'contact_no' => $data['contact_no'],
        //             'gender' => $data['gender'],
        //             'department' => $data['department'],
        //             'user_type' => $data['category']
        //         ]);
        //         break;
            // case 'faculty':
            //     $profile = FacultyProfile::firstOrCreate([
            //         'first_name' => $data['first_name'],
            //         'last_name' => $data['last_name'],
            //         'contact_no' => $data['contact_no'],
            //         'gender' => $data['gender'],
            //         'department' => $data['department'],
            //     ]);
            //     break;
            // case 'non-teaching':
            //     $profile = NonTeachingProfile::firstOrCreate([
            //         'first_name' => $data['first_name'],
            //         'last_name' => $data['last_name'],
            //         'contact_no' => $data['contact_no'],
            //         'gender' => $data['gender'],
            //         'department' => $data['department'],
            //     ]);
        //         break;
        //     case 'Student' || 'Non-Teaching' || 'Faculty':
        //         $studentID = date('Ymd') . time();
        //         $profile = StudentProfile::firstOrCreate([
        //             'student_id' => $studentID,
        //             'first_name' => $data['first_name'],
        //             'last_name' => $data['last_name'],
        //             'contact_no' => $data['contact_no'],
        //             'gender' => $data['gender'],
        //             'year' => $data['year'],
        //             'section' => $data['section'],
        //             'course' => $data['course'],
        //             'department' => $data['department'],
        //             'id_no' => $data['id_no'],
        //             'user_type' => $data['category']
        //             // 'is_lgbt' => array_key_exists('is_lgbt', $data) ? $data['is_lgbt'] : '0'
        //         ]);
        //         break;

        //     default:
        //         # code...
        //         break;
        // }

        $studentID = date('Ymd') . time();

        $profile = StudentProfile::firstOrCreate([
            'student_id' => $studentID,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'contact_no' => $data['contact_no'],
            'gender' => $data['gender'],
            'year' => $data['year'],
            'section' => $data['section'],
            'course' => $data['course'],
            'department' => $data['department'],
            'id_no' => $data['id_no'],
            'user_type' => $data['category']
        ]);

        $user = $profile->user()->create([
            'name' => $data['first_name'] . ' ' . $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status' => 'Waiting for Approval'
        ]);

        User::where('id', 1)->first()->notify(new RegisterNotification($user));

        return $user;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo()
    {
        return '/register-verification';
    }
}
