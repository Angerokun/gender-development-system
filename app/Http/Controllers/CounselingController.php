<?php

namespace App\Http\Controllers;

use App\Models\Counseling;
use App\Models\CounselingRequest;
use App\Models\StudentProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class CounselingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counseling_request = CounselingRequest::all();

        return view('portal.individual-counseling.index', [
            'counseling_requests' => $counseling_request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = StudentProfile::all();
        return view('portal.individual-counseling.create', [
            'students' => $students
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $counseling = Counseling::create([
            'background_case' => $request->background_case,
            'goal' => $request->goal,
            'approach' => $request->approach,
            'comment' => $request->comment,
            'recommendation' => $request->recommendation,
            'types' => $request->types
        ]);

        $counseling->counselingItems()->create([
            'student_profile_id' => $request->student_id
        ]);

        return array(
            "status" => "success",
            'data' => $counseling,
            'desc' => 'Counseling Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $counseling = Counseling::findOrFail($id);

        return view(
            'portal.individual-counseling.show',
            [
                'counseling' => $counseling
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $counseling = Counseling::findOrFail($id);
        return view(
            'portal.individual-counseling.edit',
            [
                'counseling' => $counseling
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $counseling = Counseling::findOrFail($id);

        $counseling->update([
            'background_case' => $request->background_case,
            'goal' => $request->goal,
            'approach' => $request->approach,
            'comment' => $request->comment,
            'recommendation' => $request->recommendation,
            'types' => $request->types
        ]);

        return array(
            "status" => "success",
            'data' => $counseling,
            'desc' => 'Counseling Successfully Created!'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * datatables
     *
     * @return void
     */
    public function individualCounselingDatatable()
    {
        if (Auth::user()->profile_type == 'App\Models\StudentProfile') {
            $counselings = Counseling::all();

            $ids = array();

            if (count($counselings) > 0) {
                foreach ($counselings as $counseling) {
                    if ($counseling->counselingRequest->student_profile_id == Auth::user()->profile_id) {
                        $ids[] = $counseling->id;
                    }
                }
            }
            $counselings = Counseling::whereIn('id', $ids)->get();
        } else {
            $counselings = Counseling::all();
        }

        $dt = DataTables::of($counselings)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('student_name', function ($model) {
            return $model->counselingRequest->student->first_name . ' ' . $model->counselingRequest->student->last_name ;
        })
        ->addColumn('background_case', function ($model) {
            return $model->background_case;
        })
        ->addColumn('goal', function ($model) {
            return $model->goal;
        })
        ->addColumn('approach', function ($model) {
            return $model->approach;
        })
        ->addColumn('types', function ($model) {
            return $model->types;
        })
        ->addColumn('date_created', function ($model) {
            return date('F d, Y h:i A', strtotime($model->created_at));
        })
        ->make(true);

        return $dt;
    }
}
