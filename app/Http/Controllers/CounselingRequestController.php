<?php

namespace App\Http\Controllers;

use App\ITextMo;
use App\Models\Counseling;
use App\Models\CounselingRequest;
use App\Models\StudentProfile;
use App\Models\User;
use App\Notifications\ApprovalCounselingNotification;
use App\Notifications\CounselingNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class CounselingRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.counseling-request.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = StudentProfile::all();
        return view('portal.counseling-request.create', [
            'students' => $students
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_no = date('Ymd') . time();

        $counselingRequest = CounselingRequest::create([
            'student_profile_id' => $request->student_id,
            'request_no' => $request_no,
            'status' => 'Pending',
            'counseling_type' => $request->counseling_type,
            'description' => $request->description
        ]);

        User::where('id', 1)->first()->notify(new CounselingNotification($counselingRequest));

        return array(
            "status" => "success",
            'data' => $counselingRequest,
            'desc' => 'Request Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $counselingRequest = CounselingRequest::findOrFail($id);

        return view('portal.counseling-request.show', [
            'counseling_request' => $counselingRequest
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Datatable
     *
     * @return void
     */
    public function counselingRequestDatatable()
    {
        if (Auth::user()->profile_type == 'App\Models\StudentProfile') {
            $counselings = CounselingRequest::all();

            $ids = array();

            if (count($counselings) > 0) {
                foreach ($counselings as $counseling) {
                    if ($counseling->student_profile_id == Auth::user()->profile_id) {
                        $ids[] = $counseling->id;
                    }
                }
            }
            $counselings = CounselingRequest::whereIn('id', $ids)->get();
        } else {
            $counselings = CounselingRequest::where('status', '=', 'Pending')->get();
        }

        $dt = DataTables::of($counselings)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('student_name', function ($model) {
            return $model->student->first_name . ' ' . $model->student->last_name;
        })
        ->addColumn('request_no', function ($model) {
            return $model->request_no;
        })
        ->addColumn('counseling_type', function ($model) {
            return $model->counseling_type;
        })
        ->addColumn('status', function ($model) {
            return $model->status;
        })
        ->addColumn('date_request', function ($model) {
            return date('F d, Y h:i A', strtotime($model->created_at));
        })
        ->make(true);

        return $dt;
    }

    /**
     * status update view
     *
     * @param $id
     * @param $status
     *
     * @return void
     */
    public function statusUpdate($id, $status)
    {
        $counselingRequest = CounselingRequest::findOrFail($id);

        return view('portal.counseling-request.approval', [
            'counseling_request' => $counselingRequest,
            'status' => $status
        ]);
    }

    /**
     * update record
     *
     * @param Request $request
     * @param $id
     * @param $status
     *
     * @return void
     */
    public function updateCounseling(Request $request, $id, $status)
    {
        if ($status == 'Approved') {
            $counseling = Counseling::create([
                'counseling_request_id' => $id,
                'schedule_date' => $request->schedule_date,
                'meeting_type' => $request->meeting_type,
                'meeting_link' => $request->meeting_link,
                'conducted_by' => $request->conducted_by,
                'background_case' => $request->background_case
            ]);
        }
        $counseling_request = CounselingRequest::findOrFail($id);

        $counseling_request->update([
            'status' => $status
        ]);

        User::where('id', $counseling_request->student->user->id)->first()->notify(new ApprovalCounselingNotification($counseling_request));

        if ($status == 'Approved') {
            // $to_name = 'Angelo Zamora';
            // $to_email = 'olegnaaromaz16@gmail.com';
            // $data = array('name' => 'Ogbonna Vitalis(sender_name)', 'body' => 'A test mail');
            // Mail::send(
            //     'portal.emails.mail',
            //     $data,
            //     function ($message) use ($to_name, $to_email) {
            //         $message->to($to_email, $to_name)->subject('Laravel Test Mail');

            //         $message->from(env('MAIL_USERNAME'), 'Test Mail');
            //     }
            // );

            return array(
                "status" => "success",
                'data' => $counseling,
                'desc' => 'Counseling Successfully Approved!',
                'redirect' => true,
                'route' => route('counseling-request.show', $id)
            );
        }

        return redirect(route('counseling-request.show', $id));
    }
}
