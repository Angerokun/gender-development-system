<?php

namespace App\Http\Controllers;

use App\Models\Counseling;
use App\Models\MiniLibraryItem;
use App\Models\Seminar;
use App\Models\StudentProfile;
use App\Models\VisitorLog;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $student_count = StudentProfile::all()->count();
        $visitor_log = VisitorLog::all()->count();
        $seminars = Seminar::all()->count();
        $lgbt = StudentProfile::where('gender', 'LGBTQ')->count();
        $male = StudentProfile::where('gender', 'Male')->where('is_lgbt', '0')->count();
        $female = StudentProfile::where('gender', 'Female')->where('is_lgbt', '0')->count();
        $counselings = Counseling::count();
        $visitor_log_per_day = VisitorLog::whereDate('created_at', Carbon::today())->count();

        $gender_percentage = [];

        if ($student_count > 0) {
            $gender_percentage[] = $lgbt / $student_count * 100;

            $gender_percentage[] = $male / $student_count * 100;

            $gender_percentage[] = $female / $student_count * 100;
        }

        $now = Carbon::now();

        $weekStartDate = $now->startOfWeek()->format('Y-m-d');
        $weekEndDate = $now->endOfWeek()->format('Y-m-d');

        $prevWeekEndDate = $now->subDays($now->dayOfWeek)->subWeek();


        $startOfLastWeek  = $prevWeekEndDate->copy()->subDays(7);
        $startOfLastWeek  = Carbon::now()->subDays(7)->startOfWeek();

        $activity_chart_this_week = array();
        $activity_chart_prev_week = array();

        $begin = new DateTime($weekStartDate);
        $end = new DateTime($weekEndDate);

        $interval = DateInterval::createFromDateString('1 day');
        $period1 = new DatePeriod($begin, $interval, $end->modify('+1 day'));

        foreach ($period1 as $dt) {
            $count = VisitorLog::whereDate('created_at', $dt->format('Y-m-d'))->count();
            $activity_chart_this_week[] = $count;
        }

        $beginprev = new DateTime($startOfLastWeek);
        $endprev = new DateTime($prevWeekEndDate);

        $interval = DateInterval::createFromDateString('1 day');
        $period2 = new DatePeriod($beginprev, $interval, $endprev);

        foreach ($period2 as $dt) {
            $count = VisitorLog::whereDate('created_at', $dt->format('Y-m-d'))->count();
            $activity_chart_prev_week[] = $count;
        }

        $borrowers = MiniLibraryItem::orderBy('created_at', 'DESC')->get()->take(10);

        return view('portal.index', [
            'student_count' => $student_count,
            'visitor_count' => $visitor_log,
            'seminar_count' => $seminars,
            'count_lgbt' => $lgbt,
            'count_male' => $male,
            'count_female' => $female,
            'count_counseling' => $counselings,
            'count_visitor_per_day' => $visitor_log_per_day,
            'pie_gender' => json_encode($gender_percentage),
            'activity_chart' => [
                'this_week' => $activity_chart_this_week,
                'prev_week' => $activity_chart_prev_week
            ],
            'borrowers' => $borrowers
        ]);
    }
}
