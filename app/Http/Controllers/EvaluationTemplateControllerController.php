<?php

namespace App\Http\Controllers;

use App\Models\EvaluationTemplateController;
use Illuminate\Http\Request;

class EvaluationTemplateControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EvaluationTemplateController  $evaluationTemplateController
     * @return \Illuminate\Http\Response
     */
    public function show(EvaluationTemplateController $evaluationTemplateController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EvaluationTemplateController  $evaluationTemplateController
     * @return \Illuminate\Http\Response
     */
    public function edit(EvaluationTemplateController $evaluationTemplateController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EvaluationTemplateController  $evaluationTemplateController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EvaluationTemplateController $evaluationTemplateController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EvaluationTemplateController  $evaluationTemplateController
     * @return \Illuminate\Http\Response
     */
    public function destroy(EvaluationTemplateController $evaluationTemplateController)
    {
        //
    }
}
