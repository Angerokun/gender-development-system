<?php

namespace App\Http\Controllers;

use App\Models\FileDoc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class FileDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.file-docs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portal.file-docs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $file_name = $file->getClientOriginalName();
        $fileUrl = now()->timestamp . "_" . $file_name . '.' . $file->getClientOriginalExtension();

        $file_doc = FileDoc::create([
            'name' => $request->name,
            'user_id' => Auth::user()->id,
            'file' => $file_name,
            'url' => $fileUrl,
            'description' => $request->description,
            'is_view_user' => $request->has('is_view_user') ? '1' : '0'
        ]);

        $request->file->move(storage_path('app/public/'), $file_name);

        return array(
            "status" => "success",
            'data' => $file_doc,
            'redirect' => true,
            'route' => route('file-doc.index'),
            'desc' => 'File Successfully Uploaded!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * datatables
     *
     * @return void
     */
    public function fileDocDatatable()
    {
        if (Auth::user()->profile_type == 'App\Models\StudentProfile') {
            $file_doc = FileDoc::where('user_id', Auth::user()->id)
                ->orWhere('is_view_user', 1)->get();
        } else {
            $file_doc = FileDoc::all();
        }

        $dt = DataTables::of($file_doc)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->name;
        })
        ->addColumn('url', function ($model) {
            return $model->url;
        })
        ->addColumn('date_added', function ($model) {
            return date('F d, Y h:i A', strtotime($model->created_at));
        })
        ->addColumn('user', function ($model) {
            return $model->user->name;
        })
        ->make(true);

        return $dt;
    }

    /**
     * @param $docId
     * @return void
     */
    public function downloadDocs($docId)
    {
        $document = FileDoc::findOrFail($docId);

        if ($document) {
            return response()->download(storage_path('app/public/' . $document->file), $document->file_name);
        }
    }
}
