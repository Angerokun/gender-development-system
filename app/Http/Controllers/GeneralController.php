<?php

namespace App\Http\Controllers;

use App\Models\General;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $general = General::first();

        return view('portal.general.index', [
            'general' => $general
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $general = General::first();

        $general = $general->update([
            'contact_address' => $request->contact_address,
            'contact_email' => $request->contact_email,
            'contact_number' => $request->contact_number,
            'fb_page' => $request->fb_page,
            'twitter' => $request->twitter,
            'gmail' => $request->gmail,
            'linkedin' => $request->linkedin,
            'tagline' => $request->tagline,
            'youtube_url' => $request->youtube_url,
            'mission' => $request->mission,
            'vision' => $request->vision,
            'core_values' => $request->core_values
        ]);

        return array(
            "status" => "success",
            'data' => $general,
            'desc' => 'General Setting Successfully Update!'
        );
    }

    public function getSetting()
    {
        return General::first();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
