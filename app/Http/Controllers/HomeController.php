<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\General;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $general = General::first();
        $albums = Album::all();
        return view('home', [
            'general' => $general,
            'albums' => $albums
        ]);
    }

    public function privacyPolicy()
    {
        return view('privacy-policy');
    }
}
