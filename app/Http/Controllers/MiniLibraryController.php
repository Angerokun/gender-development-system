<?php

namespace App\Http\Controllers;

use App\Models\MiniLibrary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class MiniLibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.mini-library.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mini_library = MiniLibrary::count();

        return view('portal.mini-library.create', [
            'book_no' => str_pad($mini_library + 1, 8, '0', STR_PAD_LEFT)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mini_library = MiniLibrary::create([
            'book_no' => $request->book_no,
            'title' => $request->title,
            'description' => $request->description,
            'no_of_copies' => $request->no_of_copies,
            'author' => $request->author,
            'date_publication' => $request->date_publication,
            'publishing_company' => $request->publishing_company
        ]);

        return array(
            "status" => "success",
            'data' => $mini_library,
            'book_count' => str_pad(MiniLibrary::count() + 1, 8, '0', STR_PAD_LEFT),
            'desc' => 'Mini Library Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mini_library = MiniLibrary::findOrFail($id);

        $no_borrow = $mini_library->items()->whereNull('return_date')->value('no_of_copies');
        if ($no_borrow == null) {
            $no_borrow = 0;
        }

        $arr = [];
        if (( $mini_library->no_of_copies - $no_borrow) == 0) {
            foreach ($mini_library->items()->get() as $key => $value) {
                $arr[] = $value->due_date;
            }
        }


        return view('portal.mini-library.show', [
            'mini_library' => $mini_library,
            'no_borrow' => $no_borrow,
            'date_available' => $arr

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mini_library = MiniLibrary::findOrFail($id);

        return view('portal.mini-library.edit', [
            'mini_library' => $mini_library
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mini_library = MiniLibrary::findOrFail($id);

        $mini_library->update([
            'book_no' => $request->book_no,
            'title' => $request->title,
            'description' => $request->description,
            'no_of_copies' => $request->no_of_copies,
            'author' => $request->author,
            'date_publication' => $request->date_publication,
            'publishing_company' => $request->publishing_company
        ]);

        return array(
            "status" => "success",
            'data' => $mini_library,
            'desc' => 'Book Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $mini_library = MiniLibrary::findOrFail($id);

        return view(
            'portal.mini-library.delete',
            ['mini_library' => $mini_library]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mini_library = MiniLibrary::findOrFail($id);

        $mini_library->delete();

        return redirect(route('mini-library.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function miniLibraryDatatable()
    {
        $mini_library = MiniLibrary::all();

        $dt = DataTables::of($mini_library)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('book_no', function ($model) {
            return $model->book_no;
        })
        ->addColumn('title', function ($model) {
            return $model->title;
        })
        ->addColumn('description', function ($model) {
            return $model->description;
        })
        ->addColumn('no_of_copies', function ($model) {
            return $model->no_of_copies;
        })
        ->addColumn('date_publication', function ($model) {
            return $model->date_publication == null ? '-' : date('F d, Y', strtotime($model->date_publication));
        })
        ->make(true);

        return $dt;
    }
}
