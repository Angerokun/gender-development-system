<?php

namespace App\Http\Controllers;

use App\Models\MiniLibraryItem;
use App\Models\StudentProfile;
use App\Models\User;
use App\Notifications\ApprovalBorrowBookNotification;
use App\Notifications\BorrowBookNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class MiniLibraryItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $students = StudentProfile::all();

        return view('portal.mini-library.borrowers.create', [
            'students' => $students,
            'id' => $id
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if (Auth::user()->profile_type == 'App\\Models\\StudentProfile') {
            $borrow = MiniLibraryItem::create([
                'mini_library_id' => $id,
                'student_id' => Auth::user()->profile_id,
                'no_of_copies' => $request->no_of_copies,
                'date_needed' => $request->date_needed,
                'due_date' => $request->return_date,
                'status' => 'Pending'
            ]);
        } else {
            $borrow = MiniLibraryItem::create([
                'mini_library_id' => $id,
                'student_id' => Auth::user()->profile_id,
                'no_of_copies' => $request->no_of_copies,
                'date_needed' => $request->release_date,
                'release_date' => $request->release_date,
                'due_date' => $request->due_date,
                'return_date' => $request->return_date,
                'status' => $request->status,
                'remarks' => $request->remarks
            ]);
        }

        User::where('id', 1)->first()->notify(new BorrowBookNotification($borrow));

        return array(
            "status" => "success",
            'data' => $borrow,
            'desc' => 'Book Successfully Borrow!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrow = MiniLibraryItem::findOrFail($id);

        return view(
            'portal.mini-library.borrowers.show',
            [
                'borrow' => $borrow
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $borrow = MiniLibraryItem::findOrFail($id);

        $borrow->update([
            'status' => $request->status,
            'due_date' => $request->due_date,
            'release_date' => $request->release_date,
            'return_date' => $request->return_date,
            'remarks' => $request->remarks,
            'id_number' => $request->id_number
        ]);

        User::where('id', $borrow->student->user->id)->first()->notify(new ApprovalBorrowBookNotification($borrow));
  
        return array(
            "status" => "success",
            'data' => $borrow,
            'desc' => 'Book Borrow Successfully Updated!',
            'redirect' => true,
            'route' => route('mini-library.show', $id)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function borroBooksDatatable()
    {
        if (Auth::user()->profile_type == 'App\\Models\\AdminProfile') {
            $borrows = MiniLibraryItem::all();
        } else {
            $borrows = MiniLibraryItem::where('student_id', Auth::user()->profile_id)->get();
        }

        $dt = DataTables::of($borrows)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('student', function ($model) {
            return $model->student->first_name . ' ' . $model->student->last_name;
        })
        ->addColumn('no_of_copies', function ($model) {
            return $model->no_of_copies;
        })
        ->addColumn('book', function ($model) {
            return $model->miniLibrary->title;
        })
        ->addColumn('release_date', function ($model) {
            return date('F d, Y', strtotime($model->date_needed));
        })
        ->addColumn('due_date', function ($model) {
            return date('F d, Y', strtotime($model->return_date));
        })
        ->addColumn('status', function ($model) {
            return $model->status;
        })
        ->make(true);

        return $dt;
    }

    /**
     * status update view
     *
     * @param $id
     * @param $status
     *
     * @return void
     */
    public function statusUpdate($id, $status)
    {
        $mini_library_item = MiniLibraryItem::findOrFail($id);

        return view('portal.mini-library.borrowers.approval', [
            'mini_library_item' => $mini_library_item,
            'status' => $status
        ]);
    }

    public function borrowApproved($id, $status)
    {
        $mini_library_item = MiniLibraryItem::findOrFail($id);

        $mini_library_item->update([
            'status' => $status
        ]);

        User::where('id', $mini_library_item->student->user->id)->first()->notify(new ApprovalBorrowBookNotification($mini_library_item));

        return redirect(route('mini-library.index'));
    }

    public function returnBookView($id)
    {
        $mini_library_item = MiniLibraryItem::findOrFail($id);

        return view('portal.mini-library.borrowers.return', [
            'mini_library_item' => $mini_library_item
        ]);
    }

    public function returnBook(Request $request, $id)
    {
        $mini_library_item = MiniLibraryItem::findOrFail($id);

        $mini_library_item->update([
            'return_date' => date('Y-m-d')
        ]);

        return redirect(route('books-borrow.show', $mini_library_item));
    }
}
