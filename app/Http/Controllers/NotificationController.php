<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class NotificationController extends Controller
{
    /**
     * Get unread Notification
     *
     * @return void
     */
    public function getUnreadNotificationCount()
    {
        return Auth::User()->unreadNotifications->count();
    }

    /**
     * Get read all Notifications
     *
     * @return void
     */
    public function readAllNotification()
    {
        return Auth::User()->unreadNotifications->markAsRead();
    }

    public function markAsViewNotification(string $id)
    {
        $notification = Auth::User()->notifications->where('id', $id)->first();
        return $notification->markAsRead();
    }

    public function index()
    {
        return view('portal.notifications.index');
    }
}
