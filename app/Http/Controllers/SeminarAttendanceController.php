<?php

namespace App\Http\Controllers;

use App\Models\Seminar;
use App\Models\SeminarAttendance;
use App\Models\StudentProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SeminarAttendanceController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $id)
    {
        $student_profile = StudentProfile::all();

        $seminar = Seminar::findOrFail($id);

        return view(
            'portal.seminars.attendance.create',
            [
                'students' => $student_profile,
                'id' => $id,
                'seminar' => $seminar
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function attend(Request $request, int $seminar_id)
    {
        $seminar_attendance = SeminarAttendance::where('student_profile_id', Auth::user()->profile_id)
            ->where('seminar_id', $seminar_id)->first();

        $seminar_attendance->update([
            'date_attended' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('seminar.show', $seminar_id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return void
     */
    public function store(Request $request, int $id)
    {
        if ($request->input('student_id') == null) {
            $studentID = date('Ymd') . time();

            $name = trim($request->name);

            $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);

            $first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $name));

            $studentProfile = StudentProfile::create([
                'student_id' => $studentID,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'section' => $request->section,
                'contact_no' => $request->contact_no,
                'gender' => $request->gender
            ]);

            $seminar_attendance = SeminarAttendance::firstOrCreate([
                'seminar_id' => $id,
                'student_profile_id' => $studentProfile->id,
                'date' => $request->date
            ]);
        } else {
            $seminar_attendance = SeminarAttendance::firstOrCreate([
                'seminar_id' => $id,
                'student_profile_id' => $request->student_id,
                'date' => $request->date
            ]);
        }

        $seminar_attendance->update([
            'date_attended' => date('Y-m-d H:i:s')
        ]);

        return array(
            "status" => "success",
            'data' => $seminar_attendance,
            'desc' => 'Attendance Successfully Created!'
        );
    }
}
