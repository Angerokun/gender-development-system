<?php

namespace App\Http\Controllers;

use App\Models\EvaluationTemplate;
use App\Models\Seminar;
use App\Models\SeminarAttendance;
use App\Models\User;
use App\Notifications\SeminarNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Yajra\DataTables\DataTables;

class SeminarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.seminars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $evaluation = EvaluationTemplate::all();

        return view('portal.seminars.create', [
            'evaluations' => $evaluation
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = User::all();

        $ids = array();

        foreach ($users as $user) {
            $user_type = $user->profile()->whereIn('user_type', $request->notify)->first();
            if ($user_type) {
                $ids[] = $user->id;
            }
        }

        $seminar = Seminar::create([
            'name' => $request->name,
            'evaluation_template_id' => 1,
            'date_schedule' => $request->date_schedule,
            'type' => $request->type,
            'venue' => $request->venue,
            'description' => $request->description,
            'notify' => json_encode($request->notify)
        ]);

        if (count($ids) > 0) {
            Notification::send(User::whereIn('id', $ids)->get(), new SeminarNotification($seminar));
        }

        return array(
            "status" => "success",
            'data' => $seminar,
            'desc' => 'Seminar Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seminar = Seminar::findOrFail($id);

        return view('portal.seminars.show', [
            'seminar' => $seminar
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seminar = Seminar::findOrFail($id);
        $evaluation = EvaluationTemplate::all();

        return view('portal.seminars.edit', [
            'seminar' => $seminar,
            'evaluations' => $evaluation
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seminar = Seminar::findOrFail($id);

        $seminar->update([
            'name' => $request->name,
            'evaluation_template_id' => $request->evaluation_template_id,
            'date_schedule' => $request->date_schedule,
            'type' => $request->type,
            'venue' => $request->venue,
            'description' => $request->description
        ]);

        return array(
            "status" => "success",
            'data' => $seminar,
            'desc' => 'Seminar Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $seminar = Seminar::findOrFail($id);

        return view(
            'portal.seminars.delete',
            ['seminar' => $seminar]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seminar = Seminar::findOrFail($id);

        $seminar->delete();

        return redirect(route('seminar.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function seminarDatatable()
    {
        if (Auth::user()->profile_type == 'App\Models\StudentProfile') {
            $seminars = Seminar::all();

            $ids = array();

            if (count($seminars) > 0) {
                foreach ($seminars as $seminar) {
                    if (in_array(Auth::user()->profile->user_type, json_decode($seminar->notify))) {
                        $ids[] = $seminar->id;
                    }
                }
            }
            $seminar = Seminar::whereIn('id', $ids)->get();
        } else {
            $seminar = Seminar::all();
        }

        $dt = DataTables::of($seminar)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('name', function ($model) {
            return $model->name;
        })
        ->addColumn('type', function ($model) {
            return $model->type;
        })
        ->addColumn('date_schedule', function ($model) {
            return date('F d, Y h:i A', strtotime($model->date_schedule));
        })
        ->addColumn('description', function ($model) {
            return $model->description;
        })
        ->addColumn('date_added', function ($model) {
            return $model->venue;
        })
        ->make(true);

        return $dt;
    }

    /**
     * register confirmation
     *
     * @param int $seminar_id
     *
     * @return void
     */
    public function register(int $seminar_id)
    {
        $seminar = Seminar::findOrFail($seminar_id);

        return view(
            'portal.seminars.register',
            [
            'seminar' => $seminar
            ]
        );
    }

    /**
     * join
     *
     * @param int $seminar_id
     *
     * @return void
     */
    public function join(int $seminar_id)
    {
        $seminar_attendance = SeminarAttendance::create([
            'seminar_id' => $seminar_id,
            'student_profile_id' => Auth::user()->profile_id,
            'date' => date('Y-m-d H:i:s')
        ]);

        return redirect(route('seminar.show', $seminar_id));
    }
}
