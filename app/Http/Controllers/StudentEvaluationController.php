<?php

namespace App\Http\Controllers;

use App\Models\EvaluationTemplate;
use App\Models\Seminar;
use App\Models\StudentEvaluation;
use App\Models\StudentEvaluationItem;
use App\Models\StudentProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class StudentEvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.evaluations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($seminar_id)
    {
        $seminar = Seminar::findOrFail($seminar_id);
        $evaluation_template = EvaluationTemplate::findOrFail($seminar->evaluation_template_id);
        $student_profile = StudentProfile::all();

        return view(
            'portal.seminars.evaluation.create',
            [
                'seminar' => $seminar,
                'students' => $student_profile,
                'evaluation_template' => $evaluation_template
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $studentEvaluation = StudentEvaluation::create([
            'seminar_id' => $id,
            'student_id' => $request->student_id,
            'comments' => $request->comments
        ]);

        foreach ($request->input('data_arr') as $key => $value) {
            StudentEvaluationItem::create([
                'student_evaluation_id' => $studentEvaluation->id,
                'evaluation_template_item_id' => $value,
                'mark' => $request->input('is_mark' . $value),
                'essay' => $request->input('is_essay' . $value)
            ]);
        }

        return array(
            "status" => "success",
            'data' => $studentEvaluation,
            'desc' => 'Evaluation Successfully Submitted!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $studentEvaluation = StudentEvaluation::findOrFail($id);

        return view('portal.evaluations.show', [
            'student_evaluation' => $studentEvaluation
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * datatables
     *
     * @return void
     */
    public function evaluationDatatable()
    {
        if (Auth::user()->profile_type == 'App\Models\StudentProfile') {
            $evaluation = StudentEvaluation::where('student_id', Auth::user()->profile_id)->get();
        } else {
            $evaluation = StudentEvaluation::all();
        }

        $dt = DataTables::of($evaluation)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('seminar_name', function ($model) {
            return $model->seminar->name;
        })
        ->addColumn('student_evaluated', function ($model) {
            return $model->student->first_name . ' ' . $model->student->last_name;
        })
        ->addColumn('created_at', function ($model) {
            return date('F d, Y h:i A', strtotime($model->created_at));
        })
        ->make(true);

        return $dt;
    }
}
