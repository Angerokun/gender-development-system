<?php

namespace App\Http\Controllers;

use App\Models\StudentProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use function App\Datatable\action_data;

class StudentProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.profiling.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portal.profiling.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->has('image')) {
            $base64_str = substr($request->image, strpos($request->image, ",") + 1);
            $image = base64_decode($base64_str);
            $filename = "student-" . time() . ".png";
            $request->image->move(storage_path('app/public/'), $filename);
        } else {
            $filename = null;
        }

        $studentID = date('Ymd') . time();

        $studentProfile = StudentProfile::create([
            'image' => $filename,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'student_id' => $studentID,
            'birth_date' => $request->birth_date,
            'contact_no' => $request->contact_no,
            'section' => $request->section,
            'course' => $request->course,
            'year' => $request->year,
            'gender' => $request->gender
            // 'is_lgbt' => $request->has('is_lgbt') ? $request->is_lgbt : '0'
        ]);
        return array(
            "status" => "success",
            'data' => $studentProfile,
            'desc' => 'Student Account Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = StudentProfile::findOrFail($id);

        return view(
            'portal.profiling.show',
            [
                "profiling" =>  $student,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $studentProfile = StudentProfile::findOrFail($id);

        return view(
            'portal.profiling.edit',
            ['profiling' => $studentProfile]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $studentProfile = StudentProfile::findOrFail($id);

        if ($request->has('image')) {
            $base64_str = substr($request->image, strpos($request->image, ",") + 1);
            $image = base64_decode($base64_str);
            $filename = "student-" . time() . ".png";
            $request->image->move(storage_path('app/public/'), $filename);
        } else {
            if ($studentProfile->image != null) {
                $filename = $studentProfile->image;
            } else {
                $filename = null;
            }
        }

        $request->merge([
            'student_id' => $studentProfile->student_id
        ]);

        $studentProfile->update([
            'image' => $filename,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'student_id' => $request->student_id,
            'birth_date' => $request->birth_date,
            'contact_no' => $request->contact_no,
            'course' => $request->course,
            'year' => $request->year,
            'gender' => $request->gender,
            // 'is_lgbt' => $request->has('is_lgbt') ? $request->is_lgbt : '0'
        ]);

        return array(
            "status" => "success",
            'data' => $studentProfile,
            'desc' => 'Student Profile Successfully Updated!'
        );
    }

    /**
     * delete
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $studentProfile = StudentProfile::findOrFail($id);

        return view(
            'portal.profiling.delete',
            ['profiling' => $studentProfile]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $studentProfile = StudentProfile::findOrFail($id);

        $studentProfile->delete();

        return redirect(route('profiling.index'));
    }

    /**
     * datatables
     *
     * @return void
     */
    public function studentProfileDatatable()
    {
        $student_profile = StudentProfile::all();

        $dt = DataTables::of($student_profile)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('image', function ($model) {
            return ($model->image == null ?
                asset('web-design/img/default.png') : route('profiling.image', $model->image));
        })
        ->addColumn('full_name', function ($model) {
            return $model->last_name . ', ' . $model->first_name . ' ' . $model->middle_name;
        })
        ->addColumn('student_id', function ($model) {
            return $model->student_id;
        })
        ->addColumn('gender', function ($model) {
            return $model->gender;
        })
        ->addColumn('section', function ($model) {
            return $model->user_type;
        })
        ->addColumn('contact_no', function ($model) {
            return $model->contact_no;
        })
        ->make(true);

        return $dt;
    }

    /**
     * get Image
     *
     * @param $filename
     *
     * @return void
     */
    public function getImage($filename)
    {
        $file = Storage::disk('public')->get($filename);

        return response($file, 200);
    }
}
