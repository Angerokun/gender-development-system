<?php

namespace App\Http\Controllers;

use App\Models\StudentProfile;
use App\Models\VisitorLog;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class VisitorLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.logs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student_profile = StudentProfile::all();
        return view('portal.logs.create', ['students' => $student_profile]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('student_id') == null) {
            $studentID = date('Ymd') . time();

            $name = trim($request->name);

            $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $name ) );

            $studentProfile = StudentProfile::create([
                'student_id' => $studentID,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'section' => $request->section
            ]);

            $visitorLog = VisitorLog::create([
                'student_profile_id' => $studentProfile->id,
                'date_visit' => $request->date_visit,
                'reason' => $request->reason
            ]);

        } else {
            $visitorLog = VisitorLog::create([
                'student_profile_id' => $request->student_id,
                'date_visit' => $request->date_visit,
                'reason' => $request->reason
            ]);
        }

        return array(
            "status" => "success",
            'data' => $visitorLog,
            'desc' => 'Logs Successfully Created!'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $log = VisitorLog::findOrFail($id);

        return view('portal.logs.show', ['log' => $log]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = VisitorLog::findOrFail($id);

        $log->delete();

        return redirect(route('visitor-log.index'));
    }

    /**
     * delete
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $log = VisitorLog::findOrFail($id);

        return view(
            'portal.logs.delete',
            ['log' => $log]
        );
    }

    /**
     * datatables
     *
     * @return void
     */
    public function visitorLogDatatable()
    {
        $logs = VisitorLog::all();

        $dt = DataTables::of($logs)
        ->addColumn('id', function ($model) {
            return $model->id;
        })
        ->addColumn('image', function ($model) {
            return ($model->student->image == null ?
                asset('web-design/img/default.png') : route('profiling.image', $model->student->image));
        })
        ->addColumn('full_name', function ($model) {
            return $model->student->last_name . ', ' . $model->student->first_name . ' ' . $model->student->middle_name;
        })
        ->addColumn('date_visit', function ($model) {
            return $model->date_visit;
        })
        ->addColumn('remarks', function ($model) {
            return $model->remarks;
        })
        ->addColumn('reason', function ($model) {
            return $model->reason;
        })
        ->make(true);

        return $dt;
    }
}
