<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'albums';

    protected $guarded = [];

    /**
     * items relationship
     *
     * @return HasMany
     */
    public function albumItems(): HasMany
    {
        return $this->hasMany(AlbumItem::class, 'album_id');
    }
}
