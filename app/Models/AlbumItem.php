<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlbumItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'album_items';

    protected $guarded = [];

    /**
     * items relationship
     *
     * @return HasMany
     */
    public function album(): BelongsTo
    {
        return $this->belongsTo(Album::class, 'album_id');
    }
}
