<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Counseling extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'counselings';

    protected $guarded = [];

    /**
     * items relationship
     *
     * @return HasMany
     */
    public function counselingItems(): HasMany
    {
        return $this->hasMany(CounselingItem::class, 'counseling_id');
    }

    /**
     * counseling request
     *
     * @return BelongsTo
     */
    public function counselingRequest(): BelongsTo
    {
        return $this->belongsTo(CounselingRequest::class, 'counseling_request_id');
    }
}
