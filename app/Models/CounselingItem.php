<?php

namespace App\Models;

use App\Models\Counseling;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CounselingItem extends Model
{
    use HasFactory;

    protected $table = 'counseling_items';

    protected $guarded = [];

    /**
     * counseling relationship
     *
     * @return HasMany
     */
    public function counseling(): BelongsTo
    {
        return $this->belongsTo(Counseling::class, 'counseling_id');
    }

    /**
     * student profile relationship
     *
     * @return BelongsTo
     */
    public function student(): BelongsTo
    {
        return $this->belongsTo(StudentProfile::class, 'student_profile_id');
    }
}
