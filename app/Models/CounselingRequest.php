<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class CounselingRequest extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'counseling_requests';

    protected $guarded = [];

    /**
     * counseling relationship
     *
     * @return HasMany
     */
    public function counselings(): HasMany
    {
        return $this->hasMany(Counseling::class, 'counseling_request_id');
    }

    /**
     * student relationship
     *
     * @return BelongsTo
     */
    public function student(): BelongsTo
    {
        return $this->belongsTo(StudentProfile::class, 'student_profile_id');
    }
}
