<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluationTemplateItem extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'evaluation_template_items';

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function evaluationTemplate(): BelongsTo
    {
        return $this->belongsTo(EvaluationTemplate::class, 'evaluation_template_id');
    }
}
