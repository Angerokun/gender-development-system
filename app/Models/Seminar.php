<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seminar extends Model
{
    use HasFactory;

    use SoftDeletes;

    public const TYPE = [
        'SEMINAR' => 'Seminar',
        'TRAINING' => 'Training',
        'WORKSHOP' => 'Workshop'
    ];

    /**
     * @var string
     */
    protected $table = 'seminars';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * attedance for seminar relationship
     *
     * @return HasMany
     */
    public function attendance(): HasMany
    {
        return $this->hasMany(SeminarAttendance::class, 'seminar_id');
    }

    /**
     * evaluation relationship
     *
     * @return HasMany
     */
    public function evaluations(): HasMany
    {
        return $this->hasMany(StudentEvaluation::class, 'seminar_id');
    }

    /**
     * evaluation template relationship
     *
     * @return BelongsTo
     */
    public function evaluationTemplate(): BelongsTo
    {
        return $this->belongsTo(EvaluationTemplate::class, 'evaluation_template_id');
    }
}
