<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeminarAttendance extends Model
{
    use HasFactory;

    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'seminar_attendances';

    /**
     * @var array
     */
    protected $guarded = [];


    /**
     * Seminar Relationship
     *
     * @return BelongsTo
     */
    public function seminar(): BelongsTo
    {
        return $this->belongsTo(Seminar::class, 'seminar_id');
    }

    /**
     * student relationship
     *
     * @return BelongsTo
     */
    public function student(): BelongsTo
    {
        return $this->belongsTo(StudentProfile::class, 'student_profile_id');
    }
}
