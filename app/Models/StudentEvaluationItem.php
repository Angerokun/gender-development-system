<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentEvaluationItem extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'student_evaluation_items';

    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function studentEvaluation(): BelongsTo
    {
        return $this->belongsTo(StudentProfile::class, 'student_evaluation_id');
    }

    /**
     * @return BelongsTo
     */
    public function evaluationTemplateItems(): BelongsTo
    {
        return $this->belongsTo(EvaluationTemplateItem::class, 'evaluation_template_item_id');
    }
}
