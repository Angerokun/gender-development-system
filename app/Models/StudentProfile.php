<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentProfile extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'student_profiles';

    protected $guarded = [];

    /**
     * morph one to user model
     *
     * @return MorphOne
     */
    public function user(): MorphOne
    {
        return $this->morphOne(User::class, "profile");
    }

    /**
     * logs
     *
     * @return HasMany
     */
    public function logs(): HasMany
    {
        return $this->hasMany(VisitorLog::class, 'student_profile_id');
    }

    /**
     * seminars
     *
     * @return HasMany
     */
    public function seminars(): HasMany
    {
        return $this->hasMany(SeminarAttendance::class, 'student_profile_id');
    }

    /**
     * counselings
     *
     * @return HasMany
     */
    public function counselings(): HasMany
    {
        return $this->hasMany(CounselingItem::class, 'counseling_id');
    }
}
