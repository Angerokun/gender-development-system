<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApprovalCounselingNotification extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'approval_counseling',
            'id'         => $this->request->id,
            'profile_name' => 'GAD LSPU',
            'counseling_type' => $this->request->counseling_type,
            'request_no'  => $this->request->request_no,
            'details' => 'Your Counseling Request was ' . $this->request->status . '. click this to see details.',
            'route' => route('counseling-request.show', $this->request->id),
            'created_at' => $this->request->created_at
        ];
    }
}
