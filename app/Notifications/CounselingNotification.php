<?php

namespace App\Notifications;

use App\Models\StudentProfile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CounselingNotification extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'counseling',
            'id'         => $this->request->id,
            'student_profile_id' => $this->request->student_profile_id,
            'profile_name' => $this->getProfile($this->request->student_profile_id),
            'counseling_type' => $this->request->counseling_type,
            'request_no'  => $this->request->request_no,
            'details' => 'Request for Counseling (' . $this->request->counseling_type . ').',
            'route' => route('counseling-request.show', $this->request->id),
            'created_at' => $this->request->created_at
        ];
    }

    public function getProfile($student_id)
    {
        $student_profile = StudentProfile::where('id', $student_id)->withTrashed()->first();

        return $student_profile->first_name . ' ' . $student_profile->last_name;
    }
}
