<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SeminarNotification extends Notification
{
    use Queueable;

    protected $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'notif_type' => 'seminar',
            'id' => $this->request->id,
            'profile_name' => 'GAD LSPU',
            'name' => $this->request->name,
            'venue' => $this->request->venue,
            'details' => 'Join to our Seminar for' . $this->request->name . ' at ' .
                $this->request->venue . ' on ' . date('M d, Y H:i A', strtotime($this->request->date_schedule)),
            'date_schedule' => $this->request->date_schedule,
            'route' => route('seminar.show', $this->request->id),
            'created_at' => $this->request->created_at
        ];
    }
}
