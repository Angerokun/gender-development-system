<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_profiles', function (Blueprint $table) {
            $table->id();
            $table->string('student_id')->unique()->index();
            $table->string('user_type');
            $table->string('department')->nullable();
            $table->string('id_no')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('is_lgbt')->default(0);
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('section')->nullable();
            $table->string('course')->nullable();
            $table->string('year')->nullable();
            $table->string('gender')->nullable();
            $table->string('contact_no')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_profiles');
    }
}
