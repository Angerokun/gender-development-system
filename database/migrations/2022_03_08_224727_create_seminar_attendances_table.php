<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeminarAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminar_attendances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('seminar_id');
            $table->unsignedBigInteger('student_profile_id');
            $table->dateTime('date');
            $table->dateTime('date_attended')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('seminar_id')
                ->references('id')
                ->on('seminars')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('student_profile_id')
                ->references('id')
                ->on('student_profiles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminar_attendances');
    }
}
