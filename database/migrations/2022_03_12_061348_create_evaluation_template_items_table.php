<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluationTemplateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_template_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('evaluation_template_id');
            $table->unsignedBigInteger('evaluation_template_item_id')->nullable();
            $table->text('questionaire');
            $table->integer('is_essay')->default(0);
            $table->integer('is_mark')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('evaluation_template_id')
                ->references('id')
                ->on('evaluation_templates')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('evaluation_template_item_id')
                ->references('id')
                ->on('evaluation_template_items')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_template_items');
    }
}
