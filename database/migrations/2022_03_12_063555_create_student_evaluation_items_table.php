<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentEvaluationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_evaluation_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_evaluation_id');
            $table->unsignedBigInteger('evaluation_template_item_id');
            $table->integer('mark')->nullable();
            $table->string('essay')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('student_evaluation_id')
                ->references('id')
                ->on('student_evaluations')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('evaluation_template_item_id')
                ->references('id')
                ->on('evaluation_template_items')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_evaluation_items');
    }
}
