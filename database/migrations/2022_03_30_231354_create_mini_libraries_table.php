<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMiniLibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mini_libraries', function (Blueprint $table) {
            $table->id();
            $table->string('book_no');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('no_of_copies');
            $table->string('author')->nullable();
            $table->date('date_publication')->nullable();
            $table->string('publishing_company')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mini_libraries');
    }
}
