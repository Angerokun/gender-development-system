<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMiniLibraryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mini_library_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mini_library_id');
            $table->unsignedBigInteger('student_id');
            $table->string('no_of_copies');
            $table->date('date_needed');
            $table->date('release_date')->nullable();
            $table->date('due_date')->nullable();
            $table->date('return_date')->nullable();
            $table->string('status');
            $table->string('id_number')->nullable();
            $table->string('remarks')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('mini_library_id')
                ->references('id')
                ->on('mini_libraries')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('student_id')
                ->references('id')
                ->on('student_profiles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mini_library_items');
    }
}
