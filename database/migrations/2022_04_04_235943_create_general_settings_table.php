<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_settings', function (Blueprint $table) {
            $table->id();
            $table->string('contact_address');
            $table->string('contact_email');
            $table->string('contact_number');
            $table->text('fb_page')->nullable();
            $table->text('twitter')->nullable();
            $table->text('gmail')->nullable();
            $table->text('linkedin')->nullable();
            $table->text('tagline');
            $table->text('youtube_url')->nullable();
            $table->text('mission');
            $table->text('vision');
            $table->text('core_values');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_settings');
    }
}
