<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCounselingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counselings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('counseling_request_id');
            $table->dateTime('schedule_date');
            $table->string('meeting_type');
            $table->string('meeting_link')->nullable();
            $table->string('conducted_by')->nullable();
            $table->longText('background_case')->nullable();
            $table->longText('goal')->nullable();
            $table->string('approach')->nullable();
            $table->string('comment')->nullable();
            $table->string('recommendation')->nullable();
            $table->string('types')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('counseling_request_id')
                ->references('id')
                ->on('counseling_requests')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counselings');
    }
}
