<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCounselingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counseling_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('counseling_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('relationship');
            $table->string('contact_no');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('counseling_id')
                ->references('id')
                ->on('counselings')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counseling_items');
    }
}
