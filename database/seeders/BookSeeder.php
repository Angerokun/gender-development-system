<?php

namespace Database\Seeders;

use App\Models\MiniLibrary;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $file = __DIR__ . '/books.csv';

        if (MiniLibrary::count() > 0) {
            return;
        }

        ini_set('auto_detect_line_endings', true);
        $handle = fopen($file, 'r');
        while (($data = fgetcsv($handle)) !== false) {
            if ($data[3] != '') {
                $no_copies = explode(" ", $data[3]);
                $copies = $no_copies[0];
            } else {
                $copies = 0;
            }

            MiniLibrary::create([
                'book_no' => $data[0],
                'title' => $data[1],
                'description' => $data[2],
                'no_of_copies' => $copies,
                'author' => $data[4],
                'date_publication' => $data[5] != '' ? date('Y-m-d H:i:s', strtotime($data[5])) : null,
                'publishing_company' => $data[6]
            ]);
        }

        ini_set('auto_detect_line_endings', false);
    }
}
