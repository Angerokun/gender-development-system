<?php

namespace Database\Seeders;

use App\Models\EvaluationTemplate;
use App\Models\EvaluationTemplateItem;
use Illuminate\Database\Seeder;

class EvaluationTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluation = EvaluationTemplate::create([
            'name' => 'GAD SEMINAR/WORKSHOP/TRAINING Evaluation Sheet',
            'instructions' => 'Please evaluate the overall activities by checking the appropriate columns where:
            (Kailangan naming ang inyong pagtataya para sa pangkalahatang aktibidades, markahan ng tsek ang naaangkop sa inyong kasagutan ayon sa sumusunod na sanggunian)'
        ]);

        $item = EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => null,
            'questionaire' => 'I. Content/Paksa ng Seminar/Training :'
        ]);

        $item2 = EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => null,
            'questionaire' => 'II. Materials /Kagamitang Pantalakay'
        ]);

        $item3 = EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => null,
            'questionaire' => 'III.	Resource Person/ Tagapagsalita'
        ]);

        $item4 = EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => null,
            'questionaire' => 'IV.	Meals, Venue, Facilities and Others/ Lugar, Kagamitan at inihandang Pagkain'
        ]);

        $item5 = EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => null,
            'questionaire' => 'V. Over all Evaluation/Pangkalahatang Pagtataya'
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item5->id,
            'questionaire' => 'The program objectives are attained. (Ang mga layunin ng programa ay nakamit)',

            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item5->id,
            'questionaire' => 'The entire program is well-organized. (Maayos ang pagdaos ng programa.)',
            'is_mark' => 1
        ]);


        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item4->id,
            'questionaire' => 'Meals are well-presented, satisfying and served on time. Maayos ang pagkakahanda at nasa oras ang serbisyo ng pagkain.)',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item4->id,
            'questionaire' => 'Facilities, sound system and other needed equipment are functional. (Maayos ang pasilidad at mga kagamitan).',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item4->id,
            'questionaire' => 'Participants are comfortable. (Komportable ang mga kalahok.)',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item4->id,
            'questionaire' => 'The venue has proper lighting and ventilation. (Kaaya-aya ang lugar na pinagdausan ng programa.)',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item3->id,
            'questionaire' => 'Motivated the participants to actively involve. (Nahikayat ang mga dumalo na aktibong lumahok sa gawain.)',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item3->id,
            'questionaire' => 'Well-poised, alert and can hold participants’ attention. (May sigla at buhay ang paraan ng pagtalakay).',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item3->id,
            'questionaire' => 'Concept were clearly discussed. (Malinaw na natalakay at naipaliwanag ang mga konsepto). ',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item3->id,
            'questionaire' => 'Knowledgeable and well-versed in the topic. (Malawak ang kaalaman ukol sa paksa). ',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item2->id,
            'questionaire' => 'The materials are available and prepared before the activity start. (Kumpleto ang lahat ng kinakailangang kagamitan)',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item2->id,
            'questionaire' => 'The visual aids and handouts were helpful in facilitating inputs and generating outputs. (Lubos na nakatulong ang mga visuals at handouts).',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item->id,
            'questionaire' => 'The topics are well organized and systematically discussed (Ang paksa ay natalakay nang kumpleto at detalyado)',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item->id,
            'questionaire' => 'The topic are relevant and vital to my work/task. (Ang mga paksang natalakay ay mahalaga at kapaki-pakinabang).',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item->id,
            'questionaire' => 'The topics are timely. (Napapanahon ang mga impormasyong ibinahagi).',
            'is_mark' => 1
        ]);

        EvaluationTemplateItem::create([
            'evaluation_template_id' => $evaluation->id,
            'evaluation_template_item_id' => $item->id,
            'questionaire' => 'Which among the topics you considered are significant to you? Write it on the space provided. (Alin sa mga Paksang Natalakay ang pinakamahalaga para sa iyo? Isulat sa ibaba).',
            'is_essay' => 1
        ]);
    }
}
