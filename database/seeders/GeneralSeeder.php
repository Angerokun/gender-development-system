<?php

namespace Database\Seeders;

use App\Models\General;
use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        General::create([
            'contact_address' => 'Laguna State Polytechnic University L.De leon St. Siniloan,Laguna',
            'contact_email' => 'gelzen.jamolin@lspu.edu.ph',
            'contact_number' => '0949-3178018',
            'fb_page' => 'https://web.facebook.com/LSPU-Siniloan-Gender-Development-101389475057899/?_rdc=1&_rdr',
            'twitter' => null,
            'gmail' => null,
            'linkedin' => null,
            'tagline' => 'We are the lead State University steering development efforts towards women`s empowerment and gender equality.',
            'youtube_url' => null,
            'mission' => "We undertake to design and implement policies and programs geared towards promoting gender equality and equity, protecting the rights of children and enhancing the quality of life of women and men equally participating in the development, management and operation of globally competitive output, thus uplifting their overall development and promoting the welfare of families and communities.",
            'vision' => "We are the lead State University steering development efforts towards women's empowerment and gender equality.",
            'core_values' => "1.Respect
                                2. Accountability
                                3. Equality
                                4. Empowerment
                                5. Professionalism
                                6. Honesty
                                7. Commitment
                                8. Teamwork"
        ]);
    }
}
