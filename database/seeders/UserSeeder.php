<?php

namespace Database\Seeders;

use App\Models\AdminProfile;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = AdminProfile::firstOrCreate([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'user_type' => 'Admin'
        ]);

        $admin->user()->firstOrCreate([
            'name' => 'John Doe',
            'email' => 'admin@lspu.com',
            'password' => Hash::make('password123')
        ]);
    }
}
