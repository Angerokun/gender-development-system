$.fn.register_fields = function (class_container)
{
    var form_id = $(this);
    $.validate({
        modules: 'logic',
        form : form_id,
        showHelpOnFocus : false,
        showErrorDialogs: false,
        validateHiddenInputs: true,
        onError : function($form) {
            toastMessage('Oopps!','It seems like you missed some required fields',"error")
            var container = "<div class='alert alert-danger col-sm-12'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>It seems like you missed some required fields</div>";
            $(class_container).html(container);
            $(class_container).delay(5000).fadeOut("slow");
            $('html, body').animate({
                scrollTop: 0
            }, 500);
            $(class_container).show();
        },
        onSuccess : function($form) {
            // form_id.submit();
            // alert('The form '+$form.attr('id')+' is valid!');
            var formData = new FormData(form_id[0]); //data from form stored in a variable
            $.ajax({
                type: "POST", //form submission type
                url: form_id.attr('action'), //destination when the form submitted, then will do a php function for the data from the form
                data: formData, //fetch all the data that are inputed to the form
                dataType: "json",
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function()
                {
                    // $(".please_wait").show();
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    var container = "<div class='alert alert-danger col-sm-12'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><ul>";
                    $.each(errors['errors'],function(index,value){
                        // var nj seJSON(value);
                        container += "<li>"+value+"</li>";
                    });
                    container += "</ul></div>";
                    $(class_container).html(container);
                    $(class_container).delay(5000).fadeOut("slow");
                    $('html, body').animate({
                        scrollTop: 0 
                    }, 500);
                    $(class_container).show();
                },
                success: function(data){
                    if (data.status === "success")
                    {
                        form_id.each(function(){
                            this.reset(); //resets the form values
                        });
                        $('.to_hide').attr('hidden',true);
                        // disable_input(form_id);
                        // readonly_input(form_id);
                    }
                    if(data.redirect){
                        // alert()
                        setTimeout(function () {
                            window.location.href = data.route;
                        })
                    }
                    if($(".ifCloseModal")) {

                        $('#myModal_1').modal('hide');

                    }
                    //NOTE: condition for Create-Profile for New register only
                    //-----------------------------------------------------------------------------------------------------------
                    if ($(".ifCloseModal")[0]){
                        $('#myModal').modal('hide');
                        // $('#div_education').remove();
                        // $('#div_employment').remove();
                        var url_modal = $('#user_url').val();
                        $.ajax({
                            type: "GET", 
                            url: url_modal,
                            success: function(html){
                                if($('#modal_function').val() == 'RefreshEducation')
                                {
                                    $('.class_education').empty().html(html)
                                }
                                if($('#modal_function').val() == 'RefreshEmployment')
                                {
                                    $('.class_employment').empty().html(html)
                                }



                            }
                        });


                    }
                    else {
                        toastMessage('Success',data.desc,data.status);
                        // $(class_container).show();
                        // $(class_container).html(data.desc);
                        // $(class_container).delay(5000).fadeOut("slow");
                        //     $('html, body').animate({
                        //         scrollTop: 0 
                        //     }, 500);
                        
                    }

                },
                complete:function()
                {
                    // $(".please_wait").hide();
                }
            });
            return false; // Will stop the submission of the form
        },
        onValidate : function($form) {

        },
        onElementValidate : function(valid, $el, $form, errorMess) {
            // console.log('Input ' +$el.attr('name')+ ' is ' + ( valid ? 'VALID':'NOT VALID') );
            // console.log($el.attr('type'));
            var type = $el.attr('type');
            if (type === "file"){
                var validator = $el.attr('data-validator');
                (valid ? $(validator).css('color','black'):$(validator).css('color','red'))
            }
        }
    });

};

function toastMessage(title,message,toastType){
    toastr.remove();
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr[toastType](message, title);
}

$(document).on('click','.print-form-report',function (e) {
    e.preventDefault();
    let form_id = $(this).attr('data-form');
    $('#'+form_id).removeAttr('hidden');
    printJS({
        printable: form_id,
        type: 'html',
        showModal: true,
        scanStyles: false,
        css: 'http://localhost/gender-development-system/public/css/print-report.css'
    })
    $('#'+form_id).attr('hidden', true);
    return false;
});
