@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="page-banner">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col-md-6">
        <nav aria-label="Breadcrumb">
            <ul class="breadcrumb justify-content-center py-0 bg-transparent">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item active">Albums</li>
            </ul>
        </nav>
        <h1 class="text-center">Albums</h1>
        </div>
    </div>
    </div>
</div>

<div class="page-section">
    <div class="container">
    <div class="row">
        @foreach ($albums as $album)
        <div class="col-md-6 col-lg-4 py-3">
        <div class="card-blog">
            <div class="header">
            <div class="avatar">
                <img src="{{ asset($album->albumItems()->first()->image_url) }}" alt="">
            </div>
            </div>
            <div class="body">
            <div class="post-title">{{ $album->title}}</div>
            <div class="post-excerpt">{{ $album->description }}</div>
            </div>
            <div class="footer">
            <a href="{{ route('albums-web.show', $album->id) }}">Read More <span class="mai-chevron-forward text-sm"></span></a>
            </div>
        </div>
        </div>
        @endforeach

        <!-- <div class="col-12 mt-5">
        <nav aria-label="Page Navigation">
            <ul class="pagination justify-content-center">
            <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active" aria-current="page">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
            </li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item">
                <a class="page-link" href="#">Next</a>
            </li>
            </ul>
        </nav>
        </div> -->

    </div>

    </div>
</div>
@endsection
<style>
    .post-excerpt {
        max-width: 300px; 
        min-width: 100px; 
        overflow: hidden; 
        text-overflow: ellipsis; 
        max-height: 130px;
        min-height: 112px;
    }
</style>