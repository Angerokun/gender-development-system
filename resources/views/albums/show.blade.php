@extends('layouts.app')

@section('content')
<div class="container mt-5" >

<div class="row" style="margin-top:94px;">
    <div class="col-lg-8">
    <div class="">
        <div class="header">
        <div class="bg-light">
            <div class="container">
            <div class="owl-carousel wow fadeInUp" id="testimonials">
                @foreach ($album->albumItems as $item)
                <div class="item">
                <div class="row align-items-center">
                    <div class="col-md-12 py-3">
                    <div class="testi-image-album">
                        <img src="{{ asset($item->image_url) }}" alt="">
                    </div>
                    </div>
                </div>
                </div>
                @endforeach
            </div>
            </div>
        </div>
        <!-- <div class="post-thumb">
            <img src="../assets/img/blog/blog-1.jpg" alt="">
        </div>
        <div class="meta-header">
            <div class="post-author">
            <div class="avatar">
                <img src="../assets/img/person/person_1.jpg" alt="">
            </div>
            by <a href="#">Stephen Doe</a>
            </div>

            <div class="post-sharer">
            <a href="#" class="btn social-facebook"><span class="mai-logo-facebook-f"></span></a>
            <a href="#" class="btn social-twitter"><span class="mai-logo-twitter"></span></a>
            <a href="#" class="btn social-linkedin"><span class="mai-logo-linkedin"></span></a>
            <a href="#" class="btn"><span class="mai-mail"></span></a>
            </div>
        </div> -->
        </div>
        <h1 class="post-title">{{ $album->title }}</h1>
        <div class="post-meta">
                <div class="post-date">
                  <span class="icon">
                    <span class="mai-time-outline"></span>
                  </span> <a href="#">{{ date('F d, Y', strtotime($album->created_at)) }}</a>
                </div>
              </div>
        <div class="post-content">
            <p>{{ $album->description }}</p>
        </div>
    </div>

    <!-- <div class="comment-form-wrap pt-5">
        <h2 class="mb-5">Leave a comment</h2>
        <form action="#" class="">
        <div class="form-row form-group">
            <div class="col-md-6">
            <label for="name">Name *</label>
            <input type="text" class="form-control" id="name">
            </div>
            <div class="col-md-6">
            <label for="email">Email *</label>
            <input type="email" class="form-control" id="email">
            </div>
        </div>
        <div class="form-group">
            <label for="website">Website</label>
            <input type="url" class="form-control" id="website">
        </div>

        <div class="form-group">
            <label for="message">Message</label>
            <textarea name="msg" id="message" cols="30" rows="8" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <input type="submit" value="Post Comment" class="btn btn-primary">
        </div>

        </form>
    </div> -->

    </div>
    <div class="col-lg-4">
    <div class="widget">

        <!-- Widget recent post -->
        <div class="widget-box">
        <h4 class="widget-title">Other Post</h4>
        <div class="divider"></div>
        @if ($albums->where('id', '!=', $album->id)->count() > 0)
        @foreach ($albums->where('id', '!=', $album->id) as $album_item)
        <div class="blog-item">
            <a class="post-thumb" href="">
                <img src="{{ asset($album_item->albumItems()->first()->image_url) }}" alt="">
            </a>
            <div class="content">
                <h6 class="post-title"><a href="{{ route('albums-web.show', $album_item->id) }}">{{ $album_item->title }}</a></h6>
                <div class="meta">
                <a href="{{ route('albums-web.show', $album_item->id) }}"><span class="mai-calendar"></span>{{ date('F d, Y', strtotime($album_item->created_at)) }}</a>
                </div>
            </div>
        </div>
        @endforeach
        @endIf

        </div>

        <!-- Widget Tag Cloud -->
        <!-- <div class="widget-box">
        <h4 class="widget-title">Tag Cloud</h4>
        <div class="divider"></div>

        <div class="tag-clouds">
            <a href="#" class="tag-cloud-link">Projects</a>
            <a href="#" class="tag-cloud-link">Design</a>
            <a href="#" class="tag-cloud-link">Travel</a>
            <a href="#" class="tag-cloud-link">Brand</a>
            <a href="#" class="tag-cloud-link">Trending</a>
            <a href="#" class="tag-cloud-link">Knowledge</a>
            <a href="#" class="tag-cloud-link">Food</a>
        </div>
        </div> -->

    </div>
    </div>
</div>

</div>
@endsection
<style>
    .testi-image-album {
        position: relative;
        display: block;
        margin: 0 auto;
        width: 300px;
        background-color: #645F88;
        border-radius: 8px;
        overflow: hidden;
    }
</style>