@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top: 100px;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Category</label>

                            <div class="col-md-6">
                                <select name="category" id="category" class="form-control" id="" required>
                                    <option value="">Select Category</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Student">Student</option>
                                    <option value="Faculty">Faculty</option>
                                    <option value="Non-Teaching">Non-teaching</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" placeholder="Enter First Name" name="first_name" value="{{ old('first_name') }}" required autocomplete="name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" placeholder="Enter Last Name" name="last_name" value="{{ old('last_name') }}" required autocomplete="name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Contact No') }}</label>

                            <div class="col-md-6">
                                <input id="contact_no" type="text" class="form-control @error('contact_no') is-invalid @enderror" placeholder="Enter Contact Number" name="contact_no" value="{{ old('contact_no') }}" required autocomplete="name" autofocus>

                                @error('contact_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Enter Email Address" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3" id="student_panel1">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Year</label>

                            <div class="col-md-6">
                                <input id="year" type="text" class="form-control " placeholder="Enter Year" name="year" id="year">
                            </div>
                        </div>

                        <div class="row mb-3" id="student_panel2">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Section</label>

                            <div class="col-md-6">
                                <input id="section" type="text" class="form-control " placeholder="Enter Section" name="section" id="section">
                            </div>
                        </div>

                        <div class="row mb-3" id="student_panel3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Course</label>

                            <div class="col-md-6">
                                <input id="course" type="text" class="form-control" placeholder="Enter Course" name="course"  id="course" >
                            </div>
                        </div>

                        <div class="row mb-3" id="department_panel">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Department</label>

                            <div class="col-md-6">
                                <input id="department" type="text" class="form-control" placeholder="Enter Department" name="department" id="department">
                            </div>
                        </div>

                        <div class="row mb-3" id="department_panel1">
                            <label for="name" class="col-md-4 col-form-label text-md-end">ID No.</label>

                            <div class="col-md-6">
                                <input id="id_no" type="text" class="form-control" placeholder="Enter ID No." name="id_no" id="id_no">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">Sex</label>

                            <div class="col-md-6">
                                <select name="gender" class="form-control" id="" required>
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Enter Password" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Enter Password" required autocomplete="new-password">
                            </div>
                        </div>

                        <!-- <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <p style="color: #898798;">By clicking Register, I agree that I have read and accepted the GAD LSPU
                                <a href="{{ route('privacy-policy') }}" target="_blank">Terms of Use and Privacy Policy</a></p>
                            </div>
                        </div> -->

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#student_panel1').hide();
    $('#student_panel2').hide();
    $('#student_panel3').hide();
    $('#department_panel').hide();
    $('#department_panel1').hide();

    $('#category').change(function () {
        var type = $('#category').find(":selected").val();
        if(type == 'Student') {
            $('#student_panel1').show();
            $('#student_panel2').show();
            $('#student_panel3').show();
            $('#department_panel').hide();
            $('#department_panel1').hide();
            $("#year").attr('required', '');  
            $("#section").attr('required', '');  
            $("#course").attr('required', '');  
            $("#department").removeAttr('required');  
            $("#id_no").removeAttr('required');  
        } else if (type == 'Admin' || type == 'Faculty' || type == 'Non-Teaching') {
            $('#student_panel1').hide();
            $('#student_panel2').hide();
            $('#student_panel3').hide();
            $('#department_panel').show();
            $('#department_panel1').show();
            $("#year").removeAttr('required');  
            $("#section").removeAttr('required');  
            $("#course").removeAttr('required');  
            $("#department").attr('required', '');  
            $("#id_no").attr('required', '');  
        } else {
            $('#student_panel1').hide();
            $('#student_panel2').hide();
            $('#student_panel3').hide();
            $('#department_panel').hide();
            $('#department_panel1').hide();
        }
    });
</script>
@endpush
@endsection
