@extends('layouts.app')

@section('content')
<div class="page-banner home-banner">
    <div class="container h-100">
        <div class="row align-items-center h-100">
        <div class="col-lg-6 py-3 wow fadeInUp">
            <h1 class="mb-4">Gender and Development Laguna State Polytechnic University - SC</h1>
            <p class="text-lg mb-5">{{ $general->tagline }}</p>

            <a href="{{ route('about') }}" class="btn btn-outline border text-secondary">More Info</a>
            <a href="{{ $general->youtube_url }}" target="_blank" class="btn btn-primary btn-split ml-2">Watch Video <div class="fab"><span class="mai-play"></span></div></a>
        </div>
        <div class="col-lg-6 py-3 wow zoomIn">
            <div class="img-place">
            <img src="{{ asset('web-design/img/bg_image_1.png') }}" alt="">
            </div>
        </div>
        </div>
    </div>
</div>

<div class="page-section features">
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-lg-4 py-3 wow fadeInUp">
        <div class="d-flex flex-row">
            <div class="img-fluid mr-3">
            <img src="{{ asset('web-design/img/icon_pattern.svg') }}" alt="">
            </div>
            <div>
            <h5>Vision</h5>
            <p>{{ $general->vision }}</p>
            </div>
        </div>
        </div>

        <div class="col-md-6 col-lg-4 py-3 wow fadeInUp">
        <div class="d-flex flex-row">
            <div class="img-fluid mr-3">
            <img src="{{ asset('web-design/img/icon_pattern.svg') }}" alt="">
            </div>
            <div>
            <h5>Mission</h5>
            <p>{{ $general->mission }}</p>
            </div>
        </div>
        </div>

        <div class="col-md-6 col-lg-4 py-3 wow fadeInUp">
        <div class="d-flex flex-row">
            <div class="img-fluid mr-3">
            <img src="{{ asset('web-design/img/icon_pattern.svg') }}" alt="">
            </div>
            <div>
            <h5>Core Values</h5>
            <p>{{ $general->core_values }}</p>
            </div>
        </div>
        </div>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<div class="page-section">
    <div class="container">
    <div class="row">
        <div class="col-lg-6 py-3 wow zoomIn">
        <div class="img-place text-center">
            <img src="{{ asset('web-design/img/bg_image_2.png') }}" alt="">
        </div>
        </div>
        <div class="col-lg-6 py-3 wow fadeInRight">
        <h2 class="title-section">Quality <span class="marked">Objectives</span> </h2>
        <div class="divider"></div>
        <p>
We, at LSPU are committed with continual improvement to provide quality, efficient and effective services to the university stakeholder’s highest level of satisfaction through a dynamic and excellent management system imbued with utmost integrity, professionalism and innovation.
</p>
        <div class="img-place mb-3">
            <img src="{{ asset('web-design/img/testi_image.png') }}" alt="">
        </div>
        <a href="#" class="btn btn-primary">More Details</a>
        <a href="#" class="btn btn-outline border ml-2">Success Stories</a>
        </div>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<!-- <div class="page-section counter-section">
    <div class="container">
    <div class="row align-items-center text-center">
        <div class="col-lg-4">
        <p>Total Invest</p>
        <h2>$<span class="number" data-number="816278"></span></h2>
        </div>
        <div class="col-lg-4">
        <p>Yearly Revenue</p>
        <h2>$<span class="number" data-number="216422"></span></h2>
        </div>
        <div class="col-lg-4">
        <p>Growth Ration</p>
        <h2><span class="number" data-number="73"></span>%</h2>
        </div>
    </div>
    </div>
</div> -->

<div class="page-section">
    <div class="container">
    <div class="row">
        <div class="col-lg-6 py-3 wow fadeInLeft">
        <h2 class="title-section">We're <span class="marked">Commited</span> with continual improvement</h2>
        <div class="divider"></div>
        <p class="mb-5">To provide quality, efficient and effective services to the university stakeholder’s highest level of satisfaction through a dynamic and excellent management system imbued with utmost integrity, professionalism and innovation.</p>
        <a href="{{ route('services') }}" class="btn btn-primary">More Details</a>
        </div>
        <div class="col-lg-6 py-3 wow zoomIn">
        <div class="img-place text-center">
            <img src="{{ asset('web-design/img/bg_image_3.png') }}" alt="">
        </div>
        </div>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<!-- <div class="page-section">
    <div class="container">
    <div class="text-center wow fadeInUp">
        <div class="subhead">Why Choose Us</div>
        <h2 class="title-section">Your <span class="marked">Comfort</span> is Our Priority</h2>
        <div class="divider mx-auto"></div>
    </div>

    <div class="row mt-5 text-center">
        <div class="col-lg-4 py-3 wow fadeInUp">
        <div class="display-3"><span class="mai-shapes"></span></div>
        <h5>High Performance</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, sit.</p>
        </div>
        <div class="col-lg-4 py-3 wow fadeInUp">
        <div class="display-3"><span class="mai-shapes"></span></div>
        <h5>Friendly Prices</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, sit.</p>
        </div>
        <div class="col-lg-4 py-3 wow fadeInUp">
        <div class="display-3"><span class="mai-shapes"></span></div>
        <h5>No time-confusing</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, sit.</p>
        </div>
    </div>
    </div>
</div> -->

<div class="page-section border-top">
    <div class="container">
    <div class="text-center wow fadeInUp">
        <h2 class="title-section">Services Offered for Gender and Development</h2>
        <div class="divider mx-auto"></div>
    </div>

    <div class="row justify-content-center">
        <div class="col-12 col-lg-auto py-3 wow fadeInLeft">
        <div class="card-pricing">
            <div class="header">
            <div class="price-icon"><span class="mai-people"></span></div>
            <div class="price-title">Seminar/Workshop</div>
            </div>
            <div class="body py-3">
            <div class="price-info">
                <p>Attende Seminars or Workshops for Free..</p>
            </div>
            </div>
            <div class="footer">
            <a href="{{ route('login') }}" class="btn btn-outline rounded-pill">Join Now</a>
            </div>
        </div>
        </div>

        <div class="col-12 col-lg-auto py-3 wow fadeInUp">
        <div class="card-pricing active">
            <div class="header">
            <div class="price-labled">Best</div>
            <div class="price-icon"><span class="mai-business"></span></div>
            <div class="price-title">Counseling</div>
            </div>
            <div class="body py-3">
            <div class="price-info">
                <p>counseling is counseling focused on the individual's immediate or near future concerns. Individual counseling may encompass career counseling and planning, grief after a loved one dies or dealing with problems at a job before they become big.</p>
            </div>
            </div>
            <div class="footer">
            <a href="{{ route('login') }}" class="btn btn-outline rounded-pill">Appoint Now</a>
            </div>
        </div>
        </div>

        <div class="col-12 col-lg-auto py-3 wow fadeInRight">
        <div class="card-pricing">
            <div class="header">
            <div class="price-icon"><span class="mai-rocket-outline"></span></div>
            <div class="price-title">Mini Library</div>
            </div>
            <div class="body py-3">

            <div class="price-info">
                <p>Borrow Books using online registration</p>
            </div>
            </div>
            <div class="footer">
            <a href="{{ route('login') }}" class="btn btn-outline rounded-pill">Borrow Book</a>
            </div>
        </div>
        </div>
        
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<!-- <div class="page-section bg-light">
    <div class="container">
    
    <div class="owl-carousel wow fadeInUp" id="testimonials">
        <div class="item">
        <div class="row align-items-center">
            <div class="col-md-6 py-3">
            <div class="testi-image">
                <img src="{{ asset('web-design/img/person/person_1.jpg') }}" alt="">
            </div>
            </div>
            <div class="col-md-6 py-3">
            <div class="testi-content">
                <p>Necessitatibus ipsum magni accusantium consequatur delectus a repudiandae nemo quisquam dolorum itaque, tenetur, esse optio eveniet beatae explicabo sapiente quo.</p>
                <div class="entry-footer">
                <strong>Melvin Platje</strong> &mdash; <span class="text-grey">CEO Slurin Group</span>
                </div>
            </div>
            </div>
        </div>
        </div>

        <div class="item">
        <div class="row align-items-center">
            <div class="col-md-6 py-3">
            <div class="testi-image">
                <img src="{{ asset('web-design/img/person/person_2.jpg') }}" alt="">
            </div>
            </div>
            <div class="col-md-6 py-3">
            <div class="testi-content">
                <p>Repudiandae vero assumenda sequi labore ipsum eos ducimus provident a nam vitae et, dolorum temporibus inventore quaerat consectetur quos! Animi, qui ratione?</p>
                <div class="entry-footer">
                <strong>George Burke</strong> &mdash; <span class="text-grey">CEO Letro</span>
                </div>
            </div>
            </div>
        </div>
        </div>

    </div>
    </div>
</div> -->

<div class="page-section">
    <div class="container">
    <div class="row align-items-center">
        <div class="col-lg-6 py-3 wow fadeInUp">
        <h2 class="title-section">Get in Touch</h2>
        <div class="divider"></div>
        <p>Reach our Offices or Contact us<br> provided below.</p>

        <ul class="contact-list">
            <li>
            <div class="icon"><span class="mai-map"></span></div>
            <div class="content">{{ $general->contact_address }}</div>
            </li>
            <li>
            <div class="icon"><span class="mai-mail"></span></div>
            <div class="content"><a href="#">{{ $general->contact_email }}</a></div>
            </li>
            <li>
            <div class="icon"><span class="mai-phone-portrait"></span></div>
            <div class="content"><a href="#">{{ $general->contact_number }}</a></div>
            </li>
        </ul>
        </div>
        <div class="col-lg-6 py-3 wow fadeInUp">
        <div class="subhead">Contact Us</div>
        <h2 class="title-section">Drop Us a Line</h2>
        <div class="divider"></div>
        
        <form action="#">
            <div class="py-2">
            <input type="text" class="form-control" placeholder="Full name">
            </div>
            <div class="py-2">
            <input type="text" class="form-control" placeholder="Email">
            </div>
            <div class="py-2">
            <textarea rows="6" class="form-control" placeholder="Enter message"></textarea>
            </div>
            <button type="submit" class="btn btn-primary rounded-pill mt-4">Send Message</button>
        </form>
        </div>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<div class="page-section border-top">
    <div class="container">
    <div class="text-center wow fadeInUp">
        <div class="subhead">Our Albums</div>
        <h2 class="title-section">Read our latest <span class="marked">Album</span></h2>
        <div class="divider mx-auto"></div>
    </div>
    <div class="row my-5 card-blog-row">
        @foreach($albums->take(4) as $album)
        <div class="col-md-6 col-lg-3 py-3 wow fadeInUp">
        <div class="card-blog">
            <div class="header">
            <div class="entry-footer">
                <div class="post-author"></div>
                <a href="#" class="post-date">{{ date('F d, Y', strtotime($album->created_at)) }}</a>
            </div>
            </div>
            <div class="body">
            <div class="post-title"><a href="{{ route('albums-web.show', $album->id) }}">{{$album->title }}</a></div>
            <div class="post-excerpt">{{ $album->description }}</div>
            </div>
            <div class="footer">
            <a href="{{ route('albums-web.show', $album->id) }}">Read More <span class="mai-chevron-forward text-sm"></span></a>
            </div>
        </div>
        </div>
        @endforeach
    </div>

    <div class="text-center">
        <a href="blog.html" class="btn btn-outline-primary rounded-pill">Discover More</a>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<!-- <div class="page-section client-section">
    <div class="container-fluid">
    <div class="row row-cols-2 row-cols-md-3 row-cols-lg-5 justify-content-center">
        <div class="item wow zoomIn">
        <img src="{{ asset('web-design/img/clients/airbnb.png') }}" alt="">
        </div>
        <div class="item wow zoomIn">
        <img src="{{ asset('web-design/img/clients/google.png') }}" alt="">
        </div>
        <div class="item wow zoomIn">
        <img src="{{ asset('web-design/img/clients/stripe.png') }}" alt="">
        </div>
        <div class="item wow zoomIn">
        <img src="{{ asset('web-design/img/clients/paypal.png') }}" alt="">
        </div>
        <div class="item wow zoomIn">
        <img src="{{ asset('web-design/img/clients/mailchimp.png') }}" alt="">
        </div>
    </div>
    </div>  -->
@endsection
<style>
    .post-excerpt {
        max-width: 300px; 
        min-width: 100px; 
        overflow: hidden; 
        text-overflow: ellipsis; 
        max-height: 130px;
        min-height: 112px;
    }
</style>
