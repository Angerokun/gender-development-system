<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gender and Development | Laguna State Polytechnic University - SC</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design/vendor/animate/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design/css/maicons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design/vendor/owl-carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design/css/theme.css') }}">
    <link rel="shortcut icon" href="{{ asset('web-design-portal/images/favicon.png') }}" />
</head>
<body>
    <!-- <div class="fb-customerchat"
    page_id="102195112512848"
    theme_color="#6C55F9"
    logged_in_greeting="Hi! How can we help you?"
    logged_out_greeting="GoodBye!... Hope to see you soon."
    minimized="false">
    </div> -->

    <div id="app">
        <!-- Back to top button -->
        <div class="back-to-top"></div>
        <!-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <ul class="navbar-nav ms-auto">
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav> -->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light navbar-float">
            <div class="container">
                <a href="{{ route('home') }}" class="navbar-brand">GAD <span class="text-primary">LSPU</span></a>

                <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-collapse collapse" id="navbarContent">
                <ul class="navbar-nav ml-lg-4 pt-3 pt-lg-0">
                    <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                    <a href="{{ route('about') }}" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                    <a href="{{ route('services') }}" class="nav-link">Services</a>
                    </li>
                    <li class="nav-item">
                    <a href="{{ route('albums-web.index') }}" class="nav-link">Seminars/Workshop</a>
                    </li>
                    <li class="nav-item">
                    <a href="{{ route('contact') }}" class="nav-link">Contact</a>
                    </li>
                </ul>
                @guest
                <div class="ml-auto">
                    <a href="{{ route('login') }}" class="btn btn-outline rounded-pill">Sign In</a>
                    <a href="{{ route('privacy-policy') }}" style="color: white;" class="btn btn-outline rounded-pill btn-primary">Sign Up</a>
                </div>
                </div>
                @else
                <div class="ml-auto">
                    <a href="{{ route('login') }}" class="btn btn-outline rounded-pill">Go to Portal</a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="color: white;" class="btn btn-outline rounded-pill btn-danger">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
                
                @endguest
            </div>
            </nav>

            
        </header>

        <main class="py-4">
            @yield('content')
        </main>

        <footer class="page-footer">
            <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-lg-3 py-3">
                <h3>GAD<span class="text-primary">LSPU</span></h3>
                <p>Reach our Offices or Contact us  provided below.</p>

                <p><a href="#" >{{ App\Common\settings()->contact_address }}</a></p>
                <p><a href="#">{{ App\Common\settings()->contact_email }}</a></p>
                </div>
                <div class="col-lg-3 py-3">
                <h5>Quick Links</h5>
                <ul class="footer-menu">
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Registration</a></li>
                </ul>
                </div>
                <div class="col-lg-3 py-3">
                <h5>About Us</h5>
                <ul class="footer-menu">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                    <li><a href="{{ route('services') }}">Services</a></li>
                    <li><a href="{{ route('albums-web.index') }}">Seminars/Workshop</a></li>
                    <li><a href="{{ route('contact') }}">Contact us</a></li>
                </ul>
                </div>
                <div class="col-lg-3 py-3">
                <h5>Subscribe</h5>

                <div class="sosmed-button mt-4">
                    <a href="{{ App\Common\settings()->fb_page }}"><span class="mai-logo-facebook-f"></span></a>
                    <a href="{{ App\Common\settings()->twitter }}"><span class="mai-logo-twitter"></span></a>
                    <a href="{{ App\Common\settings()->gmail }}"><span class="mai-logo-google"></span></a>
                    <a href="{{ App\Common\settings()->linkedin }}"><span class="mai-logo-linkedin"></span></a>
                </div>
                </div>
            </div>

            <!-- <div class="row">
                <div class="col-sm-6 py-2">
                <p id="copyright">&copy; 2020 <a href="https://macodeid.com/">MACode ID</a>. All rights reserved</p>
                </div>
                <div class="col-sm-6 py-2 text-right">
                <div class="d-inline-block px-3">
                    <a href="#">Privacy</a>
                </div>
                <div class="d-inline-block px-3">
                    <a href="#">Contact Us</a>
                </div>
                </div>
            </div> -->
            </div> <!-- .container -->
        </footer> <!-- .page-footer -->
    </div>
    @stack('scripts')

    <!-- Scripts -->
    <script src="{{ asset('web-design/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('web-design/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('web-design/vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('web-design/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('web-design/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('web-design/vendor/animateNumber/jquery.animateNumber.min.js') }}"></script>
    <script src="{{ asset('web-design/js/google-maps.js') }}"></script>
    <script src="{{ asset('web-design/js/theme.js') }}"></script>
    <!-- <script>
        window.fbAsyncInit = function() {
            FB.init({
            appId            : '912333495590130',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v2.11'
            });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "http://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script> -->
</body>
</html>
