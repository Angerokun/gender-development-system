@extends('portal.layouts.app')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Account Verification</h4>
                <div class="table-responsive">
                <table class="table table-striped" id="verify-account">
                    <thead>
                        <tr>
                            <th scope="col" width="30%">Name</th>
                            <th scope="col" width="30%">Account Type</th>
                            <th scope="col" width="30%">Contact No.</th>
                            <th scope="col" width="30%">Date Created</th>
                            <th scope="col" width="30%">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            @if($user->status == 'Waiting for Approval' && $user->status != null) 
                            <tr>
                                <td><a href="{{ route('account-verification.show', $user->id) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->profile->user_type }}</td>
                                <td>{{ $user->profile->contact_no }}</td>
                                <td>{{ date('Y-m-d h:i A', strtotime($user->created_at)) }}</td>
                                <td>{{ $user->status }}</td>
                            </tr>
                            @endIf
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $("#verify-account").DataTable();
</script>
@endpush
@endsection
