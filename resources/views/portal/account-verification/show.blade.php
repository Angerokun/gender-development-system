@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div align='right'>
                    <a href="{{ route('account-verification.verify-modal', ['id' => $user->id, 'status' => 'Approved']) }}" title="Approved Request" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-primary"><i class="fas fa-thumbs-up"></i> Approve</button></a>
                    <a href="{{ route('account-verification.verify-modal', ['id' => $user->id, 'status' => 'Rejected']) }}" title="Reject Request" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-danger"><i class="fas fa-thumbs-down"></i> Reject</button></a>
                    <a href="{{ route('account-verification.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">{{ $user->name }} - {{ ucwords($user->profile->user_type) }}</h4>
                <p class="card-description">
                    Basic Information
                </p>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>First name</b></label><br>
                        <p>&nbsp;{{ $user->profile->first_name  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Last name</b></label><br>
                        <p>&nbsp;{{ $user->profile->last_name }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Gender</b></label><br>
                        <p>&nbsp;{{ $user->profile->gender  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Email</b></label><br>
                        <p>&nbsp;{{ $user->email }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Contact No</b></label><br>
                        <p>&nbsp;{{ $user->profile->contact_no  }}</p>
                    </div>
                    @if ($user->profile->user_type == 'Student')
                    <div class="form-group col-md-6">
                        <label><b>Section / Course / Year</b></label><br>
                        <p>&nbsp;{{ $user->profile->section }} / {{ $user->profile->course }} / {{ $user->profile->year }}</p>
                    </div>
                    @else
                    <div class="form-group col-md-6">
                        <label><b>Department</b></label><br>
                        <p>&nbsp;{{ $user->profile->department }}</p>
                    </div>
                    @endIf
                </div>
                
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
