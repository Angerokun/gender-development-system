<form method="POST"  id="verify-user" class="form-horizontal" action="{{ route('account-verification.verify',['id' => $user->id, 'status' => $status]) }}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    @if ($status == 'Approved')
    <h6> Are you sure you want to Approved {{ $user->name }}?</h6>
    @else
    <h6> Are you sure you want to Disapproved {{ $user->name }}?</h6>
    <div class="form-row">
        <div class="form-group col-md-12">
            <label><b>Add Remarks</b></label><br>
            <textarea name="remarks" id="" cols="30" rows="10" class="form-control"></textarea>
        </div>
    </div>
    @endIf
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <button type="button" id="modal_close" class="btn btn-secondary">Close</button>
                <button type="submit" class="btn btn-primary">{{ $status }}</button>
            </div>
        </div>
    </div>
</form>
<!-- <script>
    $('#verify-user').register_fields('.errors');
</script> -->
