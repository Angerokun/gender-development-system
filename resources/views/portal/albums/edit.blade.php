@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('albums.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Create Albums</h4>
                <form method="POST" id ="album-create" action="{{ route('albums.update', $album->id) }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Album Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="title" value="{{ $album->title }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                        <textarea name="description" id="" class="form-control">{{ $album->description }}</textarea>
                        </div>
                    </div>
                    </div>
                </div>
                <hr>
                <div class="container">
                    <h4><b>Album Image</b></h4>
                    <div class="row" id="upload-panel">
                        @foreach( $album->albumItems as $img_product)
                        <div class="col-md-3 col-sm-6 existImage">
                            <div class="image_uploaded">
                                <input type="hidden" name="exist_items[]" value="{{ $img_product->id }}">
                                <button type="button" class="btn btn-danger delete_" id=""><i  class="fa fa-times-circle"  aria-hidden="true"></i></button>
                                <img  style="width:100%" src="{{ asset($img_product->image_url) }}"></div><br>                              
                            <input type="text" class="form-control" name="sort_order_edit{{ $img_product->id }}" placeholder="Sort Order" value="{{ $img_product->sort_order }}" data-validation="required">
                        </div>
                        @endforeach
                        <div class="col-md-3 col-sm-6">
                            <div id="add-image-upload" class="cropit-preview-add"></div>
                        </div>
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-edit" ></i> Update</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript" src="{{asset('js/jquery.cropit.js')}}"></script>
<script>
    $('#album-create').register_fields('.errors');

    $(function() {
        $('.image-editor0').cropit({minZoom: 2});
        $('#submit_product').click(function() {
          for (let i = 0; i < $('.upimage').length; i++) {
            // Move cropped image data to hidden input
            $('.hidden-image-data'+i).val($('.image-editor'+i).cropit('export',{originalSize: true,quality: 0.5}));
          }
          var formValue = $(this).serialize();
          $('#result-data').text(formValue);
          // Prevent the form from actually submitting
          return true;
        });
      });
      var ctr = 0;
      $("#add-image-upload").click(function(e){  
            e.preventDefault();
            $('#upload-panel').prepend(
            '<div class="col-md-3 col-sm-6 upimage">'+
                '<input type="hidden" name="items[]" value="'+ctr+'">'+
                '<div class="image-editor'+ctr+'">'+
                    '<input type="file" class="cropit-image-input">'+
                    '<div class="cropit-preview"><button type="button" class="btn btn-danger" id="delete_div"><i  class="fa fa-times-circle"  aria-hidden="true"></i></button></div>'+
                    '<div class="image-size-label">Resize image'+
                    '</div>'+
                    '<input type="range" class="cropit-image-zoom-input">'+
                    '<input type="hidden" name="image-data'+ctr+'" class="hidden-image-data'+ctr+'" />'+
                    '<input type="text" class="form-control" name="sort_order'+ctr+'" placeholder="Sort Order" data-validation="required">'+
                '</div>'+
                '<script>$(".image-editor'+ctr+'").cropit({minZoom: 2});<\/script>'+
            '</div>');
            

            $('#delete_div').click(function(e){    
                $(this).closest('.upimage').remove();
                ctr--;
                return false;
            });
            ctr++;
      });

      $('.delete_').click(function(e){    
        $(this).closest('.existImage').remove();
        return false;
    });

</script>
@endpush
<style>
    /********************* Crop It Css **********************/
.cropit-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-top: 7px;
    width: 255px;
    height: 340px;
  }


  #delete_div{
      position: absolute;
      margin: 2px;
      z-index: 1;
  }

  .delete_{
    position: absolute;
    margin: 2px;
    z-index: 1; 
  }

  .cropit-preview-image-container {
    cursor: move;
  }

  .image-size-label {
    margin-top: 10px;
  }

  input {
    display: block;
  }

  button[type="submit"] {
    margin-top: 10px;
  }

  #result {
    margin-top: 10px;
    width: 900px;
  }

  #result-data {
    display: block;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    word-wrap: break-word;
  }

  .cropit-preview-add  {
    background-color: #f8f8f8;
    background-size: cover;
    background-image: url('/web-design-portal/images/add_image.jpg');
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-top: 35px;
    width: 255px;
    height: 340px;
  }

  .image_uploaded{
    background-color: #f8f8f8;
    background-size: cover;
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-top: 35px;
    width: 255px;
    height: 340px;
  }
  /********************* End Crop It Css **********************/

</style>
@endsection
