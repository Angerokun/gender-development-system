@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    <div align='right'>
                        <a href="{{ route('albums.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                <h4 class="card-title">Albums</h4>
                <p class="card-description">
                Albums List.
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="album-table">
                    <thead>
                        <tr>
                            <th scope="col" width="40%">Album Name</th>
                            <th scope="col" width="60%">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#album-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('album-list') !!}",
                columns:[
                    {data: "title","name":"title","width":"40%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("albums.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "description", 'name' :"description",orderable:false,searchable:false,"width":"60%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection
<style>
    .table th, .table td { max-width: 200px; min-width: 70px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
</style>