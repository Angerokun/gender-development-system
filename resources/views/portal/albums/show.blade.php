@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right'>
                    <a href="{{ route('albums.edit', $album->id) }}" style="color: orange;margin-right:10px;" ><i class="fa fa-edit"></i></a>
                    <a href="{{ route('albums.delete', $album->id) }}" style="color: red;" title="Delete Profiling Record" class="popup_form" data-toggle="modal"><i class="fa fa-trash"></i></a>
                    <a href="{{ route('albums.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4  class="card-title">{{ $album->title }}</h4>
                <p class="card-description">
                   {{ $album->description }}
                </p>
                <form method="POST" id ="album-item-create" action="{{ route('album-items.store', $album->id) }}">
                    @csrf
                    <hr>
                    <div class="container">
                        <h4><b>Album Image</b></h4>
                        <div class="row" id="upload-panel">
                            @foreach( $album->albumItems as $img_product)
                            <div class="col-md-3 col-sm-6 existImage">
                                <div class="image_uploaded">
                                    <input type="hidden" name="exist_items[]" value="{{ $img_product->id }}">
                                    <img  style="width:100%" src="{{ asset($img_product->image_url) }}"></div><br>                              
                            </div>
                            @endforeach
                        </div>
                    </div>
                </form>
                <div id="result">
                    <code id="result-data"></code>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript" src="{{asset('js/jquery.cropit.js')}}"></script>
<style>
    /********************* Crop It Css **********************/
.cropit-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-top: 7px;
    width: 255px;
    height: 340px;
  }


  #delete_div{
      position: absolute;
      margin: 2px;
      z-index: 1;
  }

  .delete_{
    position: absolute;
    margin: 2px;
    z-index: 1; 
  }

  .cropit-preview-image-container {
    cursor: move;
  }

  .image-size-label {
    margin-top: 10px;
  }

  input {
    display: block;
  }

  button[type="submit"] {
    margin-top: 10px;
  }

  #result {
    margin-top: 10px;
    width: 900px;
  }

  #result-data {
    display: block;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    word-wrap: break-word;
  }

  .cropit-preview-add  {
    background-color: #f8f8f8;
    background-size: cover;
    background-image: url('/web-design-portal/images/add_image.jpg');
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-top: 35px;
    width: 255px;
    height: 340px;
  }

  .image_uploaded{
    background-color: #f8f8f8;
    background-size: cover;
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-top: 35px;
    width: 255px;
    height: 340px;
  }
  /********************* End Crop It Css **********************/

</style>
@endpush
@endsection
