@if ($status == 'Approved')
<form method="POST" id ="counseling-create" action="{{ route('request.update',['id' => $counseling_request->id, 'status' => $status]) }}">
<input type="hidden" name="_method" value="PUT">
@csrf
<div class="errors"></div>
<div class="row">
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Schedule Date</label>
        <div class="col-sm-9">
        <input type="datetime-local" class="form-control" style="border-left: solid 2.0px #ec0000" name="schedule_date" value="" data-validation="required"/>
        </div>
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Meeting Type</label>
        <div class="col-sm-9">
        <select name="meeting_type" class="form-control" style="border-left: solid 2.0px #ec0000" data-validation="required">
            <option value="Video Conference">Video Conference</option>
            <option value="GAD Office">GAD Office</option>
        </select>
        </div>
    </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Meeting Link</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="meeting_link" value=""/>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Conducted By</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="conducted_by" value="" style="border-left: solid 2.0px #ec0000" data-validation="required"/>
        </div>
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Background Case</label>
        <div class="col-sm-9">
            <textarea name="background_case" class="form-control"></textarea>
        </div>
    </div>
    </div>
</div>

<div align="right" style="margin-bottom: 20px;">
    <a href="{{ route('counseling-request.show', $counseling_request->id) }}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
    <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-thumbs-up" ></i> Approved</button>
</div>
</form>
<script>
    $('#counseling-create').register_fields('.errors');
</script>
@else

<form method="POST"  class="form-horizontal" action="{{ route('request.update',['id' => $counseling_request->id, 'status' => $status]) }}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">
    {{csrf_field()}}
    <h6> Are you sure you want to Disapproved counseling request {{ $counseling_request->request_no }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{ route('counseling-request.show', $counseling_request->id) }}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Reject</button>
            </div>
        </div>
    </div>
</form>

@endIf