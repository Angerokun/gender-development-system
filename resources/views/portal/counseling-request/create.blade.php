@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('counseling-request.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Create Counseling Request</h4>
                <form method="POST" id ="counseling-request-create" action="{{ route('counseling-request.store') }}">
                @csrf
                <div class="errors"></div>
                <p class="card-description">
                    Type of Counseling
                </p>
                <div class="row container">
                    <ul>
                        <li>
                            <p><strong>Individual Counseling.</strong> Individual counseling is a personal, individualized approach to counseling that helps people work through difficulties in their personal lives. Individual counseling may address issues such as mental health, life adjustments, and substance abuse.</p>
                        </li>
                        <li>
                            <p><strong>Couples Counseling.</strong> Issues and disagreements between couples aren’t unusual—but when escalated, they may require the help of a professional counselor. Whether a couple is dealing with issues of closeness or more serious problems like aggressive behavior, this type of counseling provides tools for resolving conflicts and rebuilding relationships.
                        </li>
                        <li>
                            <p><strong>Group Counseling.</strong> Group counseling may be a viable option for people experiencing issues that others are also dealing with. Knowing they are not alone can help clients reach important goals, know that they are being held accountable, and develop management strategies. Common topics addressed in group therapy settings include substance abuse, anger management, working through trauma, and more.
                        </li>
                        <li>
                            <p><strong>Family Counseling.</strong> Family counseling may be necessary for a variety of reasons including the loss of a family member, changes in family dynamics, or family conflict. Sometimes, family counseling is conducted with all family members present, while other times it may work with family members individually.
                        </li>
                    </ul>
                </div><hr>
                <p class="card-description">
                    Counseling Request Details
                </p>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-9">
                        <select name="student_id" class="form-control" style="border-left: solid 2.0px #ec0000" id="" data-validation="required">
                            <option value="">Name</option>
                            @if (Auth::user()->profile_type == 'App\Models\StudentProfile')
                                <option value="{{ Auth::user()->profile_id }}" selected>{{ Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name }}</option>
                            @else
                            @foreach ($students as $student)
                                <option value="{{ $student->id }}">{{ $student->first_name . ' ' . $student->last_name }}</option>
                            @endforeach
                            @endif
                        </select>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Counseling Type</label>
                        <div class="col-sm-9">
                            <select name="counseling_type" class="form-control" style="border-left: solid 2.0px #ec0000" id="" data-validation="required">
                                <option value="Individual Counseling">Individual Counseling</option>
                                <option value="Couples Counseling">Couples Counseling</option>
                                <option value="Group Counseling">Group Counseling</option>
                                <option value="Family Counseling">Family Counseling</option>
                            </select>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                    </div>
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#counseling-request-create').register_fields('.errors');
</script>
@endpush
@endsection
