@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    <div align='right'>
                        <a href="{{ route('counseling-request.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                <h4 class="card-title">Counseling Request</h4>
                <p class="card-description">
                List of Counseling.
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="counseling-request-table">
                    <thead>
                        <tr>
                            <th scope="col" width="25%">Student Name</th>
                            <th scope="col" width="20%">Request No</th>
                            <th scope="col" width="25%">Counseling</th>
                            <th scope="col" width="15%">Status</th>
                            <th scope="col" width="15%">Date Created</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#counseling-request-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('counseling-request-list') !!}",
                columns:[
                    {data: "student_name","name":"student_name","width":"25%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("counseling-request.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data:"request_no","name":"request_no","width":"20%"},
                    {data:"counseling_type","name":"counseling_type","width":"25%"},
                    {data:"status","name":"status","width":"15%"},
                    {data:"date_request","name":"date_request","width":"15%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection