@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right'>
                @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile' && $counseling_request->status == 'Pending')
                    <a href="{{ route('request.approval', ['id' => $counseling_request->id, 'status' => 'Approved']) }}" title="Approved Request" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-primary"><i class="fas fa-thumbs-up"></i> Approve</button></a>
                    <a href="{{ route('request.approval', ['id' => $counseling_request->id, 'status' => 'Rejected']) }}" title="Reject Request" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-danger"><i class="fas fa-thumbs-down"></i> Reject</button></a>
                @endIf
                    <a href="{{ route('counseling-request.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4  class="card-title">{{ $counseling_request->request_no }} - {{ $counseling_request->counseling_type }}</h4><hr>
                <p>Status: {{ $counseling_request->status }} | Date Created: {{ date('F d, Y H:i A', strtotime($counseling_request->created_at)) }}</p>
                <p class="card-description">
                   {{ $counseling_request->description }}
                </p>
                <div class="form-row">
                    <div class="form-group col-md-12">
                    </div>
                </div>
                    <hr>

                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="attendance-tab" data-toggle="tab" href="#attendance" role="tab" aria-controls="attendance" aria-selected="true">Counseling</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="attendance" role="tabpanel" aria-labelledby="attendance-tab">
                            <h4  class="card-title">Counseling Schedule</h4>
                            <div class="table-responsive">
                            <table class="table table-striped" id="counseling-table">
                                <thead>
                                    <tr>
                                        <th scope="col" width="50%">Schedule Date</th>
                                        <th scope="col" width="25%">Meeting Type</th>
                                        <th scope="col" width="25%">Conducted By</th>
                                        <th scope="col" width="25%">Background Case</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($counseling_request->counselings as $counseling)
                                    <tr>
                                        <td><a href="{{ route('counseling.show', $counseling->id) }}">{{ date('F d, Y', strtotime($counseling->schedule_date)) }}</a></td>
                                        <td>{{ $counseling->meeting_type }}</td>
                                        <td>{{ $counseling->conducted_by }}</td>
                                        <td>{{ $counseling->background_case }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
    $('#counseling-table').DataTable();
    </script>
@endpush
@endsection
