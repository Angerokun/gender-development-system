Hello <strong>{{ $name }}!</strong>,
<br>
<p>This email is to inform you that your Account was <strong>{{ $status }}</strong>.</p>
@if($status == 'Approved')
<p>You can now access your account using this link <a href="http://gad-lspu.info/login">http://gad-lspu.info/login</a></p>
@else
<p>Your account was rejected with a remarks of "{{ $remarks }}"</p>
@endIf