@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    <!-- <div align='right'>
                        <a href="{{ route('seminar.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div> -->
                <h4 class="card-title">Evaluations</h4>
                <p class="card-description">
                Evaluation of Seminars or Workships.
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="evaluation-table">
                    <thead>
                        <tr>
                            <th scope="col" width="25%">Seminar</th>
                            <th scope="col" width="25%">Student Evaluated</th>
                            <th scope="col" width="25%">Date Created</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#evaluation-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('evaluation-list') !!}",
                columns:[
                    {data: "seminar_name","name":"seminar_name","width":"25%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("evaluation.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "student_evaluated", 'name' :"student_evaluated",orderable:false,searchable:false,"width":"25%"},
                    {data: "created_at", 'name' :"created_at",orderable:false,searchable:false,"width":"25%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection