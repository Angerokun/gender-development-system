@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right'>
                    <a href="{{ route('evaluation.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <p align="center">
                   <strong>Title of Seminar/Training/Workshop (Pamagat ng Seminar):</strong> <u>{{ $student_evaluation->seminar->name }}</u><br/>
                   <strong>Date(Petsa):</strong> <u>{{ date('F d, Y h:i a', strtotime($student_evaluation->seminar->date_schedule)) }}</u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <strong>Venue(Lugar):</strong> <u>{{ $student_evaluation->seminar->venue }}</u><br/>
                   {{ $student_evaluation->seminar->evaluationTemplate->instructions }}
                </p>
                <div class="col-md-12">
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table" style="border:none; border-color: white;" >
                                <tbody >
                                    <tr align="center">
                                        <td>5 = <strong>Strongly Agree</strong></td>
                                        <td>4 = <strong>Agree</strong></td>
                                        <td>3 = <strong>Moderately Agree</strong></td>
                                        <td>2 = <strong>Disagree</strong></td>
                                        <td>1 = <strong>Strongly Disagree</strong></td>
                                    </tr>
                                    <tr align="center">
                                        <td>(Lubos na sumasang-ayon)</td>
                                        <td>(Sumasang-ayon)</td>
                                        <td>(Bahagyang Sumasang-ayon)</td>
                                        <td>(Di sumasang-ayon)</td>
                                        <td>(Lubos na di sumasangayon)</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <table class="table table-bordered" id="bhw-table">
                            <thead>
                                <tr align="center">
                                    <th width="75%"></th>
                                    <!-- <th width="20%" style="text-align:center" >Remarks</th> -->
                                    <th width="5%" style="text-align:center">5</th>
                                    <th width="5%" style="text-align:center">4</th>
                                    <th width="5%" style="text-align:center">3</th>
                                    <th width="5%" style="text-align:center">2</th>
                                    <th width="5%" style="text-align:center">1</th>
                                </tr>
                            </thead>
                            <tbody id="table_logs">
                                @foreach ($student_evaluation->seminar->evaluationTemplate->items as $item)
                                    @if ($item->evaluation_template_item_id == null)
                                    <tr align="left">
                                        <td style="word-wrap: break-word; white-space:normal;"><strong>{{$item->questionaire}}</strong></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @php 
                                        $subModules = App\Models\EvaluationTemplateItem::where('evaluation_template_item_id', $item->id)->get(); 
                                    @endphp
                                    @foreach($subModules as $key => $sub)
                                    <tr align="left">
                                        <td style="word-wrap: break-word; white-space:normal;">{{ $key+1 . '.' }}  {{$sub->questionaire}}</td>
                                        @php
                                            $mark = App\Models\StudentEvaluationItem::where('evaluation_template_item_id', $sub->id)->where('student_evaluation_id', $student_evaluation->id)->first();
                                        @endphp
                                        @if ($mark)
                                            @if ($mark->mark != null)
                                            <td>{!! $mark->mark == '5' ? '&#10004;' : '' !!}</td>
                                            <td>{!! $mark->mark == '4' ? '&#10004;' : '' !!}</td>
                                            <td>{!! $mark->mark == '3' ? '&#10004;' : '' !!}</td>
                                            <td>{!! $mark->mark == '2' ? '&#10004;' : '' !!}</td>
                                            <td>{!! $mark->mark == '1' ? '&#10004;' : '' !!}</td>
                                            @elseif ($mark->essay != null)
                                                <td colspan="5">{{ $mark->essay }}</td>
                                            @endif
                                        @else
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
