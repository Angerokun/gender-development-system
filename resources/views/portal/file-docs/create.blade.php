<form method="POST" id ="file-doc-create" action="{{ route('file-doc.store') }}">
    @csrf
    <div class="errors"></div>
    <div class="form-group row">
        <div class="row">
            <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">File Name</label>
                <div class="col-sm-9">
                <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="name" value="" data-validation="required"/>
                </div>
            </div>
            </div>
            <div class="col-md-6">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Upload File</label>
                <div class="col-sm-9">
                <input type="file" name="file" id="" style="border-left: solid 2.0px #ec0000" accept=".pdf,.docx,.png,.xlsx,.jpg,.jpeg" value="submit" class="form-control">
                </div>
            </div>
            </div>
            @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile')
            <div cass="col-md-6">
                <div class="form-group row" style="margin-left:160px;">
                    <input class="form-check-input " type="checkbox" value="1" name="is_view_user" id="" >
                    <label class="form-check-label" >
                        You want to view this file to User?
                    </label>
                </div>
            </div>
            @endIf
            <div class="col-md-12">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Description</label>
                <div class="col-sm-12">
                <textarea class="form-control" name="description" id="" ></textarea>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{route('file-doc.index')}}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</form>
<script>
    $('#file-doc-create').register_fields('.errors');

    function onImage() {
        $('#image').click();
    }

    function onFileSelected() {
        var selectedFile = event.target.files[0];
        var files = event.target.files;
        var reader = new FileReader();
        if(selectedFile && selectedFile.size < 2097152)
        {
            var imgtag = document.getElementById("im");
            imgtag.title = selectedFile.name;
            reader.onload = function(event) {
                imgtag.src = event.target.result;
            };
            reader.readAsDataURL(selectedFile);
        }
    }
</script>
