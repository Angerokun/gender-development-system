@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    <div align='right'>
                        <a class="popup_form" data-toggle="modal" data-url="{{ route('file-doc.create') }}" title="Upload File" href="#"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                <h4 class="card-title">Files and Documents</h4>
                <p class="card-description">
                Files and Documents List.
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="docs-table">
                    <thead>
                        <tr>
                            <th scope="col" width="35%">File Name</th>
                            <th scope="col" width="35%">Description</th>
                            <th scope="col" width="30%">Date Added</th>
                            <th scope="col" width="30%">User Added</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#docs-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('file-doc-list') !!}",
                columns:[
                    {data: "name","name":"name","width":"35%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("download.docs", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "description", 'name' :"description",orderable:false,searchable:false,"width":"35%"},
                    {data: "date_added", 'name' :"date_added",orderable:false,searchable:false,"width":"30%"},
                    {data: "user", 'name' :"user",orderable:false,searchable:false,"width":"30%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection