@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">General Settings</h4>
                <form method="POST" id ="general-create" action="{{ route('general-settings.store') }}">
                @csrf
                <div class="errors"></div>
                <p class="card-description">
                    General
                </p>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Contact Address</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="contact_address" value="{{ $general->contact_address }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Contact Email</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="contact_email" value="{{ $general->contact_email }}" style="border-left: solid 2.0px #ec0000" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Contact Number</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="contact_number" value="{{ $general->contact_number }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">FB Page</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="fb_page" value="{{ $general->fb_page }}"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Twitter</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="twitter" value="{{ $general->twitter }}" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Gmail</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="gmail" value="{{ $general->gmail }}" />
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Linkedin</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="linkedin" value="{{ $general->linkedin }}" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Youtube Url</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="youtube_url" value="{{ $general->youtube_url }}"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Tag Line</label>
                        <div class="col-sm-9">
                        <textarea name="tagline" class="form-control" style="border-left: solid 2.0px #ec0000" data-validation="required">{{ $general->tagline }}</textarea>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Mission</label>
                        <div class="col-sm-9">
                            <textarea name="mission" class="form-control" style="border-left: solid 2.0px #ec0000" data-validation="required">{{ $general->mission }}</textarea>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Vision</label>
                        <div class="col-sm-9">
                            <textarea name="vision" class="form-control" style="border-left: solid 2.0px #ec0000" data-validation="required">{{ $general->vision }}</textarea>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Core Values</label>
                        <div class="col-sm-9">
                            <textarea name="core_values" class="form-control" style="border-left: solid 2.0px #ec0000" data-validation="required">{{ $general->core_values }}</textarea>
                        </div>
                    </div>
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-edit" ></i> Update</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#general-create').register_fields('.errors');
</script>
@endpush
@endsection
