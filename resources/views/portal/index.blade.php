@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-sm-12">
        <div class="home-tab">
        <div class="d-sm-flex align-items-center justify-content-between border-bottom">
            <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
            </li>
            </ul>
        </div>
        <div class="tab-content tab-content-basic">
            <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview"> 
            <div class="row">
                <div class="col-sm-12">
                <div class="statistics-details d-flex align-items-center justify-content-between">
                    <div>
                    <p class="statistics-title">Students</p>
                    <h3 class="rate-percentage">{{ $student_count }}</h3>
                    <!-- <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>-0.5%</span></p> -->
                    </div>
                    <div>
                    <p class="statistics-title">Visitors</p>
                    <h3 class="rate-percentage">{{ $visitor_count }}</h3>
                    <!-- <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+0.1%</span></p> -->
                    </div>
                    <div>
                    <p class="statistics-title">Seminars and Workshops</p>
                    <h3 class="rate-percentage">{{ $seminar_count }}</h3>
                    <!-- <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p> -->
                    </div>
                    <div class="d-none d-md-block">
                    <p class="statistics-title">Total LGBT</p>
                    <h3 class="rate-percentage">{{ $count_lgbt }}</h3>
                    <!-- <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p> -->
                    </div>
                    <div class="d-none d-md-block">
                    <p class="statistics-title">Counseling</p>
                    <h3 class="rate-percentage">{{ $count_counseling }}</h3>
                    <!-- <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p> -->
                    </div>
                    <div class="d-none d-md-block">
                    <p class="statistics-title">Male</p>
                    <h3 class="rate-percentage">{{ $count_male }}</h3>
                    <!-- <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p> -->
                    </div>
                    <div class="d-none d-md-block">
                    <p class="statistics-title">Female</p>
                    <h3 class="rate-percentage">{{ $count_female }}</h3>
                    <!-- <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p> -->
                    </div>
                </div>
                </div>
            </div> 
            <input type="hidden" id="this_week" value="{{ json_encode($activity_chart['this_week']) }}">
            <input type="hidden" id="prev_week" value="{{ json_encode($activity_chart['prev_week']) }}">
            <div class="row">
                <div class="col-lg-8 d-flex flex-column">
                <div class="row flex-grow">
                    <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                    <div class="card card-rounded">
                        <div class="card-body">
                        <div class="d-sm-flex justify-content-between align-items-start">
                            <div>
                            <h4 class="card-title card-title-dash">Visitors Log Line Chart</h4>
                            <h5 class="card-subtitle card-subtitle-dash"></h5>
                            </div>
                            <div id="performance-line-legend"></div>
                        </div>
                        <div class="chartjs-wrapper mt-5">
                            <canvas id="performaneLine"></canvas>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="col-lg-4 d-flex flex-column">
                <div class="row flex-grow">
                    <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                    <div class="card bg-primary card-rounded">
                        <div class="card-body pb-0">
                        <h4 class="card-title card-title-dash text-white mb-4">Total Site Visited</h4>
                        <div class="row">
                            <div class="col-sm-4">
                            <p class="status-summary-ight-white mb-1">Visited</p>
                            <h2 class="text-info">357</h2>
                            </div>
                            <div class="col-sm-8">
                            <div class="status-summary-chart-wrapper pb-4">
                                <canvas id="status-summary"></canvas>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                    <div class="card card-rounded">
                        <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                            <div class="d-flex justify-content-between align-items-center mb-2 mb-sm-0">
                                <div class="circle-progress-width">
                                <div id="totalVisitors" class="progressbar-js-circle pr-2"></div>
                                </div>
                                <div>
                                <p class="text-small mb-2">Total Visitors</p>
                                <h4 class="mb-0 fw-bold">{{ $visitor_count }}</h4>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-6">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="circle-progress-width">
                                <div id="visitperday" class="progressbar-js-circle pr-2"></div>
                                </div>
                                <div>
                                <p class="text-small mb-2">Visits per day</p>
                                <h4 class="mb-0 fw-bold">{{ $count_visitor_per_day }}</h4>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 d-flex flex-column">
                <!-- <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                    <div class="card card-rounded">
                        <div class="card-body">
                        <div class="d-sm-flex justify-content-between align-items-start">
                            <div>
                            <h4 class="card-title card-title-dash">Market Overview</h4>
                            <p class="card-subtitle card-subtitle-dash">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                            </div>
                            <div>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> This month </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                <h6 class="dropdown-header">Settings</h6>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="d-sm-flex align-items-center mt-1 justify-content-between">
                            <div class="d-sm-flex align-items-center mt-4 justify-content-between"><h2 class="me-2 fw-bold">$36,2531.00</h2><h4 class="me-2">USD</h4><h4 class="text-success">(+1.37%)</h4></div>
                            <div class="me-3"><div id="marketing-overview-legend"></div></div>
                        </div>
                        <div class="chartjs-bar-wrapper mt-3">
                            <canvas id="marketingOverview"></canvas>
                        </div>
                        </div>
                    </div>
                    </div>
                </div> -->
                <!-- <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                    <div class="card card-rounded table-darkBGImg">
                        <div class="card-body">
                        <div class="col-sm-8">
                            <h3 class="text-white upgrade-info mb-0">
                            Enhance your <span class="fw-bold">Campaign</span> for better outreach
                            </h3>
                            <a href="#" class="btn btn-info upgrade-btn">Upgrade Account!</a>
                        </div>
                        </div>
                    </div>
                    </div>
                </div> -->
                <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                    <div class="card card-rounded">
                        <div class="card-body">
                        <div class="d-sm-flex justify-content-between align-items-start">
                            <div>
                            <h4 class="card-title card-title-dash">Book Requests</h4>
                            <p class="card-subtitle card-subtitle-dash">You have 50+ new requests</p>
                            </div>
                            <div>
                            <!-- <button class="btn btn-primary btn-lg text-white mb-0 me-0" type="button"><i class="mdi mdi-account-plus"></i>Add new member</button> -->
                            </div>
                        </div>
                        <div class="table-responsive  mt-1">
                            <table class="table select-table">
                            <thead>
                                <tr>
                                <th>User</th>
                                <th>Book</th>
                                <th>Release Date</th>
                                <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($borrowers as $borrower)
                                <tr>
                                    <td>
                                        <div class="d-flex ">
                                        <img src="{{ ($borrower->student->image == null ? asset('web-design/img/default.png') : route('profiling.image', $borrower->student->image)) }}" alt="">
                                        <div>
                                            <h6>{{ $borrower->student->first_name . ' ' . $borrower->student->last_name }}</h6>
                                            <p>{{ $borrower->student->section }}</p>
                                        </div>
                                        </div>
                                    </td>
                                    <td>
                                        <h6>{{ $borrower->miniLibrary->title }}</h6>
                                        <p>No. of Copies: {{ $borrower->no_of_copies }}</p>
                                    </td>
                                    <td>
                                        {{ date('F d, Y', strtotime($borrower->date_needed )) }}
                                        <!-- <div>
                                        <div class="d-flex justify-content-between align-items-center mb-1 max-width-progress-wrap">
                                            <p class="text-success">79%</p>
                                            <p>85/162</p>
                                        </div>
                                        <div class="progress progress-md">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 85%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        </div> -->
                                    </td>
                                    <td>
                                        @if ($borrower->status == 'Approved')
                                            <div class="badge badge-opacity-success">Approved</div></td>
                                        @elseif ($borrower->status == 'Pending')
                                            <div class="badge badge-opacity-warning">Pending</div></td>
                                        @else
                                        <div class="badge badge-opacity-danger">Rejected</div></td>
                                        @endif
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                </div>
                <div class="col-lg-4 d-flex flex-column">
                <!-- <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                    <div class="card card-rounded">
                        <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="card-title card-title-dash">Todo list</h4>
                                <div class="add-items d-flex mb-0">
                                <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?">
                                <button class="add btn btn-icons btn-rounded btn-primary todo-list-add-btn text-white me-0 pl-12p"><i class="mdi mdi-plus"></i></button>
                                </div>
                            </div>
                            <div class="list-wrapper">
                                <ul class="todo-list todo-list-rounded">
                                <li class="d-block">
                                    <div class="form-check w-100">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                                    </label>
                                    <div class="d-flex mt-2">
                                        <div class="ps-4 text-small me-3">24 June 2020</div>
                                        <div class="badge badge-opacity-warning me-3">Due tomorrow</div>
                                        <i class="mdi mdi-flag ms-2 flag-color"></i>
                                    </div>
                                    </div>
                                </li>
                                <li class="d-block">
                                    <div class="form-check w-100">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                                    </label>
                                    <div class="d-flex mt-2">
                                        <div class="ps-4 text-small me-3">23 June 2020</div>
                                        <div class="badge badge-opacity-success me-3">Done</div>
                                    </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check w-100">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                                    </label>
                                    <div class="d-flex mt-2">
                                        <div class="ps-4 text-small me-3">24 June 2020</div>
                                        <div class="badge badge-opacity-success me-3">Done</div>
                                    </div>
                                    </div>
                                </li>
                                <li class="border-bottom-0">
                                    <div class="form-check w-100">
                                    <label class="form-check-label">
                                        <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                                    </label>
                                    <div class="d-flex mt-2">
                                        <div class="ps-4 text-small me-3">24 June 2020</div>
                                        <div class="badge badge-opacity-danger me-3">Expired</div>
                                    </div>
                                    </div>
                                </li>
                                </ul>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div> -->
                <div class="row flex-grow">
                    <div class="col-12 grid-margin stretch-card">
                    <div class="card card-rounded">
                        <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4 class="card-title card-title-dash">Type By Sex</h4>
                            </div>
                            <canvas class="my-auto" id="doughnutChart" height="200"></canvas>
                            <div id="doughnut-chart-legend" class="mt-5 text-center"></div>
                            <input type="hidden" value="{{ $pie_gender }}" id="pie_gender">
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>


                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
@push('scripts')
@endpush
<script src="{{ asset('web-design-portal/js/chart.js') }}"></script>
<script src="{{ asset('web-design-portal/js/dashboard.js') }}"></script>
@endsection
<style>
    .table th, .table td { max-width: 200px; min-width: 70px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
</style>