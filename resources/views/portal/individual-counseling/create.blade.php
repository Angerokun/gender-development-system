@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('counseling.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Create Individual Counseling</h4>
                <form method="POST" id ="counseling-create" action="{{ route('counseling.store') }}">
                @csrf
                <div class="errors"></div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Select Student</label>
                        <div class="col-sm-9">
                            <select data-live-search="true" data-validation="required" style="border-left: solid 2.0px #ec0000"  title="Choose Student..." class="form-control select-picker" name="student_id">
                                    <option value="">Create New</option>
                                @foreach($students as $student)
                                    <option value="{{ $student->id }}" data-subtext="{{ $student->section }}">{{ $student->first_name . ' ' .$student->last_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                    </div>
                    </div>
                </div>
                <p >
                    Background of the Case:
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="summernote" data-validation="required" style="border-left: solid 2.0px #ec0000" name="background_case"></textarea>
                    </div>
                </div><hr>
                <p >
                    Goal:
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="summernote1" data-validation="required" style="border-left: solid 2.0px #ec0000" name="goal"></textarea>
                    </div>
                </div><hr>
                <p >
                    Approach:
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="summernote2" name="approach"></textarea>
                    </div>
                </div><hr>
                <p >
                    Comment:
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="summernote3" name="comment"></textarea>
                    </div>
                </div><hr>
                <p >
                    Recommendations:
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <textareaname="recommendation"></textareaname=>
                    </div>
                </div><hr>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $('#counseling-create').register_fields('.errors');

    $('.select-picker').selectpicker();

    $(document).ready(function() {
        $('#summernote').summernote({
            placeholder: 'Background of the Case..',
        });
        $('#summernote1').summernote({
            placeholder: 'Goal..',
        });
        $('#summernote2').summernote({
            placeholder: 'Approach..',
        });
        $('#summernote3').summernote({
            placeholder: 'Comment..',
        });
        $('#summernote4').summernote({
            placeholder: 'Recommendation..',
        });
    });
</script>
@endpush
<style>
    .note-editable {
        height: 150px;
    }
</style>
@endsection
