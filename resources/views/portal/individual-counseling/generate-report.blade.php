<div align="center">
    <h3>Counseling Report for Gender and Development LSPU</h3>
</div>
<div align="right" style="margin-top: -30px">
<p class="pad">Year: <span id="year_label">{{ date('Y') }}</span></p>
</div>

<table class="table table-bordered text-center mt-table" id="tracking_table" >
    <thead>
        <tr>
            <th scope="col" width="25%">Name</th>
            <th scope="col" width="10%">Request No.</th>
            <th scope="col" width="25%">Meeting Type</th>
            <th scope="col" width="25%">Status</th>
        </tr>
    </thead>
    <tbody class="report" name="report">
        @foreach ($counseling_requests as $counseling)
        <tr>
            <td>{{ $counseling->student->first_name . ' ' . $counseling->student->last_name }}</td>
            <td>{{ $counseling->request_no }}</td>
            <td>{{ $counseling->counseling_type }}</td>
            <td>{{ $counseling->status }}</td>
        </tr>
        @endforeach
    </tbody>
    <!-- <tbody class="nodatatr" name="nodatatr">
        <tr align="center" >
            <td colspan="10">No Data Available</td>
        </tr>
    </tbody> -->
</table>
<!-- <div align="left">
    <p class="pad">Prepared By: ______________________</p>
    <p class="pad" style="margin-top: -15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Barangay Nutrition Scholar</p>
</div>
<div align="right" style="margin-top: -50px">
    <p class="pad">Verified By: ______________________</p>
    <p class="pad" style="margin-top: -15px">Barangay Captain</p>
</div> -->

<style>
    .border{
        border: 3px solid #000;
    }
    .table thead th, .table td {
        vertical-align: middle !important;
    }
    .input-total-pr { 
        border: none;
        background-color: transparent;
    }
    .border-b{
        border-bottom: 1px solid #000;
        width: 250px;
        text-align:center;
    }
   
</style>
