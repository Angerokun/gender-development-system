@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                @if (Auth::user()->profile_type != 'App\Models\StudentProfile')
                    <div align='right'>
                    <button type="button" class="btn btn-primary print-form-report" data-form="print_counseling_report"><i class="fas fa-download"></i> Download or Print</button>
                        <a href="{{ route('counseling.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                @endif
                <h4 class="card-title">Counseling</h4>
                <p class="card-description">
                List of Counseling.
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="counseling-table">
                    <thead>
                        <tr>
                            <th scope="col" width="20%">Student Name</th>
                            <!-- <th scope="col" width="20%">Background Case</th>
                            <th scope="col" width="20%">Goal</th>
                            <th scope="col" width="15%">Approach</th>
                            <th scope="col" width="15%">Types</th> -->
                            <th scope="col" width="10%">Date Created</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="print_counseling_report" hidden>
        @include('portal.individual-counseling.generate-report')
    <style>
        .mt-table{
            margin-top: 1.5rem!important;
        }
        .bbm-supply {
                border-bottom: 2px solid;
                position: inherit;
                margin-bottom: -20px;
                margin-top: -1rem;
                width: 80%;
            }
        </style>
    </div>
@push('scripts')
<script>
    $(function() {
        $('#counseling-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('counseling-list') !!}",
                columns:[
                    {data: "student_name","name":"student_name","width":"25%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("counseling.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    // {data:"background_case","name":"background_case",orderable:false,searchable:false,"width":"25%"},
                    // {data:"goal","name":"goal",orderable:false,searchable:false,"width":"25%"},
                    // {data:"approach","name":"approach",orderable:false,searchable:false,"width":"15%"},
                    // {data:"types","name":"types",orderable:false,searchable:false,"width":"15%"},
                    {data:"date_created","name":"date_created",orderable:false,searchable:false,"width":"10%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection