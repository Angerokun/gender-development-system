<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gender and Development | Portal</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/all.js')}}"></script>
    
    <!-- plugins:js -->
    
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('web-design-portal/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('web-design-portal/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('web-design-portal/vendors/progressbar.js/progressbar.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('web-design-portal/js/off-canvas.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/template.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/settings.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <!-- <script src="{{ asset('web-design-portal/js/dashboard.js') }}"></script> -->
    <script src="{{ asset('web-design-portal/js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}" >

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/typicons/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/js/select.dataTables.min.css') }}"> -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('web-design-portal/css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('web-design-portal/images/favicon.png') }}" />
</head>
<body>
    <div class="fb-customerchat"
    page_id="102195112512848"
    theme_color="#6C55F9"
    logged_in_greeting="Hi! How can we help you?"
    logged_out_greeting="GoodBye!... Hope to see you soon."
    minimized="false">
    </div>
    <!-- WRAPPER -->
	<div id="app">
        <div class="container-scroller">
            <nav id="nav-bar" class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
                <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
                    <div class="me-3">
                    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
                        <span class="icon-menu"></span>
                    </button>
                    </div>
                    <div>
                    <a class="navbar-brand brand-logo" href="{{ route('home') }}">
                        <img src="{{ asset('web-design-portal/images/logo.png') }}" alt="logo" />
                    </a>
                    <a class="navbar-brand brand-logo-mini" href="{{ route('home') }}">
                        <img src="{{ asset('web-design-portal/images/logo-mini.png') }}" alt="logo" />
                    </a>
                    </div>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-top"> 
                    <ul class="navbar-nav">
                    <li class="nav-item font-weight-semibold d-none d-lg-block ms-0">
                        <h1 class="welcome-text">{{ App\Common\timeOfDay() }}, <span class="text-black fw-bold">{{ Auth::user()->name }}</span></h1>
                        <h3 class="welcome-sub-text">{{ Auth::user()->profile_type == 'App\\Models\\StudentProfile' ? 'ID No. ' . Auth::user()->profile->id_no : 'Your performance summary this week ' }}</h3>
                    </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                    <!-- <li class="nav-item dropdown d-none d-lg-block">
                        <a class="nav-link dropdown-bordered dropdown-toggle dropdown-toggle-split" id="messageDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false"> Select Category </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="messageDropdown">
                        <a class="dropdown-item py-3" >
                            <p class="mb-0 font-weight-medium float-left">Select category</p>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item preview-item">
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">Bootstrap Bundle </p>
                            <p class="fw-light small-text mb-0">This is a Bundle featuring 16 unique dashboards</p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item">
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">Angular Bundle</p>
                            <p class="fw-light small-text mb-0">Everything you’ll ever need for your Angular projects</p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item">
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">VUE Bundle</p>
                            <p class="fw-light small-text mb-0">Bundle of 6 Premium Vue Admin Dashboard</p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item">
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">React Bundle</p>
                            <p class="fw-light small-text mb-0">Bundle of 8 Premium React Admin Dashboard</p>
                            </div>
                        </a>
                        </div>
                    </li> -->
                    <!-- <li class="nav-item d-none d-lg-block">
                        <div id="datepicker-popup" class="input-group date datepicker navbar-date-picker">
                        <span class="input-group-addon input-group-prepend border-right">
                            <span class="icon-calendar input-group-text calendar-icon"></span>
                        </span>
                        <input type="text" class="form-control">
                        </div>
                    </li> -->
                    <!-- <li class="nav-item">
                        <form class="search-form" action="#">
                        <i class="icon-search"></i>
                        <input type="search" class="form-control" placeholder="Search Here" title="Search here">
                        </form>
                    </li> -->
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link count-indicator" id="notificationDropdown" href="#" data-bs-toggle="dropdown">
                        <i class="icon-mail icon-lg"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">
                        <a class="dropdown-item py-3 border-bottom">
                            <p class="mb-0 font-weight-medium float-left">You have 4 new notifications </p>
                            <span class="badge badge-pill badge-primary float-right">View all</span>
                        </a>
                        <a class="dropdown-item preview-item py-3">
                            <div class="preview-thumbnail">
                            <i class="mdi mdi-alert m-auto text-primary"></i>
                            </div>
                            <div class="preview-item-content">
                            <h6 class="preview-subject fw-normal text-dark mb-1">Application Error</h6>
                            <p class="fw-light small-text mb-0"> Just now </p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item py-3">
                            <div class="preview-thumbnail">
                            <i class="mdi mdi-settings m-auto text-primary"></i>
                            </div>
                            <div class="preview-item-content">
                            <h6 class="preview-subject fw-normal text-dark mb-1">Settings</h6>
                            <p class="fw-light small-text mb-0"> Private message </p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item py-3">
                            <div class="preview-thumbnail">
                            <i class="mdi mdi-airballoon m-auto text-primary"></i>
                            </div>
                            <div class="preview-item-content">
                            <h6 class="preview-subject fw-normal text-dark mb-1">New user registration</h6>
                            <p class="fw-light small-text mb-0"> 2 days ago </p>
                            </div>
                        </a>
                        </div>
                    </li> -->
                    <li class="nav-item dropdown"> 
                        <a class="nav-link count-indicator" id="countDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="icon-bell"></i>
                        @if (Auth::User()->unreadNotifications->count() > 0)
                        <span class="count"></span>
                        @endIf
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="countDropdown">
                        <a class="dropdown-item py-3" href="{{ route('notifications.index') }}">
                            <p class="mb-0 font-weight-medium float-left">You have {{ Auth::User()->unreadNotifications->count()  }} unread notification </p>
                            <span class="badge badge-pill badge-primary float-right">View all</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        @foreach (App\Http\Resources\Notification::collection(Auth::User()->notifications->take(5)) as $notification)
                        <a style="{{ $notification->read_at == null ? 'background-color: #d8ebff;': '' }}" class="dropdown-item preview-item notif_bell" data-url="{{ route('markAsView', $notification->id) }}" href="{{ $notification->data['route'] }}">
                            <!-- <div class="preview-thumbnail">
                            <img src="" alt="image" class="img-sm profile-pic">
                            </div> -->
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">{{ $notification->data['profile_name'] }}</p>
                            <p class="fw-light small-text mb-0">{{ $notification->data['details'] }}</p>
                            <p class="fw-light small-text mb-0">{{ $notification->created_at }} {{ $notification->time_diff }}</p>
                            </div>
                        </a>
                        @endforeach
                        <!-- <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                            <img src="" alt="image" class="img-sm profile-pic">
                            </div>
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">David Grey </p>
                            <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                            <img src="" alt="image" class="img-sm profile-pic">
                            </div>
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                            <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                            </div>
                        </a>
                        <a class="dropdown-item preview-item">
                            <div class="preview-thumbnail">
                            <img src="" alt="image" class="img-sm profile-pic">
                            </div>
                            <div class="preview-item-content flex-grow py-2">
                            <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                            <p class="fw-light small-text mb-0"> The meeting is cancelled </p>
                            </div>
                        </a> -->
                        </div>
                    </li>
                    <li class="nav-item dropdown d-none d-lg-block user-dropdown">
                        <a class="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                        <img class="img-xs rounded-circle" src="
                        {{ (Auth::user()->user_type == null ? asset('web-design-portal/images/default.png') : (Auth::user()->user->image == null ? asset('web-design-portal/images/default.png') : route('instructors.image', Auth::user()->user->image) ) ) }}" alt="Profile image"> </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                        <div class="dropdown-header text-center">
                            <img class="img-md rounded-circle" width="50" height="50" src="{{ (Auth::user()->user_type == null ? asset('web-design-portal/images/default.png') : (Auth::user()->user->image == null ? asset('web-design-portal/images/default.png') : route('instructors.image', Auth::user()->user->image) ) ) }}" alt="Profile image">
                            <p class="mb-1 mt-3 font-weight-semibold"></p>
                            <p class="fw-light text-muted mb-0"></p>
                        </div>
                        <!-- <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i> My Profile <span class="badge badge-pill badge-danger">1</span></a>
                        <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-message-text-outline text-primary me-2"></i> Messages</a>
                        <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-calendar-check-outline text-primary me-2"></i> Activity</a>
                        <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-help-circle-outline text-primary me-2"></i> FAQ</a> -->
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Sign Out
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                        </div>
                    </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>
            <div class="container-fluid page-body-wrapper">
                @include('portal.layouts.sidebar')

                <div class="main-panel">       
                    @yield('content')
                    @include('portal.layouts.modal')
                    <footer class="footer">
                        <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Learning Management System</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
	<!-- END WRAPPER -->
    @stack('scripts')
    
</body>
</html>
<script>
    window.fbAsyncInit = function() {
        FB.init({
        appId            : '912333495590130',
        autoLogAppEvents : true,
        xfbml            : true,
        version          : 'v2.11'
        });
    };
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "http://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
$(document).on('click','.notif_bell',function(e){
    var url = $(this).attr('data-url');

    $.ajax({
        method: "PUT",
        url: url,
        data: {"_token": "{{ csrf_token() }}"},
        success: function(data){
        }
    });
});
$(document).on('click','.popup_form',function(e){
    e.preventDefault();
    var title = $(this).attr('title');
    var url = $(this).attr('data-url');
    if (url == null){
        url = $(this).attr('href');
    }
    $('.modal-title').html(title);
    $.ajax({
        method: "GET",
        url: url,
        success: function(data){
            $('#myModal').modal({backdrop:'static'});
            $('#myModal').modal('show');
            $('div.modal-body').html(data);
        }
    });
});
$(document).on('click','#modal_close',function(e){
        $('#myModal').modal('hide');
    });
</script>
<script src="{{ asset('web-design-portal/vendors/js/vendor.bundle.base.js') }}"></script>
