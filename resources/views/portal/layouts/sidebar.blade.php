<nav class="sidebar sidebar-offcanvas" id="sidebar">
    @if (Auth::user()->profile_type == 'App\Models\AdminProfile')
    <ul class="nav">
        <li class="nav-item nav-category">Home</li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('portal.index') }}">
                <i class="menu-icon fa fa-tachometer-alt"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item nav-category">Gender & Develop</li>
        <li class="nav-item">
            <a href="{{ route('account-verification.index') }}" class="nav-link">
                <i class="menu-icon fa fa-user-check"></i> 
                <span class="menu-title">Account Verification</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('profiling.index') }}" class="nav-link">
                <i class="menu-icon fa fa-user-graduate"></i> 
                <span class="menu-title">Profiling</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('visitor-log.index') }}" class="nav-link">
                <i class="menu-icon fa fa-door-open"></i>
                <span class="menu-title">Visitors Log</span>
            </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="{{ route('file-doc.index') }}">
            <i class="menu-icon fa fa-file"></i>
            <span class="menu-title">Files and Documents</span>
        </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('mini-library.index') }}" class="nav-link">
                <i class="menu-icon fa fa-book"></i> 
                <span class="menu-title">Mini Library</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('albums.index') }}" class="nav-link">
                <i class="menu-icon fa fa-photo-film"></i> 
                <span class="menu-title">Album</span>
            </a>
        </li>
        <li class="nav-item nav-category">Activities</li>
        <li class="nav-item">
        <a href="{{ route('evaluation.index') }}" class="nav-link">
            <i class="menu-icon fa fa-chalkboard-teacher"></i>
            <span class="menu-title">Evaluation</span>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{ route('seminar.index') }}" class="nav-link" >
            <i class="menu-icon fa fa-video"></i>
            <span class="menu-title">Seminars/Workshops</span>
        </a>
        </li>
        <li class="nav-item nav-category">Counseling Services</li>
        <li class="nav-item">
        <a href="{{ route('counseling.index') }}" class="nav-link">
            <i class="menu-icon fa fa-user-secret"></i>
            <span class="menu-title">Counseling</span>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{ route('counseling-request.index') }}" class="nav-link">
            <i class="menu-icon fa fa-chalkboard"></i>
            <span class="menu-title">Counseling Request</span>
        </a>
        </li>
        <li class="nav-item nav-category">Settings</li>
        <li class="nav-item">
        <a href="{{ route('general-settings.index') }}" class="nav-link">
            <i class="menu-icon fa fa-gears"></i>
            <span class="menu-title">General</span>
        </a>
        </li>
        <!-- <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="menu-icon fa fa-chart-bar"></i>
            <span class="menu-title">Services</span>
        </a>
        </li> -->
    </ul>
    @else
    <ul class="nav">
        <li class="nav-item nav-category">Gender & Develop</li>
        <li class="nav-item">
        <a class="nav-link" href="{{ route('profiling.show', Auth::user()->profile_id) }}">
            <i class="menu-icon fa fa-user-graduate"></i>
            <span class="menu-title">My Profile</span>
        </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="{{ route('file-doc.index') }}">
            <i class="menu-icon fa fa-file"></i>
            <span class="menu-title">Files and Documents</span>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{ route('evaluation.index') }}" class="nav-link">
            <i class="menu-icon fa fa-chalkboard-teacher"></i>
            <span class="menu-title">Evaluation</span>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{ route('seminar.index') }}" class="nav-link" >
            <i class="menu-icon fa fa-video"></i>
            <span class="menu-title">Seminars/Workshops</span>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{ route('counseling.index') }}" class="nav-link">
            <i class="menu-icon fa fa-user-secret"></i>
            <span class="menu-title">Counseling</span>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{ route('counseling-request.index') }}" class="nav-link">
            <i class="menu-icon fa fa-chalkboard"></i>
            <span class="menu-title">Counseling Request</span>
        </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('mini-library.index') }}" class="nav-link">
                <i class="menu-icon fa fa-book"></i> 
                <span class="menu-title">Mini Library</span>
            </a>
        </li>
    </ul>
    @endif
</nav>