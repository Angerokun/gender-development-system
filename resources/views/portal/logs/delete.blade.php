<form method="POST" id="remove_bhw_form"  class="form-horizontal" action="{{route('visitor-log.destroy',$log->id)}}" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="DELETE">
    {{csrf_field()}}
    <h6> Are you sure you want to delete {{$log->date_visit }}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{ route('visitor-log.show', $log->id) }}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</form>
