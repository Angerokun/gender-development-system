@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    <div align='right'>
                        <a href="{{ route('visitor-log.create') }}" target="_blank"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                <h4 class="card-title">Visitor's Logs</h4>
                <p class="card-description">
                Visitor's Log List
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="logs-table">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">Image</th>
                            <th scope="col" width="20%">Name</th>
                            <th scope="col" width="10%">Date Visit</th>
                            <th scope="col" width="30%">Remarks</th>
                            <th scope="col" width="30%">Reason of Visit</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#logs-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('visitor-log-list') !!}",
                columns:[
                    {data: "image","name":"image",orderable:false,searchable:false,"width":"10%", className:"v-align text-center",
                    "render": function (data) {
                        return "<center><div><img src='" + data + "' alt='Image' height='50' width='50' class='rounded-circle'></div></center>"; 
                    }},
                    {data: "full_name","name":"full_name","width":"30%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("visitor-log.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "date_visit", 'name' :"date_visit",orderable:false,searchable:false,"width":"10%"},
                    {data: "remarks","name":"remarks",orderable:false,searchable:false,"width":"30%"},
                    {data: "reason", 'name' :"reason",orderable:false,searchable:false,"width":"30%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection