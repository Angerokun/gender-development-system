@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div align='right'>
                <a href="{{ route('visitor-log.delete', $log->id) }}" style="color: red;" title="Delete Log Record" class="popup_form" data-toggle="modal"><i class="fa fa-trash"></i></a>
                    <a href="{{ route('visitor-log.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">{{ date('F d, Y H:i a', strtotime($log->date_visit)) }}</h4>
                <p class="card-description">
                    Basic Information
                </p>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Full name</b></label><br>
                        <p>&nbsp;{{ $log->student->first_name . ' ' . $log->student->last_name  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Section</b></label><br>
                        <p>&nbsp;{{ $log->student->section }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Date Visit</b></label><br>
                        <p>&nbsp;{{ date('Y-m-d H:i a', strtotime($log->date_visit)) }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Reason</b></label><br>
                        <p>&nbsp;{{ $log->reason }}</p>
                    </div>
                </div>
                <hr>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
