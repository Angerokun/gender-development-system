<form method="POST" id ="counseling-create" action="{{ route('borrow_update.approval',['id' => $mini_library_item->id, 'status' => $status]) }}">
<input type="hidden" name="_method" value="PUT">
@csrf
<div class="errors"></div>
<p>Are you sure you want to {{ $status }} this request?</p>
<div align="right" style="margin-bottom: 20px;">
    <a href="{{ route('books-borrow.show', $mini_library_item->mini_library_id) }}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
    <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-thumbs-up" ></i>{{ $status }}</button>
</div>
</form>