<form method="POST" id ="book-create" action="{{ route('borrower-book.store', $id) }}">
@csrf
<div class="errors"></div>
<div class="row">
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Return Date</label>
        <div class="col-sm-9">
        <input type="date" class="form-control" style="border-left: solid 2.0px #ec0000" name="return_date" value="" data-validation="required"/>
        </div>
    </div>
    <!-- <div class="form-group row">
        <label class="col-sm-3 col-form-label">Select Student</label>
        <div class="col-sm-9">
        <select name="student_id" class="form-control" style="border-left: solid 2.0px #ec0000" id="" data-validation="required">
            <option value="">Select Student</option>
            @if (Auth::user()->profile_type == 'App\Models\StudentProfile')
                <option value="{{ Auth::user()->profile->id }}" selected>{{ Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name }}</option>
            @else
            @foreach ($students as $student)
                <option value="{{ $student->id }}">{{ $student->first_name . ' ' . $student->last_name }}</option>
            @endforeach
            @endif
        </select>
        </div>
    </div> -->
    </div>
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">No of Copies</label>
        <div class="col-sm-9">
        <input type="text" class="form-control" name="no_of_copies" value="" style="border-left: solid 2.0px #ec0000" data-validation="required"/>
        </div>
    </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
    @if (Auth::user()->profile_type == 'App\\Models\\StudentProfile')
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Date Needed</label>
        <div class="col-sm-9">
        <input type="date" class="form-control" style="border-left: solid 2.0px #ec0000" name="date_needed" value="" data-validation="required"/>
        </div>
    </div>
    @else
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Status</label>
        <div class="col-sm-9">
            <select class="form-control" name="status">
                <option value="Pending">Pending</option>
                <option value="Approved">Approved</option>
                <option value="Rejected">Rejected</option>
            </select>
        </div>
    </div>
    @endIf
    </div>
    <!-- <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Return Date</label>
        <div class="col-sm-9">
        <input type="date" class="form-control" style="border-left: solid 2.0px #ec0000" name="return_date" value="" data-validation="required"/>
        </div>
    </div>
    </div> -->
</div>
<!-- <div class="row">
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Due Date</label>
        <div class="col-sm-9">
            <input type="date" class="form-control" name="due_date" value="" data-validation="required" style="border-left: solid 2.0px #ec0000" />
        </div>
    </div>
    </div>
    <div class="col-md-6">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Remarks</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="remarks" value=""/>
        </div>
    </div>
    </div>
</div> -->
    <div align="right" style="margin-bottom: 20px;">
        <a href="{{ route('mini-library.show', $id) }}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
    </div>
</form>

<script>
    $('#book-create').register_fields('.errors');
</script>
