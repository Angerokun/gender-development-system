
<div class="col-md-12">
<div class="table-responsive">
    <table class="table table-striped" id="borrow-table" style="width:100%">
        <thead>
            <tr>
                <th scope="col" width="20%">Student Name</th>
                <th scope="col" width="10%">No of Copies</th>
                <th scope="col" width="20%">Book Borrow</th>
                <th scope="col" width="15%">Date Needed</th>
                <th scope="col" width="15%">Return Date</th>
                <th scope="col" width="20%">Status</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<script>
    $(function() {
        $('#borrow-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('books-borrow-list') !!}",
                columns:[
                    {data: "student","name":"student","width":"20%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("books-borrow.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "no_of_copies", 'name' :"no_of_copies","width":"10%"},
                    {data: "book", 'name' :"books","width":"20%"},
                    {data: "release_date", 'name' :"release_date","width":"15%"},
                    {data: "due_date", 'name' :"due_date","width":"15%"},
                    {data: "status", 'name' :"status","width":"15%"}
                ]
            }
        );
    });
</script>