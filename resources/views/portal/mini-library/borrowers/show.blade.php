@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div align='right'>
                @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile' && $borrow->status == 'Pending')
                    <a href="{{ route('borrow.approval', ['id' => $borrow->id, 'status' => 'Approved']) }}" title="Approved Request" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-primary"><i class="fas fa-thumbs-up"></i> Approve</button></a>
                    <a href="{{ route('borrow.approval', ['id' => $borrow->id, 'status' => 'Rejected']) }}" title="Reject Request" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-danger"><i class="fas fa-thumbs-down"></i> Reject</button></a>
                    <a href="{{ route('mini-library.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                @elseif (Auth::user()->profile_type == 'App\\Models\\AdminProfile' && $borrow->status == 'Approved')
                    <a href="{{ route('borrow_update.return_view', $borrow->id) }}" title="Return Book" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-info"><i class="fas fa-exit"></i>Return</button></a>
                    <a href="{{ route('mini-library.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                @else
                    <a href="{{ route('mini-library.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                @endIf
                </div>
                <h4 class="card-title">{{ $borrow->miniLibrary->book_no }} - {{ $borrow->miniLibrary->title }}</h4>
                <p class="card-description">
                    Book Information
                </p> 
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Book</b></label><br>
                        <p>&nbsp;{{ $borrow->miniLibrary->title  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Student</b></label><br>
                        <p>&nbsp;{{ $borrow->student->first_name . ' ' . $borrow->student->last_name }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>No of Copies</b></label><br>
                        <p>&nbsp;{{ $borrow->no_of_copies  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Date Needed</b></label><br>
                        <p>&nbsp;{{ date('F d, Y', strtotime($borrow->date_needed))  }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Status</b></label><br>
                        <p>&nbsp;{{ $borrow->status  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Return Date</b></label><br>
                        <p>&nbsp;{{ $borrow->return_date == null ? '-' : date('F d, Y', strtotime($borrow->return_date))  }}</p>
                    </div>
                    <!-- <div class="form-group col-md-6">
                        <label><b>Release Date</b></label><br>
                        <p>&nbsp;{{ $borrow->release_date == null ? '-' : date('F d, Y', strtotime($borrow->release_date))  }}</p>
                    </div> -->
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Remarks</b></label><br>
                        <p>&nbsp;{{ $borrow->remarks  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Due Date</b></label><br>
                        <p>&nbsp;{{ $borrow->due_date == null ? '-' : date('F d, Y', strtotime($borrow->due_date))  }}</p>
                    </div>
                </div>
                <!-- <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Due Date</b></label><br>
                        <p>&nbsp;{{ $borrow->due_date == null ? '-' : date('F d, Y', strtotime($borrow->due_date))  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>ID number</b></label><br>
                        <p>&nbsp;{{ $borrow->id_number  }}</p>
                    </div>
                </div> -->
                @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile')
                <!-- <hr>
                <form method="POST" class="borrow-update" id ="borrow-update" action="{{ route('books-borrow.update', $borrow->id) }}">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="errors"></div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                            <select name="status" class="form-control" id="">
                                <option value="Pending" {{ $borrow->status == 'Pending' ? 'selected': "" }}>Pending</option>
                                <option value="Rejected" {{ $borrow->status == 'Rejected' ? 'selected': "" }}>Rejected</option>
                                <option value="Approved" {{ $borrow->status == 'Approved' ? 'selected': "" }}>Approved</option>
                            </select>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Due Date</label>
                            <div class="col-sm-9">
                            <input type="date" class="form-control" name="due_date" value="{{ $borrow->due_date }}" style="border-left: solid 2.0px #ec0000" data-validation="required"/>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Release Date</label>
                            <div class="col-sm-9">
                            <input type="date" class="form-control" name="release_date" value="{{ $borrow->release_date }}" style="border-left: solid 2.0px #ec0000" data-validation="required"/>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Return Date</label>
                            <div class="col-sm-9">
                            <input type="date" class="form-control" name="return_date" value="{{ $borrow->return_date }}" />
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Remarks</label>
                            <div class="col-sm-9">
                                <textarea name="remarks" class="form-control">{{ $borrow->remarks }}</textarea>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">ID Number</label>
                            <div class="col-sm-9">
                                <input type="input" class="form-control" name="id_number" value="{{ $borrow->id_number }}" />
                            </div>
                        </div>
                        </div>
                    </div>
                    <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Update</button>
                    </div>
                </form> -->
                @endIf
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('#borrow-update').register_fields('.errors');
</script>
@endpush

