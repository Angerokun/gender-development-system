@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('mini-library.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Create Books</h4>
                <form method="POST" class="book_create" id ="book-create" action="{{ route('mini-library.store') }}">
                @csrf
                <div class="errors"></div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Book No.</label>
                        <div class="col-sm-9">
                        <input type="text" id="book_no" class="form-control" readonly="" style="border-left: solid 2.0px #ec0000" name="book_no" value="{{ $book_no }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Title</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="title" value="" style="border-left: solid 2.0px #ec0000" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">No of Copies</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="no_of_copies" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date of Publication</label>
                        <div class="col-sm-9">
                        <input type="date" class="form-control" style="border-left: solid 2.0px #ec0000" name="date_publication" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Author</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="author" value="" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Publishing Company</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="publishing_company" value=""/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea name="description" style="border-left: solid 2.0px #ec0000" data-validation="required" id="" class="form-control">

                            </textarea>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#book-create').register_fields('.errors');
</script>
@endpush
@endsection
