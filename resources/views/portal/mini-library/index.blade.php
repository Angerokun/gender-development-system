@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Mini Libraries</h4>
                <p class="card-description">
                Mini Library for GAD.
                </p>
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="books-tab" data-toggle="tab" href="#books" role="tab" aria-controls="books" aria-selected="true">Books</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="borrower-tab" data-toggle="tab" href="#borrower" role="tab" aria-controls="borrower" aria-selected="true">
                            @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile')
                                Borrowers
                            @else
                                Books Borrowed
                            @endIf
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="books" role="tabpanel" aria-labelledby="books-tab">
                            @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile')
                            <div align='right' style="margin-bottom: 10px;">
                                <a href="{{ route('mini-library.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                            </div>
                            @endIf
                            <div class="table-responsive">
                            <table class="table table-striped" id="library-table">
                                <thead>
                                    <tr>
                                        <th scope="col" width="20%">Title</th>
                                        <th scope="col" width="20%">Book No.</th>
                                        <th scope="col" width="20%">Description</th>
                                        <th scope="col" width="20%">No of Copies</th>
                                        <th scope="col" width="20%">Date of Publication</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="borrower" role="tabpanel" aria-labelledby="borrower-tab">
                            @include('portal.mini-library.borrowers.index')
                        </div>
                    </div>
                </div>
                
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#library-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('mini-library-list') !!}",
                columns:[
                    {data: "title","name":"title","width":"20%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("mini-library.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "book_no", 'name' :"book_no",orderable:true,searchable:false,"width":"20%"},
                    {data: "description", 'name' :"description",orderable:false,searchable:false,"width":"20%"},
                    {data: "no_of_copies", 'name' :"no_of_copies",orderable:true,searchable:false,"width":"20%"},
                    {data: "date_publication", 'name' :"date_publication",orderable:false,searchable:false,"width":"20%"}
                ]
            }
        );
    });
</script>
@endpush
<style>
    .table th, .table td { max-width: 200px; min-width: 70px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
</style>
@endsection