@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div align='right'>
                    @if (Auth::user()->profile_type == 'App\Models\AdminProfile')
                    <a href="{{ route('mini-library.edit', $mini_library->id) }}" style="color: orange;margin-right:10px;" ><i class="fa fa-edit"></i></a>
                    <a href="{{ route('mini-library.delete', $mini_library->id) }}" style="color: red;" title="Delete Profiling Record" class="popup_form" data-toggle="modal"><i class="fa fa-trash"></i></a>
                    @endIf
                    <a href="{{ route('mini-library.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">{{ $mini_library->book_no }} - {{ $mini_library->title }}</h4>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                    <div class="statistics-details d-flex align-items-center justify-content-between container">
                        <div>
                        <p class="statistics-title">No. of Available Books</p>
                        <h3 class="rate-percentage">{{ ($mini_library->no_of_copies - $no_borrow) }}</h3>
                        <!-- <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>-0.5%</span></p> -->
                        </div>
                        <div>
                        <p class="statistics-title">No of Borrowed Book</p>
                        <h3 class="rate-percentage">{{ $mini_library->items()->whereNull('return_date')->count() > 0 ? $mini_library->items()->whereNull('return_date')->value('no_of_copies') : '0'  }}</h3>
                        <!-- <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+0.1%</span></p> -->
                        </div>
                        <div>
                        <p class="statistics-title">Date Available</p>
                        @if(count($date_available) > 0)
                        @foreach($date_available as $date)
                            <h3 class="rate-percentage">{{ $date }} {{(date('Y-m-d') > $date ? '(Past Due)' : '')}}</h3>
                        @endforeach
                        @else
                        <h3 class="rate-percentage">Today</h3>
                        @endIf
                        
                        <!-- <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p> -->
                        </div>
                    </div>
                    </div>
                </div>
                <br>
                <hr>
                <p class="card-description">
                    Book Information
                </p>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Book No.</b></label><br>
                        <p>&nbsp;{{ $mini_library->book_no  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Title</b></label><br>
                        <p>&nbsp;{{ $mini_library->title }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>No of Copies</b></label><br>
                        <p>&nbsp;{{ $mini_library->no_of_copies  }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Description</b></label><br>
                        <p>&nbsp;{{ $mini_library->description  }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Author</b></label><br>
                        <p>&nbsp;{{ $mini_library->author }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Date of Publication</b></label><br>
                        <p>&nbsp;{{ $mini_library->date_publication == null ? '-' : date('F, Y', strtotime($mini_library->date_publication)) }}</p>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label><b>Publishing Company</b></label><br>
                        <p>&nbsp;{{ $mini_library->publishing_company }}</p>
                    </div>
                    <div class="form-group col-md-6">
                    
                    </div>
                </div>
                
                <div class="container" align='right'>
                    @if (Auth::user()->profile_type == 'App\Models\StudentProfile' 
                        && ($mini_library->no_of_copies - $no_borrow) > 0 )
                        @if (count($mini_library->items()->where('student_id', Auth::user()->profile_id)->get()) ==  0)
                            <a class="popup_form" data-toggle="modal" data-url="{{ route('borrower-book.create', $mini_library->id) }}" title="Reserved Book" href="#"><button type="button" class="btn btn-primary">Reserved Book</button></a>
                        @else
                            @if ($mini_library->items()->where('student_id', Auth::user()->profile_id)->whereNotNull('return_date')->first())
                            <a class="popup_form" data-toggle="modal" data-url="{{ route('borrower-book.create', $mini_library->id) }}" title="Borrow Book" href="#"><button type="button" class="btn btn-primary">Reserved Book</button></a>
                            @endIf
                        @endIf
                    @endIf
                </div>
                <div class="col-md-12">
                    <div class="profile-head">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="visitor-tab" data-toggle="tab" href="#visitor" role="tab" aria-controls="visitor" aria-selected="true">Borrowers Record</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="visitor" role="tabpanel" aria-labelledby="visitor-tab">
                            <div class="table-responsive">
                            <table class="table table-striped" id="logs-table">
                                <thead>
                                    <tr>
                                        <th scope="col" width="20%">Borrower</th>
                                        <th scope="col" width="20%">No of Copies</th>
                                        <th scope="col" width="20%">Date Needed</th>
                                        <th scope="col" width="20%">Return Date</th>
                                        <th scope="col" width="20%">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mini_library->items as $item)
                                    <tr>
                                        <td>
                                            {{ $item->student->first_name . ' ' . $item->student->last_name }}
                                        </td>
                                        <td>
                                            {{ $item->no_of_copies }}
                                        </td>
                                        <td>
                                            {{ date('F d, Y', strtotime($item->date_needed)) }}
                                        </td>
                                        <td>
                                            {{ $item->return_date != null ? date('F d, Y', strtotime($item->return_date)) : '-' }}
                                        </td>
                                        <td>
                                            {{ $item->status }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            </div>
        </div>
    </div>
</div>
@endsection
