@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <h4 class="card-title">Notifications</h4>
                <p class="card-description">
                Notification List
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="student-table">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">Link</th>
                            <th scope="col" width="20%">Date</th>
                            <th scope="col" width="17%">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (Auth::User()->notifications as $notification)
                        <tr>
                            <td><a href="{{$notification->data['route']}}">{{ $notification->data['profile_name'] }}</a></td>
                            <td>{{ $notification->created_at }}</td>
                            <td>{{ $notification->data['details'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#student-table').DataTable();
    });
</script>
@endpush
@endsection