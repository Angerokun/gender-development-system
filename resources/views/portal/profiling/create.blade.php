@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('profiling.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Create Student Profile</h4>
                <form method="POST" id ="student-create" action="{{ route('profiling.store') }}">
                @csrf
                <div class="errors"></div>
                <p class="card-description">
                    Personal info
                </p>
                <div class="col-md-4" align="center">
                    <div class="profile-img">
                        <img  src="{{asset('web-design/img/default.png')}}" alt="" id="im" name="im"/>
                        <div class="file btn btn-lg btn-primary">
                            <span onclick="onImage()">Upload Photo</span>
                            <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="first_name" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Middle Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="middle_name" value=""/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="last_name" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date of Birth</label>
                        <div class="col-sm-9">
                        <input type="date" class="form-control" style="border-left: solid 2.0px #ec0000" name="birth_date" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Contact Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="contact_no" value="" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Section</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="section" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Year</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="year" value="" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Gender</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="gender" style="border-left: solid 2.0px #ec0000" data-validation="required">
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="LGBTQ">LGBTQ</option>
                        </select>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Course</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="course" value="" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <!-- <div class="form-group row" style="margin-left:160px;">
                        <input class="form-check-input " type="checkbox" value="1" name="is_lgbt" id="" >
                        <label class="form-check-label" >
                            Are you a member of LGBT Community?
                        </label>
                    </div> -->
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#student-create').register_fields('.errors');
    function onImage() {
        $('#image').click();
    }

    function onFileSelected() {
        var selectedFile = event.target.files[0];
        var files = event.target.files;
        var reader = new FileReader();
        if(selectedFile && selectedFile.size < 2097152)
        {
            var imgtag = document.getElementById("im");
            imgtag.title = selectedFile.name;
            reader.onload = function(event) {
                imgtag.src = event.target.result;
            };
            reader.readAsDataURL(selectedFile);
        }
    }
</script>
@endpush
<style>
    .profile-img{
        text-align: center;
    }
    .profile-img img{
        width: 50%;
        height: 100%;
    }
    .profile-img .file {
        position: relative;
        overflow: hidden;
        margin-top: -20%;
        width: 50%;
        border: none;
        border-radius: 0;
        font-size: 15px;
        background: #212529b8;
    }
    .profile-img .file input {
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
    }
</style>
@endsection
