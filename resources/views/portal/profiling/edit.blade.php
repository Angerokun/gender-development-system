@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('profiling.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Edit Student Profile</h4>
                <form method="POST" id ="student-create" action="{{ route('profiling.update', $profiling->id) }}">
                    <input type="hidden" name="_method" value="PUT">
                @csrf
                <div class="errors"></div>
                <p class="card-description">
                    Personal info
                </p>
                <div class="col-md-4" align="center">
                    <div class="profile-img">
                        <img  src="{{ $profiling->image == null ? asset('web-design/images/default.png') : route('profiling.image', $profiling->image) }}" alt="" id="im" name="im"/>
                        <div class="file btn btn-lg btn-primary">
                            <span onclick="onImage()">Change Photo</span>
                            <input type="file" onchange="onFileSelected(event)" name="image" id="image" accept="image/*" value="submit" style="display: none;" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="first_name" value="{{ $profiling->first_name }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Middle Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" name="middle_name" value="{{ $profiling->middle_name }}"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="last_name" value="{{ $profiling->last_name }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Date of Birth</label>
                        <div class="col-sm-9">
                        <input type="date" class="form-control" style="border-left: solid 2.0px #ec0000" name="birth_date" value="{{ $profiling->birth_date }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Contact Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="contact_no" value="{{ $profiling->contact_no }}" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Section</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="section" value="{{ $profiling->section }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Year</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="year" value="{{ $profiling->year }}" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Gender</label>
                        <div class="col-sm-9">
                        <select class="form-control" name="gender" style="border-left: solid 2.0px #ec0000" data-validation="required">
                            <option value="Male" {{ $profiling->gender == 'Male' ? 'selected' : '' }}>Male</option>
                            <option value="Female" {{ $profiling->gender == 'Female' ? 'selected' : '' }}>Female</option>
                            <option value="LGBTQ" {{ $profiling->gender == 'LGBTQ' ? 'selected' : '' }}>LGBTQ</option>
                        </select>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Course</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="course" value="{{ $profiling->course }}" />
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">StudentID</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="student_id   " value="{{ $profiling->student_id }}" data-validation="required" />
                        </div>
                    </div>
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#student-create').register_fields('.errors');
    function onImage() {
        $('#image').click();
    }

    function onFileSelected() {
        var selectedFile = event.target.files[0];
        var files = event.target.files;
        var reader = new FileReader();
        if(selectedFile && selectedFile.size < 2097152)
        {
            var imgtag = document.getElementById("im");
            imgtag.title = selectedFile.name;
            reader.onload = function(event) {
                imgtag.src = event.target.result;
            };
            reader.readAsDataURL(selectedFile);
        }
    }
</script>
@endpush
<style>
    .profile-img{
        text-align: center;
    }
    .profile-img img{
        width: 50%;
        height: 100%;
    }
    .profile-img .file {
        position: relative;
        overflow: hidden;
        margin-top: -20%;
        width: 50%;
        border: none;
        border-radius: 0;
        font-size: 15px;
        background: #212529b8;
    }
    .profile-img .file input {
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
    }
</style>
@endsection
