@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    <div align='right'>
                        <a href="{{ route('profiling.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                <h4 class="card-title">Profiling</h4>
                <p class="card-description">
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="student-table">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">Image</th>
                            <th scope="col" width="20%">Name</th>
                            <th scope="col" width="17%">StudentId</th>
                            <th scope="col" width="15%">Gender</th>
                            <th scope="col" width="15%">User Type</th>
                            <th scope="col" width="15%">Contact No.</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#student-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('student-profile-list') !!}",
                columns:[
                    {data: "image","name":"image",orderable:false,searchable:false,"width":"10%", className:"v-align text-center",
                    "render": function (data) {
                        return "<center><div><img src='" + data + "' alt='Image' height='50' width='50' class='rounded-circle'></div></center>"; 
                    }},
                    {data: "full_name","name":"full_name","width":"30%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("profiling.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "student_id", 'name' :"student_id",orderable:false,searchable:false,"width":"22%",
                    "render": function (data) {
                        return "<span class='badge badge-pill badge-secondary'>"+data+"</span>"; 
                    }},
                    {data: "gender","name":"gender","width":"20%"},
                    {data: "section", 'name' :"section",orderable:false,searchable:false,"width":"15%"},
                    {data: "contact_no", 'name' :"contact_no",orderable:false,searchable:false,"width":"15%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection