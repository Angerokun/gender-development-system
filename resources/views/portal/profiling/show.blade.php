@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div align='right'>
                        <a href="{{ route('profiling.edit', $profiling->id) }}" style="color: orange;margin-right:10px;" ><i class="fa fa-edit"></i></a>
                        @if (Auth::user()->profile_type != 'App\Models\StudentProfile')
                            <a href="{{ route('profiling.delete', $profiling->id) }}" style="color: red;" title="Delete Profiling Record" class="popup_form" data-toggle="modal"><i class="fa fa-trash"></i></a>
                            <a href="{{ route('profiling.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                        @endif
                    </div>
                    <div class="emp-profile">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="profile-img">
                                        <img src="{{ $profiling->image == null ? asset('web-design/img/default.png') : route('profiling.image', $profiling->image) }}" alt=""/>
                                        <!-- <div class="file btn btn-lg btn-primary">
                                            Change Photo
                                            <input type="file" name="file"/>
                                        </div> -->
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="profile-head">
                                                <h5>
                                                    {{ $profiling->last_name . ', ' . $profiling->first_name . ' ' . $profiling->middle_name }}
                                                </h5>
                                                <h6>
                                                    {{ $profiling->user != null ? $profiling->user->email : '' }}
                                                </h6>

                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="visitor-tab" data-toggle="tab" href="#visitor" role="tab" aria-controls="visitor" aria-selected="true">Visitors Log</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="seminar-tab" data-toggle="tab" href="#seminar" role="tab" aria-controls="seminar" aria-selected="true">Seminars Attended</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="counseling-tab" data-toggle="tab" href="#counseling" role="tab" aria-controls="counseling" aria-selected="true">Counseling</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- <div align="right" class="col-md-2">
                                    <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="profile-work">
                                        <p>Basic Information</p>
                                        <a href=""><i class="fa fa-phone"></i> {{ $profiling->contact_no }}</a><br/>
                                        <a href=""><i class="fa fa-birthday-cake"></i> {{ $profiling->birth_date != null ? date('F d, Y', strtotime($profiling->birth_date)) : '' }}</a><br/>
                                        <a href=""><i class="fa fa-puzzle-piece"></i> {{ $profiling->section }}</a><br/>
                                        <a href=""><i class="fa fa-venus-mars"></i> {{ $profiling->gender }}</a><br/>
                                        <a href=""><i class="fa fa-suitcase"></i> {{ $profiling->course }}</a>
                                        <!-- <p>SKILLS</p>
                                        <a href="">Web Designer</a><br/>
                                        <a href="">Web Developer</a><br/>
                                        <a href="">WordPress</a><br/>
                                        <a href="">WooCommerce</a><br/>
                                        <a href="">PHP, .Net</a><br/> -->
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="tab-content profile-tab" id="myTabContent">
                                        <div class="tab-pane fade show active" id="visitor" role="tabpanel" aria-labelledby="visitor-tab">
                                            <div class="table-responsive">
                                            <table class="table table-striped" id="logs-table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" width="50%">Date Visit</th>
                                                        <th scope="col" width="50%">Reason of Visit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($profiling->logs as $log)
                                                    <tr>
                                                        <td>
                                                            {{ $log->date_visit }}
                                                        </td>
                                                        <td>
                                                            {{ $log->reason }}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade show" id="seminar" role="tabpanel" aria-labelledby="seminar-tab">
                                            <div class="table-responsive">
                                            <table class="table table-striped" id="seminars-table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" width="25%">Seminars/Workshops</th>
                                                        <th scope="col" width="25%">Date Attended</th>
                                                        <th scope="col" width="25%">Venue</th>
                                                        <th scope="col" width="25%">Type</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($profiling->seminars as $seminar)
                                                    <tr>
                                                        <td>{{ $seminar->seminar->name }}</td>
                                                        <td>{{ date('F d, Y h:i A', strtotime($seminar->seminar->date_schedule)) }}</td>
                                                        <td>{{ $seminar->seminar->venue }}</td>
                                                        <td>{{ $seminar->seminar->type }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade show" id="counseling" role="tabpanel" aria-labelledby="counseling-tab">
                                            <div class="table-responsive">
                                            <table class="table table-striped" id="seminars-table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col" width="25%">Background Case</th>
                                                        <th scope="col" width="25%">Goal</th>
                                                        <th scope="col" width="25%">Approach</th>
                                                        <th scope="col" width="25%">Date Created</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($profiling->counselings as $counseling)
                                                    <tr>
                                                        <td>{{ $counseling->counseling->background_case }}</td>
                                                        <td>{{ $counseling->counseling->goal }}</td>
                                                        <td>{{ $counseling->counseling->approach }}</td>
                                                        <td>{{ date('F d, Y h:i A', strtotime($counseling->counseling->created_at)) }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .circle {
        width: 200px;
        height: 200px;
        line-height: 200px;
        border-radius: 50%;
        font-size: 50px;
        color: #fff;
        text-align: center;
        background: #d7d7d7;
    }
    .progress .progress-bar {
        margin-left: 2px;
        border-radius: 1px;
    }
    .bg-success {
      background-color: #38c172 !important;
    }
    .progress {
      height: 35px;

    }
    .progress .skill {
      font: normal 12px "Open Sans Web";
      line-height: 35px;
      padding: 0;
      margin: 0 0 0 20px;
      text-transform: uppercase;
    }
    .progress .skill .val {
      float: right;
      font-style: normal;
      margin: 0 20px 0 0;
    }

    .progress-bar {
      text-align: left;
      transition-duration: 3s;
    }
    .emp-profile{
        /* padding: 3%; */
        margin-top: 1%;
        margin-bottom: 3%;
        border-radius: 0.5rem;
        background: #fff;
    }
    .profile-img{
        text-align: center;
    }
    .profile-img img{
        width: 70%;
        height: 100%;
    }
    .profile-img .file {
        position: relative;
        overflow: hidden;
        margin-top: -20%;
        width: 70%;
        border: none;
        border-radius: 0;
        font-size: 15px;
        background: #212529b8;
    }
    .profile-img .file input {
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
    }
    .profile-head h5{
        color: #333;
    }
    .profile-head h6{
        color: #0062cc;
    }
    .profile-edit-btn{
        border: none;
        border-radius: 1.5rem;
        width: 70%;
        padding: 2%;
        font-weight: 600;
        color: #6c757d;
        cursor: pointer;
    }
    .proile-rating{
        font-size: 12px;
        color: #818182;
        margin-top: 5%;
    }
    .proile-rating span{
        color: #495057;
        font-size: 15px;
        font-weight: 600;
    }
    .profile-head .nav-tabs{
        margin-bottom:5%;
    }
    .profile-head .nav-tabs .nav-link{
        font-weight:600;
        border: none;
    }
    .profile-head .nav-tabs .nav-link.active{
        border: none;
        border-bottom:2px solid #0062cc;
    }
    .profile-work{
        padding: 14%;
        margin-top: -15%;
    }
    .profile-work p{
        font-size: 12px;
        color: #818182;
        font-weight: 600;
        margin-top: 10%;
    }
    .profile-work a{
        text-decoration: none;
        color: #495057;
        font-weight: 600;
        font-size: 14px;
    }
    .profile-work ul{
        list-style: none;
    }
    .profile-tab label{
        font-weight: 600;
    }
    .profile-tab p{
        font-weight: 600;
        color: #0062cc;
    }
</style>
@push('scripts')
<script>
$('#logs-table').DataTable();
$('#seminars-table').DataTable();
</script>
<!-- <script src="{{ asset('web-design/js/chart.js') }}"></script> -->
@endpush
@endsection
