<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gender and Development | Portal</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/all.js')}}"></script>
    
    <!-- plugins:js -->
    
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('web-design-portal/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('web-design-portal/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('web-design-portal/vendors/progressbar.js/progressbar.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('web-design-portal/js/off-canvas.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/template.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/settings.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <!-- <script src="{{ asset('web-design-portal/js/dashboard.js') }}"></script> -->
    <script src="{{ asset('web-design-portal/js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}" >

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/typicons/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/js/select.dataTables.min.css') }}"> -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('web-design-portal/css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('web-design-portal/images/favicon.png') }}" />
</head>
<body>
    <!-- WRAPPER -->
	<div id="app">
        <div class="content-wrapper">
            <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div align='right' style="margin-bottom: 20px;">
                            <a href="{{ route('visitor-log.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                        </div>
                        <h4 class="card-title">Attendance Log for {{ $seminar->name }}</h4>
                        <div class="alert alert-info" role="alert">
                            Select Student if you have recurring data if not, fill name and section.
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                            <table class="table" id="bhw-table">
                                <thead>
                                    <tr align="center">
                                        <th width="15%" style="text-align:center" >Select Student</th>
                                        <th width="15%" style="text-align:center" >Name</th>
                                        <th width="15%" style="text-align:center" >Section</th>
                                        <th width="15%" style="text-align:center" >Gender</th>
                                        <th width="15%" style="text-align:center" >Contact No</th>
                                        <th width="15%" style="text-align:center" >Date and Time</th>
                                        <!-- <th width="20%" style="text-align:center" >Remarks</th> -->
                                        <th width="10%" style="text-align:center" >Action</th>
                                    </tr>
                                </thead>
                                <tbody id="table_logs">
                                    @php for($i = 0; $i < 5; $i++) { @endphp
                                    <tr align="center">
                                        <td>
                                            <select data-live-search="true" id="selectpick{{$i}}" title="Choose Student..." class="form-control select-picker" name="select_student{{$i}}">
                                                    <option value="">Create New</option>
                                                @foreach($students as $student)
                                                    <option value="{{ $student->id }}" data-subtext="{{ $student->section }}">{{ $student->first_name . ' ' .$student->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" name="name" id="name{{$i}}" ></td>
                                        <td><input type="text" class="form-control" name="section" id="section{{$i}}" ></td>
                                        <td>
                                            <select class="form-control" name="gender" id="gender{{$i}}">
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" name="contact_no" id="contact_no{{$i}}" ></td>
                                        <td><input type="datetime-local" class="form-control" name="date" id="date{{$i}}"></td>
                                        <td><button class="btn btn-primary" id="save{{$i}}" onclick="addLogs({{$i}})">Save</button></td>
                                    </tr>
                                    @php } @endphp
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-7" align="left">
                                    <button class="btn btn-primary addNew" type="button" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus-square" aria-hidden="true"></i> ADD ROW</button>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="{{ asset('web-design-portal/vendors/js/vendor.bundle.base.js') }}"></script>
<script>
    $('.select-picker').selectpicker();
    $('.name_new').hide();

    $('#selectpick0').change(function () {
        var pick = $('#selectpick0').find(":selected").val();
        if(pick != '') {
            $('#name0').prop("disabled", true );
            $('#section0').prop("disabled", true );
            $('#gender0').prop("disabled", true );
            $('#contact_no0').prop("disabled", true );
        } else {
            $('#gender0').prop("disabled", false );
            $('#contact_no0').prop("disabled", false );
            $('#name0').prop("disabled", false );
            $('#section0').prop("disabled", false );
        }
    });

    $('#selectpick1').change(function () {
        var pick = $('#selectpick1').find(":selected").val();
        if(pick != '') {
            $('#gender1').prop("disabled", true );
            $('#contact_no1').prop("disabled", true );
            $('#name1').prop("disabled", true );
            $('#section1').prop("disabled", true );
        } else {
            $('#gender1').prop("disabled", false );
            $('#contact_no1').prop("disabled", false );
            $('#name1').prop("disabled", false );
            $('#section1').prop("disabled", false );
        }
    });

    $('#selectpick2').change(function () {
        var pick = $('#selectpick2').find(":selected").val();
        if(pick != '') {
            $('#gender2').prop("disabled", true );
            $('#contact_no2').prop("disabled", true );
            $('#name2').prop("disabled", true );
            $('#section2').prop("disabled", true );
        } else {
            $('#gender2').prop("disabled", false );
            $('#contact_no2').prop("disabled", false );
            $('#name2').prop("disabled", false );
            $('#section2').prop("disabled", false );
        }
    });

    $('#selectpick3').change(function () {
        var pick = $('#selectpick3').find(":selected").val();
        if(pick != '') {
            $('#gender3').prop("disabled", true );
            $('#contact3').prop("disabled", true );
            $('#name3').prop("disabled", true );
            $('#section3').prop("disabled", true );
        } else {
            $('#gender3').prop("disabled", false );
            $('#contact3').prop("disabled", false );
            $('#name3').prop("disabled", false );
            $('#section3').prop("disabled", false );
        }
    });

    $('#selectpick4').change(function () {
        var pick = $('#selectpick4').find(":selected").val();
        if(pick != '') {
            $('#gender4').prop("disabled", true );
            $('#contact_no4').prop("disabled", true );
            $('#name4').prop("disabled", true );
            $('#section4').prop("disabled", true );
        } else {
            $('#gender4').prop("disabled", false );
            $('#contact_no4').prop("disabled", false );
            $('#name4').prop("disabled", false );
            $('#section4').prop("disabled", false );
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function addLogs(rows) {
        $.ajax({
            type: "POST",
            url: "{{route('seminar.attendance.store', $id)}}",
            data: {
                row: rows,
                name: $('#name'+rows).val(),
                student_id: $('#selectpick'+rows).find(":selected").val(),
                section: $("#section"+rows).val(),
                gender: $('#gender'+rows).val(),
                contact_no: $('#contact_no'+rows).val(),
                date: $('#date'+rows).val(),
            },
            dataType: "json",
        }).done(function (data) {
                toastMessage('Success',data.desc,data.status);
                $('#save'+rows).prop("disabled", true );
                $('#name'+rows).prop("disabled", true );
                $('#selectpick'+rows).prop("disabled", true );
                $('#section'+rows).prop("disabled", true );
                $('#date'+rows).prop("disabled", true );
                $('#contact_no'+rows).prop("disabled", true );
                $('#gender'+rows).prop("disabled", true );
        });
    }

    var countItem = 4;
    $(".addNew").click(function(e){
        e.preventDefault();
        countItem ++;
        $('#table_logs').append('<tr align="center" class="appendRow">'+
            '<tr align="center">'+
                '<td><select data-live-search="true" id="selectpick'+countItem+'" title="Choose Student..." class="form-control select-picker'+countItem+'" name="select_student'+countItem+'">'+
                    '<option value="">Create New</option>'+
                    @foreach($students as $student)
                        '<option value="{{ $student->id }}" data-subtext="{{ $student->section }}">{{ $student->first_name . ' ' .$student->last_name }}</option>'+
                    @endforeach
                '</select></td>'+
                '<td><input type="text" class="form-control" placeholder="" name="name" id="name'+countItem+'"></td>'+
                '<td><input type="text" class="form-control" name="section" id="section'+countItem+'"></td>'+
                '<td>'+
                    '<select class="form-control" name="gender" id="gender'+countItem+'">'+
                        '<option value="">Select Gender</option>'+
                        '<option value="Male">Male</option>'+
                        '<option value="Female">Female</option>'+
                    '</select>'+
                '</td>'+
                '<td><input type="text" class="form-control" name="contact_no" id="contact_no'+countItem+'" ></td>'+
                '<td><input type="datetime-local" class="form-control" name="date" id="date'+countItem+'"></td>'+
                '<td><button class="btn btn-primary" id="save'+countItem+'" onclick="addLogs('+countItem+')">Save</button></td>'+
            '</tr>');

        $('.select-picker'+countItem).selectpicker();

        $('#selectpick'+countItem).change(function () {
            var id = '#selectpick'+countItem;
            
            var pick = $(id).find(":selected").val();
           
            if(pick != '') {
                $('#gender'+countItem).prop("disabled", true );
                $('#contact_no'+countItem).prop("disabled", true );
                $('#name'+countItem).prop("disabled", true );
                $('#section'+countItem).prop("disabled", true );
            } else {
                $('#gender'+countItem).prop("disabled", false );
                $('#contact_no'+countItem).prop("disabled", false );
                $('#name'+countItem).prop("disabled", false );
                $('#section'+countItem).prop("disabled", false );
            }
        });

        
    })
</script>

</html>
