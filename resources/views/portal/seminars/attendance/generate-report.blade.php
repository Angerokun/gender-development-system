<div align="center">
    <h3>Seminars and Workshop for Gender and Development LSPU</h3>
</div>
<div align="left">
<p class="pad">Seminar/Workshop: {{ $seminar->name }}</p>
    <p class="pad">Date: <span id="purok_label">{{ date('F d, Y h:i A', strtotime($seminar->date_schedule)) }}</span></p>
</div>
<div align="right" style="margin-top: -30px">
<p class="pad">Year: <span id="year_label">{{ date('Y', strtotime($seminar->date_schedule)) }}</span></p>
</div>

<table class="table table-bordered text-center mt-table" id="tracking_table" >
    <thead>
        <tr>
            <th scope="col" width="25%">Name</th>
            <th scope="col" width="10%">Section</th>
            <th scope="col" width="25%">Date and Time</th>
            <th scope="col" width="25%">Date/Time Attended</th>
            <th scope="col" width="10%">Signature</th>
        </tr>
    </thead>
    <tbody class="report" name="report">
        @foreach ($seminar->attendance as $attendance)
        <tr>
            <td>{{ $attendance->student->first_name . ' ' . $attendance->student->last_name }}</td>
            <td>{{ $attendance->student->section == null ? '-' : $attendance->student->section }}</td>
            <td>{{ date('F d, Y h:i A', strtotime($attendance->date)) }}</td>
            <td>
                @if ($attendance->date_attended == null && new DateTime() < new DateTime($seminar->date_schedule))
                    <span class="badge badge-secondary"></span>
                @elseIf (new DateTime($seminar->date_schedule) < new DateTime())
                    <span class="badge badge-danger text-white">Absent</span>
                @else
                    {{ date('F d, Y H:i: A', strtotime($attendance->date_attended)) }}
                @endIf
            </td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
    <!-- <tbody class="nodatatr" name="nodatatr">
        <tr align="center" >
            <td colspan="10">No Data Available</td>
        </tr>
    </tbody> -->
</table>
<!-- <div align="left">
    <p class="pad">Prepared By: ______________________</p>
    <p class="pad" style="margin-top: -15px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Barangay Nutrition Scholar</p>
</div>
<div align="right" style="margin-top: -50px">
    <p class="pad">Verified By: ______________________</p>
    <p class="pad" style="margin-top: -15px">Barangay Captain</p>
</div> -->

<style>
    .border{
        border: 3px solid #000;
    }
    .table thead th, .table td {
        vertical-align: middle !important;
    }
    .input-total-pr { 
        border: none;
        background-color: transparent;
    }
    .border-b{
        border-bottom: 1px solid #000;
        width: 250px;
        text-align:center;
    }
   
</style>
