@if (Auth::user()->profile_type == 'App\\Models\\AdminProfile')
    <div align='right'>
        <button type="button" class="btn btn-primary print-form-report" data-form="print_attendance_report"><i class="fas fa-download"></i> Download or Print</button>
        <a href="{{ route('seminar.attendance.create', $seminar->id) }}" target="_blank"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
    </div>
@elseif (Auth::user()->profile_type == 'App\\Models\\StudentProfile' && $seminar->attendance()->where('student_profile_id', Auth::user()->profile_id)->first())
    @if ($seminar->attendance()->where('student_profile_id', Auth::user()->profile_id)->whereNull('date_attended')->first())
    <div align='right'>
        <form id="attend-form" action="{{ route('user.attend.seminar', $seminar->id) }}" method="POST" class="d-none">
            @csrf
        </form>
        <a href="" 
            onclick="event.preventDefault(); document.getElementById('attend-form').submit();"><button type="button" class="btn btn-primary">Attend</button></a>
    </div>
    @endIf
@endIf
<h4  class="card-title">Participant</h4>
<div class="table-responsive">
<table class="table table-striped" id="attendance-table">
    <thead>
        <tr>
            <th scope="col" width="50%">Name</th>
            <th scope="col" width="25%">Section</th>
            <th scope="col" width="25%">Date and Time</th>
            <th scope="col" width="25%">Date/Time Attended</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($seminar->attendance as $attendance)
        <tr>
            <td>{{ $attendance->student->first_name . ' ' . $attendance->student->last_name }}</td>
            <td>{{ $attendance->student->section == null ? '-' : $attendance->student->section }}</td>
            <td>{{ date('F d, Y h:i A', strtotime($attendance->date)) }}</td>
            <td>
                @if ($attendance->date_attended == null && new DateTime() < new DateTime($seminar->date_schedule))
                    <span class="badge badge-secondary">Pending</span>
                @elseIf (new DateTime($seminar->date_schedule) < new DateTime())
                    <span class="badge badge-danger text-white">Absent</span>
                @else
                    {{ date('F d, Y H:i: A', strtotime($attendance->date_attended)) }}
                @endIf
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div id="print_attendance_report" hidden>
        @include('portal.seminars.attendance.generate-report')
    <style>
        .mt-table{
            margin-top: 1.5rem!important;
        }
        .bbm-supply {
                border-bottom: 2px solid;
                position: inherit;
                margin-bottom: -20px;
                margin-top: -1rem;
                width: 80%;
            }
        </style>
    </div>
</div>
@push('scripts')
    <script>
    $('#attendance-table').DataTable();
    </script>
@endpush