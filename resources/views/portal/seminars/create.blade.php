@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('seminar.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Create Seminar/Training/Workshop</h4>
                <form method="POST" id ="seminar-create" action="{{ route('seminar.store') }}">
                @csrf
                <div class="errors"></div>
                <p class="card-description">
                    Seminar/Training/Workshop Information
                </p>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Seminar Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="name" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Schedule Date</label>
                        <div class="col-sm-9">
                        <input type="datetime-local" class="form-control" style="border-left: solid 2.0px #ec0000" name="date_schedule" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Type</label>
                        <div class="col-sm-9">
                            <select class="form-control" style="border-left: solid 2.0px #ec0000" name="type" value="" data-validation="required">
                                <option value="">Select Type</option>
                                <option value="Seminar">Seminar</option>
                                <option value="Training">Training</option>
                                <option value="Workshop">Workshop</option>
                            </select>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Venue</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="venue" value="" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Select User to Notify</label>
                        <div class="col-sm-9">
                        <select id="notify" data-validation="required" title="Choose Category" class="form-control select-picker" multiple name="notify[]">
                                <option value="">Select User</option>
                                <option value="Student">Student</option>
                                <option value="Admin">Admin</option>
                                <option value="Faculty">Faculty</option>
                                <option value="Non-Teaching">Non-teaching</option>
                            </select>
                        </div>
                    </div>
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('.select-picker').selectpicker();
    $('#seminar-create').register_fields('.errors');
</script>
@endpush
@endsection
