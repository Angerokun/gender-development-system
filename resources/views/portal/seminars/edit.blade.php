@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right' style="margin-bottom: 20px;">
                    <a href="{{ route('seminar.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4 class="card-title">Update Seminar/Training/Workshop</h4>
                <form method="POST" id ="seminar-edit" action="{{ route('seminar.update', $seminar->id) }}">
                <input type="hidden" name="_method" value="PUT">
                @csrf
                <div class="errors"></div>
                <p class="card-description">
                    Seminar/Training/Workshop Information
                </p>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Schedule Name</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="name" value="{{ $seminar->name }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Schedule Date</label>
                        <div class="col-sm-9">
                        <input type="datetime-local" class="form-control" style="border-left: solid 2.0px #ec0000" name="date_schedule" value="{{ date( 'Y-m-d\TH:i:s', strtotime($seminar->date_schedule)) }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Type</label>
                        <div class="col-sm-9">
                            <select class="form-control" style="border-left: solid 2.0px #ec0000" name="type" data-validation="required">
                                <option value="">Select Type</option>
                                <option value="Seminar" {{ $seminar->type == 'Seminar' ? 'selected' : '' }}>Seminar</option>
                                <option value="Training" {{ $seminar->type == 'Training' ? 'selected' : '' }}>Training</option>
                                <option value="Workshop" {{ $seminar->type == 'Workshop' ? 'selected' : '' }}>Workshop</option>
                            </select>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Venue</label>
                        <div class="col-sm-9">
                        <input type="text" class="form-control" style="border-left: solid 2.0px #ec0000" name="venue" value="{{ $seminar->venue }}" data-validation="required"/>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea name="description" class="form-control">{{ $seminar->description }}</textarea>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Select Evaluation</label>
                        <div class="col-sm-9">
                            <select class="form-control" style="border-left: solid 2.0px #ec0000" name="evaluation_template_id" value="" data-validation="required">
                                <option value="">Select Template Evaluation</option>
                                @foreach($evaluations as $evaluation)
                                    <option value="{{ $evaluation->id }}" {{ $evaluation->id == $seminar->evaluation_template_id ? 'selected' : '' }}>{{ $evaluation->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    </div>
                </div>
                <div align="right" style="margin-bottom: 20px;">
                        <button type="submit" id="submit_product" class="btn btn-warning"><i class="fa fa-edit" ></i> Update</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#seminar-edit').register_fields('.errors');
</script>
@endpush
@endsection
