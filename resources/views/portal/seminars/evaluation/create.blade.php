<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gender and Development | Portal</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/all.js')}}"></script>
    
    <!-- plugins:js -->
    
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('web-design-portal/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('web-design-portal/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('web-design-portal/vendors/progressbar.js/progressbar.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('web-design-portal/js/off-canvas.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/template.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/settings.js') }}"></script>
    <script src="{{ asset('web-design-portal/js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <!-- <script src="{{ asset('web-design-portal/js/dashboard.js') }}"></script> -->
    <script src="{{ asset('web-design-portal/js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}" >

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/typicons/typicons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- <link rel="stylesheet" href="{{ asset('web-design-portal/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('web-design-portal/js/select.dataTables.min.css') }}"> -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('web-design-portal/css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('web-design-portal/images/favicon.png') }}" />
</head>
<body>
    <!-- WRAPPER -->
	<div id="app">
        <div class="content-wrapper">
            <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <!-- <div align='right' style="margin-bottom: 20px;">
                            <a href="{{ route('visitor-log.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                        </div> -->
                        <h4 class="card-title">{{ $seminar->name }}</h4>
                        <p class="card-description">
                            {{ $evaluation_template->instructions }}
                        </p>
                        <form method="POST" id ="evaluation-create" action="{{ route('seminar.evaluation.store', $seminar->id) }}">
                            @csrf
                            <div class="errors"></div>
                            <div class="col-md-12">
                                <div class="container">
                                    <div class="table-responsive">
                                        <table class="table" >
                                            <tbody >
                                                <tr align="center">
                                                    <td>5 = <strong>Strongly Agree</strong></td>
                                                    <td>4 = <strong>Agree</strong></td>
                                                    <td>3 = <strong>Moderately Agree</strong></td>
                                                    <td>2 = <strong>Disagree</strong></td>
                                                    <td>1 = <strong>Strongly Disagree</strong></td>
                                                </tr>
                                                <tr align="center">
                                                    <td>(Lubos na sumasang-ayon)</td>
                                                    <td>(Sumasang-ayon)</td>
                                                    <td>(Bahagyang Sumasang-ayon)</td>
                                                    <td>(Di sumasang-ayon)</td>
                                                    <td>(Lubos na di sumasangayon)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 col-form-label">Name:</label>
                                    <div class="col-sm-9">
                                        <select data-live-search="true" data-validation="required" style="border-left: solid 2.0px #ec0000" title="Choose Student..." class="form-control select-picker" name="student_id">
                                                <option value="">Select Student</option>
                                            @foreach($students as $student)
                                                <option value="{{ $student->id }}" data-subtext="{{ $student->section }}">{{ $student->first_name . ' ' .$student->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                <table class="table" id="bhw-table">
                                    <thead>
                                        <tr align="center">
                                            <th width="50%"></th>
                                            <!-- <th width="20%" style="text-align:center" >Remarks</th> -->
                                            <th width="50%" style="text-align:center"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_logs">
                                        @foreach ($evaluation_template->items as $item)
                                            @if ($item->evaluation_template_item_id == null)
                                            <tr align="left">
                                                <td><strong>{{$item->questionaire}}</strong></td>
                                                <td></td>
                                            </tr>
                                            @php 
                                                $subModules = App\Models\EvaluationTemplateItem::where('evaluation_template_item_id', $item->id)->get(); 
                                            @endphp
                                            @foreach($subModules as $key => $sub)
                                            <tr align="left">
                                                <td>{{ $key+1 . '.' }}  {{$sub->questionaire}}</td>
                                                <td>
                                                    <input type="hidden" name="data_arr[]" value="{{ $sub->id }}">
                                                    @if($sub->is_mark == 1)
                                                    <div class="col-md-6">
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="is_mark{{ $sub->id }}" id="is_mark" value="5">
                                                                5
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="is_mark{{ $sub->id }}" id="is_mark" value="4">
                                                                4
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="is_mark{{ $sub->id }}" id="is_mark" value="3" >
                                                                3
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="is_mark{{ $sub->id }}" id="is_mark" value="2">
                                                                2
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="is_mark{{ $sub->id }}" id="is_mark" value="1">
                                                                1
                                                                </label>
                                                            </div>
                                                    </div>
                                                    @else
                                                        <textarea name="is_essay{{ $sub->id }}" id="" class="form-control" ></textarea>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Suggestion/Comments:</label>
                                    <div class="col-sm-10">
                                        <textarea name="comments" class="form-control"></textarea>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div align="right" style="margin-bottom: 20px;">
                                <button type="submit" id="submit_product" class="btn btn-primary"><i class="fa fa-save" ></i> Submit</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="{{ asset('web-design-portal/vendors/js/vendor.bundle.base.js') }}"></script>
<script>
    $('#evaluation-create').register_fields('.errors');
</script>
<style>
    .form-check-inline {
        display: inline-block !important;
        margin-right: 1rem;
    }
</style>
</html>
