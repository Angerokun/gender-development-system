@if(($seminar->attendance->where('student_profile_id', Auth::user()->profile_id)->first() && !$seminar->evaluations->where('student_id', Auth::user()->profile_id)->first()) 
|| Auth::user()->profile_type == 'App\\Models\\AdminProfile')
<div align='right'>
    <a href="{{ route('seminar.evaluation.create', $seminar->id) }}" target="_blank"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
</div>
@endIf
<h4  class="card-title">Evaluation</h4>
<div class="table-responsive">
<table class="table table-striped" id="evaluation-table">
    <thead>
        <tr>
            <th scope="col" width="25%">Name</th>
            <th scope="col" width="25%">Section</th>
            <th scope="col" width="25%">Comments</th>
            <th scope="col" width="25%">Date Created</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($seminar->evaluations as $evaluation)
        <tr>
            <td><a href='{{ route("evaluation.show", $evaluation->id) }}'>{{ $evaluation->student->first_name . ' ' . $evaluation->student->last_name }}</a></td>
            <td>{{ $evaluation->student->section }}</td>
            <td>{{ $evaluation->comments }}</td>
            <td>{{ date('F d, Y h:i A', strtotime($evaluation->created_at)) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@push('scripts')
    <script>
    $('#evaluation-table').DataTable();
    </script>
@endpush