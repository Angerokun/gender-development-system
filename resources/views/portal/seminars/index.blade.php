@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                    @if (Auth::user()->profile_type != 'App\Models\StudentProfile')
                    <div align='right'>
                        <a href="{{ route('seminar.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
                    </div>
                    @endif
                <h4 class="card-title">Seminars</h4>
                <p class="card-description">
                Seminars and Workships.
                </p>
                <div class="table-responsive">
                <table class="table table-striped" id="seminar-table">
                    <thead>
                        <tr>
                            <th scope="col" width="25%">Name</th>
                            <th scope="col" width="25%">Date Schedule</th>
                            <th scope="col" width="25%">Venue</th>
                            <th scope="col" width="25%">Type</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(function() {
        $('#seminar-table').DataTable(
            {
                responsive: true,
                pagingType: "full_numbers",
                processing: true,
                serverSide: true,
                type: "GET",
                ajax:"{!! route('seminar-list') !!}",
                columns:[
                    {data: "name","name":"name","width":"25%",
                            "render": function (data, type, row, meta) {
                                var url = '{{ route("seminar.show", ":id") }}';
                                url = url.replace(':id', row.id);
                                return "<a href='"+url+"'>"+data+"</a>"; 
                    }},
                    {data: "date_schedule", 'name' :"date_schedule",orderable:false,searchable:false,"width":"25%"},
                    {data: "venue","name":"venue","width":"25%"},
                    {data: "type", 'name' :"type",orderable:false,searchable:false,"width":"25%"}
                ]
            }
        );
    });
</script>
@endpush
@endsection