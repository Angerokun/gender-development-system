<form method="POST" id="remove_bhw_form"  class="form-horizontal" action="{{route('seminar.join',$seminar->id)}}" enctype="multipart/form-data">
    {{csrf_field()}}
    <h6> Are you sure you want to register {{$seminar->name}}?</h6>
    <div class="form-group" style="margin-top: 20px">
        <div class="col-12" align="right">
            <div class="col-12">
                <a href="{{ route('seminar.show', $seminar->id) }}" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Join</button>
            </div>
        </div>
    </div>
</form>
