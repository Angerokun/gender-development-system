@extends('portal.layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="row">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <div align='right'>
                    @if (Auth::user()->profile_type == 'App\\Models\\AdminProfile')
                    <a href="{{ route('seminar.edit', $seminar->id) }}" style="color: orange;margin-right:10px;" ><i class="fa fa-edit"></i></a>
                    <a href="{{ route('seminar.delete', $seminar->id) }}" style="color: red;" title="Delete Seminar Record" class="popup_form" data-toggle="modal"><i class="fa fa-trash"></i></a>    
                    @else
                    @if(!$seminar->attendance->where('student_profile_id', Auth::user()->profile_id)->first() && new DateTime($seminar->date_schedule) > new DateTime())
                    <a href="{{ route('seminar.register', $seminar->id) }}" title="Join Seminar" class="popup_form" data-toggle="modal"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> Register</button></a>
                    @endIf
                    @endif
                    <a href="{{ route('seminar.index') }}"><button type="button" class="btn btn-default"><i class="fas fa-chevron-left"></i> Back</button></a>
                </div>
                <h4  class="card-title">{{ $seminar->name }} - {{ date('F d, Y h:i A', strtotime($seminar->date_schedule)) }}</h4><hr>
                <h5>{{ $seminar->venue }}</h5>
                <p class="card-description">
                   {{ $seminar->description }}
                </p>
                <div class="form-row">
                    <div class="form-group col-md-12">
                    </div>
                </div>
                    <hr>

                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="attendance-tab" data-toggle="tab" href="#attendance" role="tab" aria-controls="attendance" aria-selected="true">Participant</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="evaluation-tab" data-toggle="tab" href="#evaluation" role="tab" aria-controls="evaluation" aria-selected="true">Evaluation</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="attendance" role="tabpanel" aria-labelledby="attendance-tab">
                                @include('portal.seminars.attendance.index')
                            </div>
                            <div class="tab-pane fade show" id="evaluation" role="tabpanel" aria-labelledby="evaluation-tab">
                                @include('portal.seminars.evaluation.index')
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
