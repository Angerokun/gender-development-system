@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top: 100px;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Account Verification</div>

                <div class="card-body text-center">
                    <h5>Your Account was Successfully Created!</h5>
                    <br>
                    <p style="color: #898798;">We will get back you once we verify your Account.<br>If there's any concerns or questions feel free to contact us.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection