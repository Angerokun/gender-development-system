@extends('layouts.app')
@section('content')
<div class="container mt-5">
    <div class="page-banner">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col-md-6">
        <nav aria-label="Breadcrumb">
            <ul class="breadcrumb justify-content-center py-0 bg-transparent">
            <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item active">Services</li>
            </ul>
        </nav>
        <h1 class="text-center">Services</h1>
        </div>
    </div>
    </div>
</div>

<div class="page-section">
    <div class="container">
    <div class="row">
        <div class="col-lg-6 py-3">
        <h2 class="title-section">We're <span class="marked">ready to</span> Serve you with best</h2>
        <div class="divider"></div>
        <p class="mb-5">We provide Counseling, workshops/seminars to help for the student with mental health problems etc.</p>
        <a href="{{ route('register') }}" class="btn btn-primary">Try now</a>
        </div>
        <div class="col-lg-6 py-3">
        <div class="img-place text-center">
            <img src="{{ asset('web-design/img/bg_image_3.png') }}" alt="">
        </div>
        </div>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->

<div class="page-section">
    <div class="container">
    <div class="text-center">
        <div class="subhead">Why Choose Us</div>
        <h2 class="title-section">Your <span class="marked">Comfort</span> is Our Priority</h2>
        <div class="divider mx-auto"></div>
    </div>

    <div class="row mt-5 text-center">
        <div class="col-lg-4 py-3">
        <div class="display-3"><span class="mai-shapes"></span></div>
        <h5>Counseling</h5>
        <p>professional guidance of the individual by utilizing psychological methods especially in collecting case history data, using various techniques of the personal interview, and testing interests and aptitudes</p>
        </div>
        <div class="col-lg-4 py-3">
        <div class="display-3"><span class="mai-shapes"></span></div>
        <h5>Seminars and Workshops</h5>
        <p>Workshops and seminars are short courses that place emphasis on discussing and exploring specific topics of interests; however, compared to other short courses that cover several topics or competencies during the length of the program, they usually focus on one particular topic or competency at a time.</p>
        </div>
        <div class="col-lg-4 py-3">
        <div class="display-3"><span class="mai-shapes"></span></div>
        <h5>Mini Library</h5>
        <p>Request Books via online and wait for the approval for release date at the Office. Books related on Gender and Development.</p>
        </div>
    </div>
    </div> <!-- .container -->
</div> <!-- .page-section -->
@endsection