<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/about', [App\Http\Controllers\AboutController::class, 'index'])->name('about');
Route::get('/services', [App\Http\Controllers\ServiceController::class, 'index'])->name('services');
Route::get('/albums-web', [App\Http\Controllers\NewsController::class, 'index'])->name('albums-web.index');
Route::get('/albums-web/{id}', [App\Http\Controllers\NewsController::class, 'show'])->name('albums-web.show');
Route::get('/contact', [App\Http\Controllers\ContactController::class, 'index'])->name('contact');
Route::get('/privacy-policy', [App\Http\Controllers\HomeController::class, 'privacyPolicy'])->name('privacy-policy');

Route::get('/register-verification', [App\Http\Controllers\AccountVerificationController::class,'registerVerification'])->name('register-verification.index');

Route::group(
    ['prefix' => 'portal'],
    function () {
        Route::get('dashboard', [App\Http\Controllers\DashboardController::class,'index'])->name('portal.index');
        Route::resource('profiling', App\Http\Controllers\StudentProfileController::class);

        Route::resource('account-verification', App\Http\Controllers\AccountVerificationController::class);

        Route::get('account-verification/{id}/verify/{status}', [App\Http\Controllers\AccountVerificationController::class, 'verifyModal'])->name('account-verification.verify-modal');

        Route::put('account-verification/{id}/verify/{status}', [App\Http\Controllers\AccountVerificationController::class, 'verify'])->name('account-verification.verify');

        Route::get(
            'student-profile-list',
            [App\Http\Controllers\StudentProfileController::class,
            'studentProfileDataTable']
        )->name('student-profile-list');

        Route::get(
            '/profiling/delete/{id}',
            [App\Http\Controllers\StudentProfileController::class,'delete']
        )->name('profiling.delete');

        Route::get(
            '/profiling/image/{file}',
            [App\Http\Controllers\StudentProfileController::class, 'getImage']
        )->name('profiling.image');

        Route::resource('visitor-log', App\Http\Controllers\VisitorLogController::class);

        Route::get(
            'visitor-log-list',
            [App\Http\Controllers\VisitorLogController::class,
            'visitorLogDatatable']
        )->name('visitor-log-list');

        Route::get(
            '/visitor-log/delete/{id}',
            [App\Http\Controllers\VisitorLogController::class,'delete']
        )->name('visitor-log.delete');

        Route::resource('file-doc', App\Http\Controllers\FileDocController::class);

        Route::get(
            'file-doc-list',
            [App\Http\Controllers\FileDocController::class,
            'fileDocDatatable']
        )->name('file-doc-list');

        Route::get(
            '/file-doc/delete/{id}',
            [App\Http\Controllers\FileDocController::class,'delete']
        )->name('file-doc.delete');

        Route::get(
            '/download-docs/{id}',
            [App\Http\Controllers\FileDocController::class, 'downloadDocs'],
        )->name('download.docs');

        Route::resource('seminar', App\Http\Controllers\SeminarController::class);

        Route::get(
            'seminar-list',
            [App\Http\Controllers\SeminarController::class,
            'seminarDatatable']
        )->name('seminar-list');

        Route::get(
            '/seminar/delete/{id}',
            [App\Http\Controllers\SeminarController::class,'delete']
        )->name('seminar.delete');

        Route::get(
            '/seminar/register/{id}',
            [App\Http\Controllers\SeminarController::class,'register']
        )->name('seminar.register');

        Route::post(
            '/seminar/join/{id}',
            [App\Http\Controllers\SeminarController::class,'join']
        )->name('seminar.join');

        Route::get(
            'seminar/{id}/attendance',
            [App\Http\Controllers\SeminarAttendanceController::class,'create']
        )->name('seminar.attendance.create');

        Route::post(
            'seminar/{id}/attendance/user',
            [App\Http\Controllers\SeminarAttendanceController::class,'attend']
        )->name('user.attend.seminar');

        Route::post(
            'seminar/{id}/attendance',
            [App\Http\Controllers\SeminarAttendanceController::class,'store']
        )->name('seminar.attendance.store');

        Route::resource('evaluation', App\Http\Controllers\StudentEvaluationController::class);

        Route::get(
            'seminar/{id}/evaluation',
            [App\Http\Controllers\StudentEvaluationController::class,'create']
        )->name('seminar.evaluation.create');

        Route::post(
            'seminar/{id}/evaluation',
            [App\Http\Controllers\StudentEvaluationController::class,'store']
        )->name('seminar.evaluation.store');

        Route::get(
            'evaluation-list',
            [App\Http\Controllers\StudentEvaluationController::class,
            'evaluationDatatable']
        )->name('evaluation-list');

        Route::resource('counseling', App\Http\Controllers\CounselingController::class);

        Route::get(
            'counseling-list',
            [App\Http\Controllers\CounselingController::class,
            'individualCounselingDatatable']
        )->name('counseling-list');

        Route::get(
            '/counseling/delete/{id}',
            [App\Http\Controllers\CounselingController::class,'delete']
        )->name('counseling.delete');

        Route::resource('mini-library', App\Http\Controllers\MiniLibraryController::class);

        Route::get(
            'mini-library-list',
            [App\Http\Controllers\MiniLibraryController::class,
            'miniLibraryDatatable']
        )->name('mini-library-list');

        Route::get(
            '/mini-library/delete/{id}',
            [App\Http\Controllers\MiniLibraryController::class,'delete']
        )->name('mini-library.delete');

        Route::post('mini-library/{id}/borrower', [App\Http\Controllers\MiniLibraryItemController::class,
        'store'])->name('borrower-book.store');

        Route::get('mini-library/{id}/borrower', [App\Http\Controllers\MiniLibraryItemController::class,
        'create'])->name('borrower-book.create');

        Route::get(
            'books-borrow-list',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'borroBooksDatatable']
        )->name('books-borrow-list');

        Route::get(
            'books/borrow/{id}',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'show']
        )->name('books-borrow.show');

        Route::put(
            'books/borrow/{id}',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'update']
        )->name('books-borrow.update');

        Route::get(
            'borrow/request/{id}/status/{status}',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'statusUpdate']
        )->name('borrow.approval');

        Route::put(
            'borrow_book/{id}/status/{status}',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'borrowApproved']
        )->name('borrow_update.approval');

        Route::get(
            'borrow_book/{id}/return',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'returnBookView']
        )->name('borrow_update.return_view');

        Route::put(
            'borrow_book/{id}/return',
            [App\Http\Controllers\MiniLibraryItemController::class,
            'returnBook']
        )->name('borrow_update.return');

        Route::resource('albums', App\Http\Controllers\AlbumController::class);

        Route::get(
            'album-list',
            [App\Http\Controllers\AlbumController::class,
            'albumDatatable']
        )->name('album-list');

        Route::get(
            '/album/delete/{id}',
            [App\Http\Controllers\AlbumController::class,'delete']
        )->name('albums.delete');

        Route::post(
            'albums-items/{id}',
            [App\Http\Controllers\AlbumController::class,'addPhotoAlbum']
        )->name('album-items.store');

        Route::resource('general-settings', App\Http\Controllers\GeneralController::class);

        Route::resource('counseling-request', App\Http\Controllers\CounselingRequestController::class);

        Route::get(
            'counseling-request-list',
            [App\Http\Controllers\CounselingRequestController::class,
            'counselingRequestDatatable']
        )->name('counseling-request-list');

        Route::get(
            'request/{id}/status/{status}',
            [App\Http\Controllers\CounselingRequestController::class,
            'statusUpdate']
        )->name('request.approval');

        Route::put(
            'request/{id}/status/{status}',
            [App\Http\Controllers\CounselingRequestController::class,
            'updateCounseling']
        )->name('request.update');

        Route::put('notification/{id}', [App\Http\Controllers\NotificationController::class, 'markAsViewNotification'])->name('markAsView');

        Route::get(
            'notification/all',
            [App\Http\Controllers\NotificationController::class,
            'index']
        )->name('notifications.index');
    }
);
