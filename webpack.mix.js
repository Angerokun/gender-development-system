const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .scripts([
        'node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js',
        'resources/js/jquery.dataTables.min.js',
        'resources/js/dataTables.bootstrap4.min.js',
        'resources/js/custom.js',
    ],'public/js/all.js')
    .styles([
        'resources/css/dataTables.bootstrap4.min.css',
        'resources/css/custom.css',
    ],'public/css/all.css');
